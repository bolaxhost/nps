﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentService; 

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Учетная запись пользователя", InstanceType = 201)]
    public class UserAccountDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0},", this.Main.name).Trim();
        }

        public IQueryable<UserAccount> GetMainData()
        {
            return GetData<UserAccount>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры")]
        public UserAccount Main { get { return GetSection<UserAccount>(); } }

        #endregion
    }
}
