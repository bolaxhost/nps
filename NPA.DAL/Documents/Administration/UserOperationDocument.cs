﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NPS.RDBDocumentService; 

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Операция пользователя", InstanceType = 203)]
    public class UserOperationDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0},", this.Main.name).Trim();
        }

        public IQueryable<UserOperation> GetMainData()
        {
            return GetData<UserOperation>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры")]
        public UserOperation Main { get { return GetSection<UserOperation>(); } }

        #endregion
    }
}
