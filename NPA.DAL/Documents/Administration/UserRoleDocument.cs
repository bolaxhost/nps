﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NPS.RDBDocumentService; 

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Роль пользователя", InstanceType = 202)]
    public class UserRoleDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0},", this.Main.name).Trim();
        }

        public IQueryable<UserRole> GetMainData()
        {
            return GetData<UserRole>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры")]
        public UserRole Main { get { return GetSection<UserRole>(); } }

        #endregion
    }
}
