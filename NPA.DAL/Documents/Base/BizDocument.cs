﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NPS.DAL.Model;
using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;
using NPS.RDBDocumentModel.IoC; 

namespace NPS.DAL
{
    
    [DocumentDefinition(Description = "Базовый документ")]
    public class BizDocument : BaseDocument
    {
        static BizDocument()
        {
            //Переопределение метода фабрики сервисов
            _createServiceFactory = () => { return new BizServiceFactory(); };

            //Регистрация документов для создания экземпляров документа по ключам
            Document.AppendDocumentType(typeof(MetaObjectDocument));
            Document.AppendDocumentType(typeof(MetaObjectInstanceDocument));
            Document.AppendDocumentType(typeof(MetaEnumDocument));
            Document.AppendDocumentType(typeof(MetaNavigationTabDocument));
        }







        public override void Create(Guid? idToCreate = null)
        {
            base.Create(idToCreate);

            //Только для первых версий документа (без учета клонов)
            if (this.System.majorVersion == 0)
            {
                //Инициализация состояния
                this.System.isActual = true;
                this.System.isEnabled = true;

                //Инициализация версии
                this.System.majorVersion = 1;
                this.System.minorVersion = 0;
            }
        }

        protected override void HandleIdentityChanges()
        {
            base.HandleIdentityChanges();

            var _UserEnvironment = new UserEnvironment(this.Identity) ;

            var _snapshot = _UserEnvironment["npsvmds_snapshot"];
            this.MyContext.DbStorage.Snapshot = (_snapshot == null) ? null : (DateTime?)_snapshot;
        }

        public virtual IQueryable<SystemSection> GetSystemData(bool useFilterByDocumentType = true, bool useFilterBySectionType = true, bool useFilterByInstanceId = false)
        {
            return GetData<SystemSection>("get_System", useFilterByDocumentType, useFilterBySectionType, useFilterByInstanceId ? this.Id : null);
        }

        public virtual IQueryable<SystemSection> GetActualSystemData(bool useFilterByDocumentType = true, bool useFilterBySectionType = true, bool useFilterByInstanceId = false)
        {
            return GetActualSystemData(GetSystemData(useFilterByDocumentType, useFilterBySectionType, useFilterByInstanceId));
        }

        public static IQueryable<SystemSection> GetActualSystemData(IQueryable<SystemSection> system)
        {
            return system.Where(d => d.isActual && d.isEnabled);
        }

        public static IQueryable<SystemAndData<TData>> GetSystemAndData<TData>(IQueryable<SystemSection> system,  IQueryable<TData> data)
             where TData : class, IDocumentAttributesNonVMDS 
        {
            return from dataItem in data
                   join systemItem in system on dataItem.instance_id equals  systemItem.instance_id
                   select new SystemAndData<TData>() { System = systemItem, Data = dataItem };

        }

        private MetaObject _extensionDefinition = null;
        public MetaObject ExtensionDefinition
        {
            get
            {
                if (_extensionDefinition != null)
                    return _extensionDefinition;

                _extensionDefinition = GetExtensionDefinition();
                return _extensionDefinition;
            }
        }

        protected virtual MetaObject GetExtensionDefinition()
        {
            return GetExtensionDefinition(this, this.MyDefinition.Definition.InstanceType);
        }

        public static MetaObject GetExtensionDefinition(BizDocument uow, EBizDocumentTypes type, int? subType = null)
        {
            return GetExtensionDefinition(uow, (int)type, subType); 
        }

        public static MetaObject GetExtensionDefinition(BizDocument uow, int type, int? subType = null)
        {
            return uow.GetActualData<MetaObject>().Where(d => !d.isAbstract && d.extensionType == type && (subType == null || d.extensionSubtype == subType)).FirstOrDefault();
        }

        private MetaObjectInstance _extensionData = null;
        protected MetaObjectInstance ExtensionData
        {
            get 
            {
                if (this.State != EDocumentItemState.dsRead)
                    return null;

                if (this.ExtensionDefinition == null)
                    return null;

                if (_extensionData != null)
                    return _extensionData;

                _extensionData = this.GetActualData<MetaObjectInstance>().Where(e => e.instance_id == this.Id.Value).FirstOrDefault();
                return _extensionData; 
            }
        }

        public override void PrepareDeleteDocument()
        {
            if (this.isCommitContextDefined)
                return;

            //Удаление расширения
            if (this.ExtensionData != null && this.ExtensionDocument.State != EDocumentItemState.dsUndefined)
                this.ExtensionDocument.PrepareDeleteDocument();

            base.PrepareDeleteDocument();
        }

        public override void PrepareSaveDocument()
        {
            if (this.isCommitContextDefined)
                return;

            //Сохранение расширения
            if (this.ExtensionDocument != null && this.ExtensionDocument.State != EDocumentItemState.dsUndefined)
            {
                this.ExtensionDocument.Main.name = this.ToString();   
                this.ExtensionDocument.PrepareSaveDocument();
            }

            base.PrepareSaveDocument();
        }

        private MetaObjectInstanceDocument _extensionDocument = null;
        public MetaObjectInstanceDocument ExtensionDocument
        {
            get
            {
                if (this.State == EDocumentItemState.dsUndefined)
                    throw new DocumentException("Некорректное текущее состояние документа для формирования расширения!");

                if (this.ExtensionDefinition == null)
                    return null;

                if (_extensionDocument != null)
                    return _extensionDocument;

                _extensionDocument = new MetaObjectInstanceDocument();
                _extensionDocument.MyContext = this.MyContext;
                _extensionDocument.Identity = this.Identity;
                _extensionDocument.UseVersioning = this.UseVersioning;  

                if (this.State == EDocumentItemState.dsNew || this.ExtensionData == null)
                {
                    _extensionDocument.Create(this.Id); //common id!!!
                    _extensionDocument.Main.definitionId = this.ExtensionDefinition.instance_id;
    
                    return _extensionDocument;
                }

                _extensionDocument.Read(this.ExtensionData.instance_id);
                return _extensionDocument; 
            }
        }

        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = true, Description = "Системный раздел документа")]
        public SystemSection System { get { return GetSection<SystemSection>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, Description = "Файлы")]
        public ICollection<Attachment> Attachments { get { return GetSections<Attachment>(); } }

        #endregion

        #region Validation
        protected override void BeforeSave()
        {
            base.BeforeSave();

            if (this.State == EDocumentItemState.dsUndefined)
                return;

            if (this.ActualTill != null)
                return;

            if (this.State == EDocumentItemState.dsNew && System.majorVersion == 1)
            {
                //Новые документы (не клоны)
                System.createdDt = DateTime.Now;
                System.createdBy = Identity;
            }
            else
            {
                //Обновляемые документы или новые версии (клоны) документов
                System.updatedDt = DateTime.Now;
                System.updatedBy = Identity;
                System.minorVersion++;
            }

            string name = this.ToString();
            if (name.Length > 1000)
                name = name.Substring(0, 1000);

            System.name = name;
        }

        protected override void AfterDelete()
        {
            base.AfterDelete();
        }

        protected override void ValidateUpdate()
        {
            base.ValidateUpdate();
        }

        #endregion

        public override Document Clone(bool excludeSystem = true)
        {
            BizDocument clone = (BizDocument)base.Clone(excludeSystem);

            if (!excludeSystem)
            {
                //Увеличение версии
                clone.System.majorVersion++;
                clone.System.minorVersion = 0;
            }
 
            return clone;
        }

        public override bool UseVersioning
        {
            get
            {
                return base.UseVersioning; 
            }

            set
            {
                base.UseVersioning = value;

                if (_extensionDocument != null)
                    _extensionDocument.UseVersioning = value; 
            }
        }
    }

    public class SystemAndData<TData>
        where TData : class, IDocumentAttributesNonVMDS 
    {
        public SystemSection System { get; set; }
        public TData Data { get; set; }
    }
}
