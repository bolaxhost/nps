﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;  

namespace NPS.DAL
{
    public enum ESystemDocumentTypes
    {
        [Description("Метаописание объекта")]
        MetaObject = 101,

        [Description("Определение перечисления")]
        MetaEnum = 111,

        [Description("Определение раздела метаданных")]
        MetaNavigationTab = 112,

        [Description("Экземпляр метаобъекта")]
        MetaObjectInstance = 121,
    }

    public enum EAdministrationDocumentTypes
    {
        [Description("Учетная запись пользователя")]
        UserAccount = 201,

        [Description("Роль пользователя")]
        UserRole = 202,

        [Description("Операция пользователя")]
        UserOperation = 203
    }

    public enum EBizDocumentTypes
    {
        [Description("Запрос")]
        Request = 1001,
    }
}
