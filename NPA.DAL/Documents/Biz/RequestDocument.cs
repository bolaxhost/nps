﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NPS.RDBDocumentService; 

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Запрос", InstanceType = 1001)]
    public class RequestDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("Заявка №{0} от {1},", this.Main.number, this.Main.applicant).Trim();
        }

        public IQueryable<Request> GetMainData()
        {
            return GetData<Request>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры")]
        public Request Main { get { return GetSection<Request>(); } }

        #endregion
    }
}
