﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Определение перечисления", InstanceType = 111)]
    public class MetaEnumDocument : BizDocument
    {
        public IQueryable<MetaEnum> GetMainData()
        {
            return GetData<MetaEnum>("get_Main");
        }
        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Main.displayName, this.Main.systemName).Trim();
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры перечисления")]
        public MetaEnum Main { get { return GetSection<MetaEnum>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, Description = "Значения перечисления")]
        public ICollection<MetaEnumValue> Values { get { return GetSections<MetaEnumValue>(); } }

        #endregion
    }
}
