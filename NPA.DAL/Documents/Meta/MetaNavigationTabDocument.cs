﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Определение раздела метаданных", InstanceType = 112)]
    public class MetaNavigationTabDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Main.displayName, this.Main.systemName).Trim();
        }

        public IQueryable<MetaNavigationTab> GetMainData()
        {
            return GetData<MetaNavigationTab>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры раздела")]
        public MetaNavigationTab Main { get { return GetSection<MetaNavigationTab>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, Description = "Подразделы")]
        public ICollection<MetaNavigationTabSection> Values { get { return GetSections<MetaNavigationTabSection>(); } }

        #endregion
    }
}
