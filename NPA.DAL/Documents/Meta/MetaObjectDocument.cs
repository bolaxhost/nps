﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Метаописание объекта", InstanceType = 101)]
    public class MetaObjectDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Main.displayName, this.Main.systemName).Trim();
        }

        public IQueryable<MetaObject> GetMainData()
        {
            return GetData<MetaObject>("get_Main");
        }


        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры метаописания")]
        public MetaObject Main { get { return GetSection<MetaObject>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, Description = "Свойства метаобъекта")]
        public ICollection<MetaObjectProperty> Properties { get { return GetSections<MetaObjectProperty>(); } }

        #endregion
    }
}
