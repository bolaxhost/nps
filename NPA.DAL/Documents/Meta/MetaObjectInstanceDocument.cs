﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection; 
using System.CodeDom.Compiler;
using Microsoft.CSharp;  

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;

namespace NPS.DAL
{
    [DocumentDefinition(Description = "Экземпляр метаобъекта", InstanceType = 121)]
    public class MetaObjectInstanceDocument : BizDocument
    {
        public override string ToString()
        {
            return string.Format("{0}", this.Main.name).Trim();
        }

        public IQueryable<MetaObjectInstance> GetMainData()
        {
            return GetData<MetaObjectInstance>("get_Main");
        }

        public override void PrepareDeleteDocument()
        {
            //Удаление связанных объектов (рекурсивное)
            foreach (var ObjectValue in this.ObjectValues.ToList())
            {
                if (ObjectValue.value != Guid.Empty)
                {
                    MetaObjectProperty Definition = ObjectValue.Definition;
                    bool cascadeDelete = (Definition == null || Definition.PropertyType != EMetaPropertyType.ptReference);

                    if (cascadeDelete)
                    {
                        MetaObjectInstanceDocument nestedDoc = new MetaObjectInstanceDocument();
                        nestedDoc.MyContext = this.MyContext;
                        nestedDoc.Identity = this.Identity;
                        nestedDoc.Read(ObjectValue.value);

                        nestedDoc.IgnoreValidation = true;
                        nestedDoc.PrepareDeleteDocument();
                    }
                }
            }

            base.PrepareDeleteDocument();
        }

        #region Sections

        [DocumentSectionDefinition(IsRequired = true, IsSystem = false, Description = "Основные параметры метаобъекта")]
        public MetaObjectInstance Main { get { return GetSection<MetaObjectInstance>(); } }


        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 1, Description = "Значения bit")]
        public ICollection<MetaObjectInstanceProperty_bit> BitValues { get { return GetSections<MetaObjectInstanceProperty_bit>(); } }
       
        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 3, Description = "Значения datetime")]
        public ICollection<MetaObjectInstanceProperty_datetime> DateTimeValues { get { return GetSections<MetaObjectInstanceProperty_datetime>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 4, Description = "Значения enum")]
        public ICollection<MetaObjectInstanceProperty_enum> EnumValues { get { return GetSections<MetaObjectInstanceProperty_enum>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 5, Description = "Значения float")]
        public ICollection<MetaObjectInstanceProperty_float> RealValues { get { return GetSections<MetaObjectInstanceProperty_float>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 6, Description = "Значения int")]
        public ICollection<MetaObjectInstanceProperty_int> IntegerValues { get { return GetSections<MetaObjectInstanceProperty_int>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 7, Description = "Значения object")]
        public ICollection<MetaObjectInstanceProperty_object> ObjectValues { get { return GetSections<MetaObjectInstanceProperty_object>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 8, Description = "Значения string")]
        public ICollection<MetaObjectInstanceProperty_string> StringValues { get { return GetSections<MetaObjectInstanceProperty_string>(); } }

        [DocumentSectionDefinition(IsRequired = false, IsSystem = false, SectionType = 9, Description = "Значения text")]
        public ICollection<MetaObjectInstanceProperty_text> TextValues { get { return GetSections<MetaObjectInstanceProperty_text>(); } }

        #endregion
    }

    /// <summary>
    /// Свойство экземпляра метаобъекта
    /// </summary>
    public class MetaObjectPropertyInstance
    {
        public string systemName { get; private set; }
        public Guid definitionId { get; private set; }

        //Хранение данных
        private bool? bitValue { get; set; }
        private string classificationValue { get; set; }
        private DateTime? dateTimeValue { get; set; }
        private Guid? enumValue { get; set; }
        private double? floatValue { get; set; }
        private int? intValue { get; set; }
        private Guid? objectValue { get; set; }
        private string stringValue { get; set; }
        private string textValue { get; set; }
        private object nestedObject { get; set; }


        public object value
        {
            get 
            {
                switch (_definition.PropertyType)
                {
                    case EMetaPropertyType.ptBoolean:
                        {
                            return this.bitValue;
                        }
                    case EMetaPropertyType.ptDate:
                    case EMetaPropertyType.ptTime:
                    case EMetaPropertyType.ptDateTime:
                        {
                            DateTime? result = this.dateTimeValue;

                            if (result != null && result.Value.Kind == DateTimeKind.Local)
                                result = result.Value.ToUniversalTime();

                            return result;
                        }
                    case EMetaPropertyType.ptEnum:
                        {
                            return this.enumValue;
                        }
                    case EMetaPropertyType.ptReal:
                        {
                            return this.floatValue;
                        }
                    case EMetaPropertyType.ptInteger:
                        {
                            return this.intValue;
                        }
                    case EMetaPropertyType.ptHyperlink:
                    case EMetaPropertyType.ptString:
                        {
                            return this.stringValue;
                        }
                    case EMetaPropertyType.ptText:
                    case EMetaPropertyType.ptHTML:
                        {
                            return this.textValue;
                        }
                    case EMetaPropertyType.ptStructure:
                    case EMetaPropertyType.ptArray:
                        {
                            return this.nestedObject;
                        }
                    case EMetaPropertyType.ptReference:
                        {
                            return this.objectValue;
                        }
                }

                return null;
            }

            set
            {
                switch (_definition.PropertyType)
                {
                    case EMetaPropertyType.ptBoolean:
                        {
                            this.bitValue = (bool?)value;
                            break;
                        }
                    case EMetaPropertyType.ptDate:
                    case EMetaPropertyType.ptTime:
                    case EMetaPropertyType.ptDateTime:
                        {
                            this.dateTimeValue = (DateTime?)value;

                            if (this.dateTimeValue != null && this.dateTimeValue.Value.Kind == DateTimeKind.Utc)
                                this.dateTimeValue = this.dateTimeValue.Value.ToLocalTime();

                            break;
                        }
                    case EMetaPropertyType.ptEnum:
                        {
                            this.enumValue = (Guid?)value;
                            break;
                        }
                    case EMetaPropertyType.ptReal:
                        {
                            this.floatValue = (double?)value;
                            break;
                        }
                    case EMetaPropertyType.ptInteger:
                        {
                            this.intValue = (int?)value;
                            break;
                        }
                    case EMetaPropertyType.ptHyperlink:
                    case EMetaPropertyType.ptString:
                        {
                            this.stringValue = (string)value;
                            break;
                        }
                    case EMetaPropertyType.ptText:
                    case EMetaPropertyType.ptHTML:
                        {
                            this.textValue = (string)value;
                            break;
                        }
                    case EMetaPropertyType.ptStructure:
                    case EMetaPropertyType.ptArray:
                        {
                            this.nestedObject = value;
                            break;
                        }
                    case EMetaPropertyType.ptReference:
                        {
                            this.objectValue = (Guid?)value;
                            break;
                        }
                }
            }
        }

        private MetaObjectProperty _definition = null;
        public MetaObjectProperty definition
        {
            get
            {
                return _definition;
            }

            set
            {
                _definition = value;

                if (_definition == null)
                {
                    definitionId = Guid.Empty;
                    systemName = string.Empty; 
                    return;
                }

                definitionId = _definition.instance_section_id;
                systemName = _definition.GetSystemName();
            }
        }

        private MetaObjectExemplar _nestedExemplar = null;
        public MetaObjectExemplar NetstedExemplar
        {
            get 
            {
                if (this.definition.nestedObjectId == null)
                    return null;

                if (_nestedExemplar != null)
                    return _nestedExemplar;
 
                MetaObjectDocument nestedDefinition = new MetaObjectDocument();
                nestedDefinition.Identity = _exemplar.definition.Identity;
                nestedDefinition.MyContext = _exemplar.definition.MyContext;
                nestedDefinition.Read(this.definition.nestedObjectId.Value);

                _nestedExemplar = new MetaObjectExemplar(nestedDefinition);
                return _nestedExemplar; 
            }
        }

        private MetaObjectExemplar _exemplar = null;
        public MetaObjectPropertyInstance(MetaObjectExemplar exemplar)
        {
            _exemplar = exemplar; 
        }

        private MetaObjectInstanceProperty_base GetSection(MetaObjectInstanceDocument doc)
        {
            IQueryable<MetaObjectInstanceProperty_base> sections = null;
            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        sections = doc.BitValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        sections = doc.DateTimeValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        sections = doc.EnumValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        sections = doc.RealValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        sections = doc.IntegerValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        sections = doc.StringValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        sections = doc.TextValues.AsQueryable();
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                case EMetaPropertyType.ptArray:
                case EMetaPropertyType.ptReference:
                    {
                        sections = doc.ObjectValues.AsQueryable();
                        break;
                    }
            }

            return sections.FirstOrDefault(p => p.definitionId == _definition.instance_section_id);
        }

        public void Load(MetaObjectInstanceDocument doc)
        {
            if (this.definition == null)
                return;

            MetaObjectInstanceProperty_base section = GetSection(doc); 

            if (section == null)
                return;

            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        this.bitValue = ((MetaObjectInstanceProperty_bit)section).value;  
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        this.dateTimeValue = ((MetaObjectInstanceProperty_datetime)section).value;
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        this.enumValue = ((MetaObjectInstanceProperty_enum)section).value;
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        this.floatValue = ((MetaObjectInstanceProperty_float)section).value;
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        this.intValue = ((MetaObjectInstanceProperty_int)section).value;
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        this.stringValue = ((MetaObjectInstanceProperty_string)section).value;
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        this.textValue = ((MetaObjectInstanceProperty_text)section).value;
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                    {
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();

                            if (this.objectValue != Guid.Empty)
                            {
                                nestedInstance.Identity = doc.Identity;
                                nestedInstance.MyContext = doc.MyContext;
                                nestedInstance.Read(this.objectValue.Value);
                            }

                            this.NetstedExemplar.Load(nestedInstance);  
                            this.nestedObject = this.NetstedExemplar.SerializeExemplar();
                        }

                        break;
                    }
                case EMetaPropertyType.ptArray:
                    {
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();

                            if (this.objectValue != Guid.Empty)
                            {
                                nestedInstance.Identity = doc.Identity;
                                nestedInstance.MyContext = doc.MyContext;
                                nestedInstance.Read(this.objectValue.Value);
                            }

                            List<object> result = new List<object>();
                            foreach (var itemId in nestedInstance.ObjectValues.Select(i => i.value))
                            {
                                MetaObjectInstanceDocument arrayItem = new MetaObjectInstanceDocument();
                                arrayItem.Identity = doc.Identity;
                                arrayItem.MyContext = doc.MyContext;
                                arrayItem.Read(itemId);

                                this.NetstedExemplar.Load(arrayItem);
                                result.Add(this.NetstedExemplar.SerializeExemplar(itemId)); 
                            }

                            if (this.nestedObject == null)
                                this.nestedObject = Array.CreateInstance(this.NetstedExemplar.CreateModel().GetType(), result.Count());
   
                            result.CopyTo((object[])this.nestedObject);
                        }

                        break;
                    }
                case EMetaPropertyType.ptReference:
                    {
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;
                        break;
                    }
            }
        }

        public void Save(MetaObjectInstanceDocument doc)
        {
            if (this.definition == null)
                return;

            MetaObjectInstanceProperty_base section = GetSection(doc);

            if (section == null)
            {
                if (!HasValue)
                    return;

                section = Add(doc); 
            }
            else if (!HasValue)
            {
                Remove(doc);
                return;
            }

            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        ((MetaObjectInstanceProperty_bit)section).value = this.bitValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        ((MetaObjectInstanceProperty_datetime)section).value = this.dateTimeValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        ((MetaObjectInstanceProperty_enum)section).value = this.enumValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        ((MetaObjectInstanceProperty_float)section).value = this.floatValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        ((MetaObjectInstanceProperty_int)section).value = this.intValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        ((MetaObjectInstanceProperty_string)section).value = this.stringValue;
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        ((MetaObjectInstanceProperty_text)section).value = this.textValue;
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                    {
                        //Повторное чтение для извлечения Id связанного документа
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            this.NetstedExemplar.DeserializeExemplar(this.nestedObject);  

                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();
                            nestedInstance.Identity = doc.Identity;
                            nestedInstance.MyContext = doc.MyContext;

                            if (this.objectValue == null || this.objectValue.Value == Guid.Empty)
                            {
                                nestedInstance.Create(Guid.NewGuid());
                                nestedInstance.Main.definitionId = this.NetstedExemplar.definition.Id.Value;
                                nestedInstance.Main.name = string.Format("{0}.{1}", doc.ToString(), _definition.displayName);  
                                this.objectValue = nestedInstance.Id;
                            }
                            else
                            {
                                nestedInstance.Read(this.objectValue.Value);
                            }

                            this.NetstedExemplar.Save(nestedInstance);
                            nestedInstance.PrepareSaveDocument();  
                        }

                        ((MetaObjectInstanceProperty_object)section).value = this.objectValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptArray:
                    {
                        //Повторное чтение для извлечения Id связанного документа
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            //Массив
                            object[] array = (object[])this.nestedObject;

                            //Массив id
                            Guid?[] itemsId = new Guid?[array.Length];
                            for (int i = 0; i < array.Length; i++)
                                itemsId[i] = (Guid?)array[i].GetType().GetProperty("id").GetValue(array[i]);   


                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();
                            nestedInstance.Identity = doc.Identity;
                            nestedInstance.MyContext = doc.MyContext;

                            if (this.objectValue == null || this.objectValue.Value == Guid.Empty)
                            {
                                //Создание списка
                                nestedInstance.Create(Guid.NewGuid());
                                nestedInstance.Main.name = string.Format("{0}.{1}[{2}]", doc.ToString(), _definition.displayName, array.Length);
                                this.objectValue = nestedInstance.Id;
                            }
                            else
                            {
                                //Чтение списка
                                nestedInstance.Read(this.objectValue.Value);

                                //Список непустых id
                                Guid[] nonNullItemsId = itemsId.Where(v => v != null).Select(v => v.Value).ToArray();

                                //Удаление
                                foreach (var deletedItem in nestedInstance.ObjectValues.Where(i => !nonNullItemsId.Contains(i.value)).ToList())
                                    nestedInstance.ObjectValues.Remove(deletedItem);
                            
                                //Обновление
                                foreach (var item in nestedInstance.ObjectValues)
                                {
                                    MetaObjectInstanceDocument arrayItem = new MetaObjectInstanceDocument();
                                    arrayItem.Identity = doc.Identity;
                                    arrayItem.MyContext = doc.MyContext;
                                    arrayItem.Read(item.value);

                                    int index = Array.IndexOf(itemsId, item.value);
                                    arrayItem.Main.name = string.Format("{0}.{1}[{2}]", doc.ToString(), _definition.displayName, index);

                                    this.NetstedExemplar.DeserializeExemplar(array[index]);
                                    this.NetstedExemplar.Save(arrayItem);

                                    item.instance_section_index = index;

                                    //Обновление элемента
                                    arrayItem.PrepareSaveDocument();
                                }
                            }

                            //Добавление новых
                            for (int i = 0; i < itemsId.Length; i++)
                            {
                                Guid? itemId = itemsId[i];
                                if (itemId == null)
                                {
                                    MetaObjectInstanceDocument arrayItem = new MetaObjectInstanceDocument();
                                    arrayItem.Identity = doc.Identity;
                                    arrayItem.MyContext = doc.MyContext;
                                    arrayItem.Create(Guid.NewGuid());

                                    arrayItem.Main.definitionId = this.NetstedExemplar.definition.Id.Value;
                                    arrayItem.Main.name = string.Format("{0}.{1}[{2}]", doc.ToString(), _definition.displayName, i);

                                    //Добавление в коллекцию
                                    nestedInstance.ObjectValues.Add(new MetaObjectInstanceProperty_object() { value = arrayItem.Id.Value, instance_section_index = i });

                                    this.NetstedExemplar.DeserializeExemplar(array[i]);
                                    this.NetstedExemplar.Save(arrayItem);

                                    //Сохранение нового элемента
                                    arrayItem.PrepareSaveDocument();
                                }
                            }

                            //Сортировка
                            nestedInstance.OrderBy(nestedInstance.ObjectValues, nestedInstance.ObjectValues.OrderBy(i => i.instance_section_index).ToList());
  
                            //Сохранение списка
                            nestedInstance.PrepareSaveDocument();
                        }

                        ((MetaObjectInstanceProperty_object)section).value = this.objectValue.Value;
                        break;
                    }
                case EMetaPropertyType.ptReference:
                    {
                        ((MetaObjectInstanceProperty_object)section).value = this.objectValue.Value;
                        break;
                    }
            }
        }

        public bool HasValue
        {
            get
            {
                if (_definition == null)
                    return false;

                switch (_definition.PropertyType)
                {
                    case EMetaPropertyType.ptBoolean:
                        {
                            return this.bitValue != null;
                        }
                    case EMetaPropertyType.ptDate:
                    case EMetaPropertyType.ptTime:
                    case EMetaPropertyType.ptDateTime:
                        {
                            return this.dateTimeValue != null;
                        }
                    case EMetaPropertyType.ptEnum:
                        {
                            return this.enumValue != null;
                        }
                    case EMetaPropertyType.ptReal:
                        {
                            return this.floatValue != null;
                        }
                    case EMetaPropertyType.ptInteger:
                        {
                            return this.intValue != null;
                        }
                    case EMetaPropertyType.ptHyperlink:
                    case EMetaPropertyType.ptString:
                        {
                            return !string.IsNullOrWhiteSpace(this.stringValue);
                        }
                    case EMetaPropertyType.ptText:
                    case EMetaPropertyType.ptHTML:
                        {
                            return !string.IsNullOrWhiteSpace(this.textValue);
                        }
                    case EMetaPropertyType.ptStructure:
                    case EMetaPropertyType.ptArray:
                        {
                            return this.nestedObject != null;
                        }
                    case EMetaPropertyType.ptReference:
                        {
                            return this.objectValue != null;
                        }
                }

                return false;
            }
        }

        private MetaObjectInstanceProperty_base CreateSection(MetaObjectInstanceDocument doc)
        {
            MetaObjectInstanceProperty_base result = null;
            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        result = new MetaObjectInstanceProperty_bit();
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        result = new MetaObjectInstanceProperty_datetime();
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        result = new MetaObjectInstanceProperty_enum();
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        result = new MetaObjectInstanceProperty_float();
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        result = new MetaObjectInstanceProperty_int();
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        result = new MetaObjectInstanceProperty_string();
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        result = new MetaObjectInstanceProperty_text();
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                case EMetaPropertyType.ptArray:
                case EMetaPropertyType.ptReference:
                    {
                        result = new MetaObjectInstanceProperty_object();
                        break;
                    }
            }

            result.Scope.MyContext = doc.MyContext;
            result.definitionId = _definition.instance_section_id;  
            return result;
        }

        private MetaObjectInstanceProperty_base Add(MetaObjectInstanceDocument doc)
        {
            var section = CreateSection(doc); 

            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        doc.BitValues.Add((MetaObjectInstanceProperty_bit)section);
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        doc.DateTimeValues.Add((MetaObjectInstanceProperty_datetime)section);
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        doc.EnumValues.Add((MetaObjectInstanceProperty_enum)section);
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        doc.RealValues.Add((MetaObjectInstanceProperty_float)section);
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        doc.IntegerValues.Add((MetaObjectInstanceProperty_int)section);
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        doc.StringValues.Add((MetaObjectInstanceProperty_string)section);
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        doc.TextValues.Add((MetaObjectInstanceProperty_text)section);
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                case EMetaPropertyType.ptArray:
                case EMetaPropertyType.ptReference:
                    {
                        doc.ObjectValues.Add((MetaObjectInstanceProperty_object)section);
                        break;
                    }
            }

            return section;
        }

        private void Remove(MetaObjectInstanceDocument doc)
        {
            switch (_definition.PropertyType)
            {
                case EMetaPropertyType.ptBoolean:
                    {
                        doc.BitValues.Remove((MetaObjectInstanceProperty_bit)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptDate:
                case EMetaPropertyType.ptTime:
                case EMetaPropertyType.ptDateTime:
                    {
                        doc.DateTimeValues.Remove((MetaObjectInstanceProperty_datetime)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptEnum:
                    {
                        doc.EnumValues.Remove((MetaObjectInstanceProperty_enum)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptReal:
                    {
                        doc.RealValues.Remove((MetaObjectInstanceProperty_float)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptInteger:
                    {
                        doc.IntegerValues.Remove((MetaObjectInstanceProperty_int)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptHyperlink:
                case EMetaPropertyType.ptString:
                    {
                        doc.StringValues.Remove((MetaObjectInstanceProperty_string)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptText:
                case EMetaPropertyType.ptHTML:
                    {
                        doc.TextValues.Remove((MetaObjectInstanceProperty_text)GetSection(doc));
                        break;
                    }
                case EMetaPropertyType.ptStructure:
                    {
                        var section = GetSection(doc);

                        //Повторное чтение для извлечения Id связанного документа
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();

                            if (this.objectValue != Guid.Empty)
                            {
                                nestedInstance.Identity = doc.Identity;
                                nestedInstance.MyContext = doc.MyContext;
                                nestedInstance.Read(this.objectValue.Value);
                                nestedInstance.PrepareDeleteDocument();  
                            }
                        }

                        doc.ObjectValues.Remove((MetaObjectInstanceProperty_object)section);
                        break;
                    }
                case EMetaPropertyType.ptArray:
                    {
                        var section = GetSection(doc);

                        //Повторное чтение для извлечения Id связанного документа
                        this.objectValue = ((MetaObjectInstanceProperty_object)section).value;

                        if (this.NetstedExemplar != null)
                        {
                            MetaObjectInstanceDocument nestedInstance = new MetaObjectInstanceDocument();

                            if (this.objectValue != Guid.Empty)
                            {
                                nestedInstance.Identity = doc.Identity;
                                nestedInstance.MyContext = doc.MyContext;
                                nestedInstance.Read(this.objectValue.Value);

                                foreach (var itemId in nestedInstance.ObjectValues.Select(i => i.value))
                                {
                                    MetaObjectInstanceDocument arrayItem = new MetaObjectInstanceDocument();
                                    arrayItem.Identity = doc.Identity;
                                    arrayItem.MyContext = doc.MyContext;
                                    arrayItem.Read(itemId);
                                    arrayItem.PrepareDeleteDocument(); 
                                }

                                nestedInstance.PrepareDeleteDocument();
                            }
                        }

                        doc.ObjectValues.Remove((MetaObjectInstanceProperty_object)section);
                        break;
                    }
                case EMetaPropertyType.ptReference:
                    {
                        doc.ObjectValues.Remove((MetaObjectInstanceProperty_object)GetSection(doc));
                        break;
                    }
            }
        }
    }

    /// <summary>
    /// Определение свойства экземпляра метаобъекта
    /// </summary>
    public class MetaObjectPropertyInstanceDefinition
    {
        public MetaObjectProperty definition { get; private set; }
        public int index { get; private set; }

        public MetaObjectPropertyInstanceDefinition(MetaObjectProperty _definition, int _index)
        {
            this.definition = _definition;
            this.index = _index;
        }
    }

    /// <summary>
    /// Экземпляр метаобъекта
    /// </summary>
    public class MetaObjectExemplar
    {
        public MetaObjectDocument definition { get; private set; }
        public MetaObjectExemplar(MetaObjectDocument _definition)
        {
            this.definition = _definition;
        }

        private Dictionary<string, MetaObjectPropertyInstanceDefinition> _propertyDefinitions = null;
        public Dictionary<string, MetaObjectPropertyInstanceDefinition> propertyDefinitions
        {
            get
            {
                if (_propertyDefinitions != null)
                    return _propertyDefinitions;

                //Добавление своих свойств
                _propertyDefinitions = new Dictionary<string, MetaObjectPropertyInstanceDefinition>();

                int index = 0;
                foreach (var p in definition.Properties)
                {
                    _propertyDefinitions.Add(p.GetSystemName(), new MetaObjectPropertyInstanceDefinition(p, index));
                    index++;
                }

                if (definition.Main.parentId != null)
                {
                    MetaObjectDocument _parent = new MetaObjectDocument();
                    _parent.Read(definition.Main.parentId.Value);

                    //Добавление непереопределенных свойств из базового определения в конец списка
                    Dictionary<string, MetaObjectPropertyInstanceDefinition> _parentpropertyDefinitions = new MetaObjectExemplar(_parent).propertyDefinitions;
                    foreach (var p in _parentpropertyDefinitions.Select(di => di.Value).OrderBy(i => i.index))
                    {
                        if (!_propertyDefinitions.ContainsKey(p.definition.GetSystemName()))
                            _propertyDefinitions.Add(p.definition.GetSystemName(), new MetaObjectPropertyInstanceDefinition(p.definition, index));

                        index++;
                    }
                }

                return _propertyDefinitions;
            }
        }

        private List<MetaObjectPropertyInstance> _propertyInstances = null;
        public List<MetaObjectPropertyInstance> propertyInstances
        {
            get
            {
                if (_propertyInstances != null)
                    return _propertyInstances;

                _propertyInstances = new List<MetaObjectPropertyInstance>();

                foreach (var d in this.propertyDefinitions.Select(d => d.Value).OrderBy(v => v.index))
                    _propertyInstances.Add(new MetaObjectPropertyInstance(this) { definition = d.definition });

                foreach (var p in _propertyInstances)
                    if (p.NetstedExemplar != null)
                        p.NetstedExemplar.modelAssembly = this.modelAssembly; 

                return _propertyInstances;
            }
        }

        public void Load(MetaObjectInstanceDocument doc)
        {
            foreach (var d in this.propertyInstances)
                d.Load(doc);
        }
        public void Save(MetaObjectInstanceDocument doc)
        {
            foreach (var d in this.propertyInstances)
                d.Save(doc);
        }

        public object SerializeExemplar(Guid? id = null)
        {
            object result = CreateModel();

            if (id != null)
                result.GetType().GetProperty("id").SetValue(result, id);

            foreach (var p in result.GetType().GetProperties())
                if (p.CanWrite)
                {
                    if (this.propertyDefinitions.ContainsKey(p.Name))
                        p.SetValue(result, this.propertyInstances[this.propertyDefinitions[p.Name].index].value);
                }

            return result;
        }

        public void DeserializeExemplar(object data)
        {
            foreach (var p in data.GetType().GetProperties())
                if (p.CanRead)
                    if (this.propertyDefinitions.ContainsKey(p.Name))
                        this.propertyInstances[this.propertyDefinitions[p.Name].index].value = p.GetValue(data);
        }

        public static string CreateTypeScript(MetaObjectExemplar exemplar, List<string> skipTypes = null)
        {
            if (skipTypes == null)
                skipTypes = new List<string>();

            StringBuilder typesScript = new StringBuilder(); 

            string className = exemplar.definition.Main.GetSystemName();

            if (skipTypes.Contains(className))
                return string.Empty;

            skipTypes.Add(className);  

            StringBuilder script = new StringBuilder();
            script.Append("#TYPES!");
            script.AppendFormat("public class {0}", className);
            script.Append("{");

            //уникальный идентификатор (для таблицы)
            script.Append("public Guid? id {get; set;}");

            foreach (var p in exemplar.propertyInstances)
            {
                string propertyName = p.definition.GetSystemName();
                switch (p.definition.PropertyType)
                {
                    case EMetaPropertyType.ptBoolean:
                        {
                            script.AppendFormat("public bool? {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptDate:
                    case EMetaPropertyType.ptTime:
                    case EMetaPropertyType.ptDateTime:
                        {
                            script.AppendFormat("public DateTime? {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptReal:
                        {
                            script.AppendFormat("public double? {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptInteger:
                        {
                            script.AppendFormat("public int? {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptEnum:
                    case EMetaPropertyType.ptReference:
                        {
                            script.AppendFormat("public Guid? {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptHyperlink:
                    case EMetaPropertyType.ptString:
                    case EMetaPropertyType.ptText:
                    case EMetaPropertyType.ptHTML:
                        {
                            script.AppendFormat("public string {0}", propertyName);
                            break;
                        }
                    case EMetaPropertyType.ptStructure:
                    case EMetaPropertyType.ptArray:
                        {
                            MetaObjectExemplar nestedExemplar = p.NetstedExemplar;
                            if (nestedExemplar != null)
                            {
                                typesScript.Append(CreateTypeScript(nestedExemplar, skipTypes));
  
                                switch (p.definition.PropertyType)
                                {
                                    case EMetaPropertyType.ptStructure:
                                        {
                                            script.AppendFormat("public {0} {1}", nestedExemplar.definition.Main.GetSystemName(), propertyName);
                                            break;
                                        }
                                    case EMetaPropertyType.ptArray:
                                        {
                                            script.AppendFormat("public {0}[] {1}", nestedExemplar.definition.Main.GetSystemName(), propertyName);
                                            break;
                                        }
                                }
                            }
                            break;
                        }
                }

                script.Append("{get; set;}");
            }

            script.Append("}");
            return script.ToString().Replace("#TYPES!", typesScript.ToString()); 
        }

        private Assembly _modelAssembly = null;
        public Assembly modelAssembly 
        { 
            get
            {
                if (_modelAssembly == null)
                {
                    StringBuilder script = new StringBuilder();
                    script.AppendFormat("using System;");
                    script.Append(CreateTypeScript(this));

                    CompilerResults compiledResult = CompileScript(script.ToString());

                    if (compiledResult.Errors.HasErrors)
                        throw new InvalidOperationException(string.Format("Ошибка компиляции: {0}", compiledResult.Errors[0].ErrorText));

                    _modelAssembly = compiledResult.CompiledAssembly;
                }

                return _modelAssembly; 
            }
            set
            {
                _modelAssembly = value;
            }
        }

        public object CreateModel()
        {
            return Activator.CreateInstance(this.modelAssembly.GetType(this.definition.Main.GetSystemName()));
        }

        private static CompilerResults CompileScript(string source)
        {
            CompilerParameters parms = new CompilerParameters();

            parms.GenerateExecutable = false;
            parms.GenerateInMemory = true;
            parms.IncludeDebugInformation = false;

            CodeDomProvider compiler = CSharpCodeProvider.CreateProvider("CSharp");
            return compiler.CompileAssemblyFromSource(parms, source);
        } 
    }
}
