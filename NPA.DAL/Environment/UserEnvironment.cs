﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using NPS.DAL.Model;
using NPS.RDBDocumentService; 

namespace NPS.DAL
{
    public class UserEnvironment
    {
        public string identity { get; set; }
        public UserEnvironment()
        {

        }
        public UserEnvironment(string identity)
        {
            this.identity = identity; 
        }

        private IDocumentStorage _storage = null;
        private IDocumentStorage storage
        {
            get
            {
                if (_storage == null)
                    _storage = new BizServiceFactory().Create<IDocumentStorage>();  

                return _storage;
            }
        }

        private Dictionary<string, object> _values = new Dictionary<string, object>();
        public object this[string key]
        {
            get
            {
                if (string.IsNullOrWhiteSpace(identity))
                    return null;

                if (_values.ContainsKey(key))
                    return _values[key];

                var property = this.storage.Set<EnvironmentProperty>().FirstOrDefault(p => p.identity == identity && p.key == key);

                if (property == null)
                    return null;

                var value = property.valueOject;
                _values.Add(key, value);

                return value;
            }

            set
            {
                if (string.IsNullOrWhiteSpace(identity))
                    return;

                if (_values.ContainsKey(key))
                    _values.Remove(key);

                var property = this.storage.Set<EnvironmentProperty>().FirstOrDefault(p => p.identity == identity && p.key == key);

                if (property == null)
                {
                    if (value == null)
                        return;

                    property = new EnvironmentProperty() { id = Guid.NewGuid(), identity = identity, key = key, valueOject = value };
                    this.storage.Set<EnvironmentProperty>().Add(property);  
                } else
                {
                    if (value == null)
                        this.storage.Set<EnvironmentProperty>().Remove(property);
                    else
                        property.valueOject = value; 
                }

                this.storage.SaveChanges(); 
            }
        }
    }
}
