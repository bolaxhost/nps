﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.DAL.Model; 

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;
using NPS.RDBDocumentModel.IoC; 

namespace NPS.DAL
{
    public class BizServiceFactory : DefaultServiceFactory
    {
        protected override void DoDefaultBindings()
        {
            base.DoDefaultBindings();

            this.Unbind<IDocumentStorage>();
            this.Bind<DbBizEntities, IDocumentStorage>();

            this.Unbind<IDocumentDataScope>(); 
            this.Bind<BizDocument, IDocumentDataScope>();
        }
    }
}
