﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Reflection;   

using NPS.RDBDocumentService;
using NPS.RDBDocumentModel;
using NPS.RDBDocumentModel.IoC; 

namespace NPS.DAL.Model
{
    public class DbBizEntities : DbContext, IDocumentStorage, IMetaExtensibleModel  
    {
        public DbBizEntities()
            : base("name=npsData")
        {
            //Режим версионности ВЫКЛ
            UseVersioning = false;

            //Физическое удаление записей из таблиц ВЫКЛ
            PhysicalDeletion = false;
        }








        void IDocumentStorage.Detach(object entity)
        {
            this.Entry(entity).State = EntityState.Detached;  
        }

        DbSet IDocumentStorage.Set(Type entityType)
        {
            return base.Set(entityType);
        }

        DbSet<TEntity> IDocumentStorage.Set<TEntity>()
        {
            return base.Set<TEntity>();
        }





        public bool UseVersioning { get; set; }
        public bool PhysicalDeletion { get; set; }
        public DateTime? Snapshot { get; set; } 






        void IDocumentStorage.SaveChanges()
        {
            base.SaveChanges();
        }







        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Files
            modelBuilder.Entity<Attachment>()
                        .HasMany(d => d.Files)
                        .WithMany(n => n.Attachments)
                        .Map(c =>
                        {
                            c.ToTable("attachment_content", "files");
                        });




            //MetaObject
            modelBuilder.Entity<MetaObject>()
                        .HasMany(d => d.Children)
                        .WithMany(n => n.Parents)
                        .Map(c =>
                        {
                            c.ToTable("mo_hierarchy", "biz");
                        });

            //MetaObjectProperty
            modelBuilder.Entity<MetaObjectProperty>()
                        .HasMany(d => d.Enums)
                        .WithMany(n => n.MetaObjectProperties)
                        .Map(c =>
                        {
                            c.ToTable("mop_enum", "biz");
                        });

            modelBuilder.Entity<MetaObjectProperty>()
                        .HasMany(d => d.NestedObjects)
                        .WithMany(n => n.NestingPlaces)
                        .Map(c =>
                        {
                            c.ToTable("mop_nested", "biz");
                        });

            modelBuilder.Entity<MetaObjectProperty>()
                        .HasMany(d => d.Tabs)
                        .WithMany(n => n.MetaObjectProperties)
                        .Map(c =>
                        {
                            c.ToTable("mop_tab", "biz");
                        });

            modelBuilder.Entity<MetaObjectProperty>()
                        .HasMany(d => d.TabSections)
                        .WithMany(n => n.MetaObjectProperties)
                        .Map(c =>
                        {
                            c.ToTable("mop_tabSection", "biz");
                        });


            //MetaObjectInstance
            modelBuilder.Entity<MetaObjectInstance>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.Instances)
                        .Map(c =>
                        {
                            c.ToTable("moi_def", "biz");
                        });

            //MetaObjectInstance properties
            modelBuilder.Entity<MetaObjectInstanceProperty_bit>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.BitValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_bitValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_datetime>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.DateTimeValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_dateTimeValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_enum>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.EnumValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_enumValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_float>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.FloatValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_floatValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_int>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.IntValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_intValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_object>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.ObjectValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_objectValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_string>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.StringValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_stringValue", "biz");
                        });

            modelBuilder.Entity<MetaObjectInstanceProperty_text>()
                        .HasMany(d => d.Definitions)
                        .WithMany(n => n.TextValues)
                        .Map(c =>
                        {
                            c.ToTable("moip_textValue", "biz");
                        });


            //UserAccount, UserRole, UserOperation
            modelBuilder.Entity<UserAccount>()
                        .HasMany(d => d.Roles)
                        .WithMany(n => n.Accounts)
                        .Map(c =>
                        {
                            c.ToTable("UserAccount_UserRole", "biz");
                        });

            modelBuilder.Entity<UserRole>()
                        .HasMany(d => d.Operations)
                        .WithMany(n => n.Roles)
                        .Map(c =>
                        {
                            c.ToTable("UserRole_UserOperation", "biz");
                        });
        }




















        public virtual DbSet<EnvironmentProperty> EnvironmentProperty { get; set; }

        public virtual DbSet<SystemSection> SystemSection { get; set; }
        public virtual DbSet<Attachment> Attachment { get; set; }
        public virtual DbSet<File> File { get; set; }

        public virtual DbSet<MetaEnum> MetaEnum { get; set; }
        public virtual DbSet<MetaEnumValue> MetaEnumValue { get; set; }

        public virtual DbSet<MetaObject> MetaObject { get; set; }
        public virtual DbSet<MetaObjectProperty> MetaObjectProperty { get; set; }

        public virtual DbSet<MetaObjectInstance> MetaObjectInstance { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_bit> MetaObjectInstanceProperty_bit { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_datetime> MetaObjectInstanceProperty_datetime { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_enum> MetaObjectInstanceProperty_enum { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_float> MetaObjectInstanceProperty_float { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_int> MetaObjectInstanceProperty_int { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_object> MetaObjectInstanceProperty_object { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_string> MetaObjectInstanceProperty_string { get; set; }
        public virtual DbSet<MetaObjectInstanceProperty_text> MetaObjectInstanceProperty_text { get; set; }

        public virtual DbSet<UserAccount> UserAccount { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<UserOperation> UserOperation { get; set; }

        public virtual DbSet<Request> Request { get; set; }
    }


}
