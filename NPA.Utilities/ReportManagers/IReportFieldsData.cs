﻿using System.Collections.Generic;

namespace NPS.Utilities.ReportManagers
{
    public interface IReportFieldsData
    {
        Dictionary<string, object> GetData(Dictionary<string, ReportField> fields);
    }
}
