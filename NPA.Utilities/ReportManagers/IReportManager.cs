﻿using System.Web.Mvc;

namespace NPS.Utilities.ReportManagers
{
    public interface IReportManager
    {
        FileStreamResult PrintAction(string fileName, IReportFieldsData dataReport);
        FileStreamResult PrintAction(byte[] file, IReportFieldsData dataReport);
        FileStreamResult MergeReport(FileStreamResult fileMergeTo, FileStreamResult[] filePartials);
    }
}