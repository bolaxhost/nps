﻿using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace NPS.Utilities.ReportManagers.ReportExcelFast
{
    public interface IReportExcelFastManager
    {
        FileStreamResult PrintAction(List<DataTable> tables, double widthColumns = 50);
        FileStreamResult PrintAction(DataTable table, double widthColumns = 50, string additionalInfo = "", string fileNames = "");
    }
}