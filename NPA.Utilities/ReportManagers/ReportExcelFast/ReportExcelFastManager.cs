﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace NPS.Utilities.ReportManagers.ReportExcelFast
{
    public class ReportExcelFastManager : IReportExcelFastManager
    {
        public FileStreamResult PrintAction(List<DataTable> tables, double widthColumns = 50)
        {
            var stream = new MemoryStream();

            using (var spreadSheet = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                var wbp = spreadSheet.AddWorkbookPart();

                var wb = new Workbook();

                var ws = new Worksheet();
                var sd = new SheetData();

                // Add stylesheet
                var stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                stylesPart.Stylesheet = GenerateStyleSheet();
                stylesPart.Stylesheet.Save();

                ws.Append(sd);

                spreadSheet.WorkbookPart.Workbook = wb;
                spreadSheet.WorkbookPart.Workbook.Save();

                foreach (var table in tables)
                {
                    Columns columns = null;
                    var sheet = AddSheet(table.TableName, spreadSheet, out columns);

                    //columns width.  
                    CreateColumnWidth(sheet, 1, (uint)table.Columns.Count, widthColumns, spreadSheet, columns);
                    //

                    var rowColumns = (from DataColumn column in table.Columns select column.ColumnName).ToList();

                    AddRow(sheet, rowColumns, spreadSheet, true, true);

                    foreach (DataRow dataRow in table.Rows)
                    {
                        List<string> rowData = dataRow.ItemArray.Select(row => row.ToString()).ToList();

                        AddRow(sheet, rowData, spreadSheet, false, true);
                    }
                }

                spreadSheet.Close();
            }

            stream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(stream, "application/vnd.ms-excel") { FileDownloadName = string.Format("{0}.xlsx", "Report") };
        }

        public FileStreamResult PrintAction(DataTable table, double widthColumns = 50, string additionalInfo = "", string fileName = "")
        {
            var stream = GetFilledStream(table, widthColumns, additionalInfo);
            return new FileStreamResult(stream, "application/vnd.ms-excel")
            {
                FileDownloadName = String.Format("{0}.xlsx", String.IsNullOrWhiteSpace(fileName) ? "Report" : fileName)
            };
        }

        private MemoryStream GetFilledStream(DataTable table, double widthColumns, string additionalInfo)
        {
            var stream = new MemoryStream();

            using (var spreadSheet = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook))
            {
                var wbp = spreadSheet.AddWorkbookPart();

                var wb = new Workbook();

                var ws = new Worksheet();
                var sd = new SheetData();

                // Add stylesheet
                var stylesPart = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                stylesPart.Stylesheet = GenerateStyleSheet();
                stylesPart.Stylesheet.Save();

                ws.Append(sd);

                spreadSheet.WorkbookPart.Workbook = wb;
                spreadSheet.WorkbookPart.Workbook.Save();


                Columns columns = null;
                var sheet = AddSheet(table.TableName, spreadSheet, out columns);

                //columns width.  
                CreateColumnWidth(sheet, 1, (uint)table.Columns.Count, widthColumns, spreadSheet, columns);
                if (!string.IsNullOrWhiteSpace(additionalInfo))
                {
                    AddRow(sheet, new List<string>() { additionalInfo }, spreadSheet, false, false, true);
                }
                var rowColumns = (from DataColumn column in table.Columns select column.ColumnName).ToList();

                AddRow(sheet, rowColumns, spreadSheet, true, true);

                foreach (DataRow dataRow in table.Rows)
                {
                    List<string> rowData = dataRow.ItemArray.Select(row => row.ToString()).ToList();

                    AddRow(sheet, rowData, spreadSheet, false, true);
                }


                spreadSheet.Close();
            }

            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        internal static string AddSheet(string Name, SpreadsheetDocument spreadSheet, out Columns col)
        {
            var wsp = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            wsp.Worksheet = new Worksheet();

            wsp.Worksheet.AppendChild(new SheetData());

            wsp.Worksheet.Save();

            UInt32 sheetId;

            if (spreadSheet.WorkbookPart.Workbook.Sheets == null)
            {
                spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                sheetId = 1;
            }
            else
            {
                sheetId = Convert.ToUInt32(spreadSheet.WorkbookPart.Workbook.Sheets.Count() + 1);
            }

            spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
            {
                Id = spreadSheet.WorkbookPart.GetIdOfPart(wsp),
                SheetId = sheetId,
                Name = Name
            }
            );

            col = new Columns();

            spreadSheet.WorkbookPart.Workbook.Save();

            return spreadSheet.WorkbookPart.GetIdOfPart(wsp);
        }

        internal static Worksheet GetWorkSheet(string sheetId, SpreadsheetDocument spreadSheet)
        {
            var wsp = (WorksheetPart)spreadSheet.WorkbookPart.GetPartById(sheetId);
            return wsp.Worksheet;
        }

        internal static void AddRow(string sheetId, List<string> dataItems, SpreadsheetDocument spreadSheet, bool isHeader = false, bool isBorder = false, bool isTextWrapped = false)
        {
            var sd = (SheetData)GetWorkSheet(sheetId, spreadSheet).Where(x => x.LocalName == "sheetData").First();
            var header = new Row();

            header.RowIndex = Convert.ToUInt32(sd.ChildElements.Count()) + 1;

            sd.Append(header);

            uint styleIndex;

            if (isBorder)
                styleIndex = (uint)(isHeader ? 7 : 6);
            else
                styleIndex = (uint)(isHeader ? 1 : 0);
            if (isTextWrapped)
                styleIndex = (uint)8;

            foreach (var item in dataItems)
            {
                AppendCell(header, header.RowIndex, item, styleIndex);
            }

            GetWorkSheet(sheetId, spreadSheet).Save();
        }

        internal static void AppendCell(Row row, uint rowIndex, string value, uint styleIndex)
        {
            var cell = new Cell();
            cell.DataType = CellValues.InlineString;
            cell.StyleIndex = styleIndex;

            var t = new Text();
            t.Text = value;

            var inlineString = new InlineString();
            inlineString.AppendChild(t);

            cell.AppendChild(inlineString);

            var nextCol = "A";
            var c = (Cell)row.LastChild;
            if (c != null)
            {
                var numIndex = c.CellReference.ToString().IndexOfAny(new[] { '1', '2', '3', '4', '5', '6', '7', '8', '9' });

                var lastCol = c.CellReference.ToString().Substring(0, numIndex);

                nextCol = IncrementColRef(lastCol);
            }

            cell.CellReference = nextCol + rowIndex;

            row.AppendChild(cell);
        }

        internal static string IncrementColRef(string lastRef)
        {
            var characters = lastRef.ToUpperInvariant().ToCharArray();
            var sum = 0;
            for (var i = 0; i < characters.Length; i++)
            {
                sum *= 26;
                sum += (characters[i] - 'A' + 1);
            }

            sum++;

            var columnName = String.Empty;
            int modulo;

            while (sum > 0)
            {
                modulo = (sum - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                sum = (int)((sum - modulo) / 26);
            }

            return columnName;
        }

        internal static void CreateColumnWidth(string sheetId, uint startIndex, uint endIndex, double width, SpreadsheetDocument spreadSheet, Columns columns)
        {
            // Find the columns in the worksheet and remove them all

            if (GetWorkSheet(sheetId, spreadSheet).Where(x => x.LocalName == "cols").Count() > 0)
                GetWorkSheet(sheetId, spreadSheet).RemoveChild<Columns>(columns);

            // Create the column
            Column column = new Column();
            column.Min = startIndex;
            column.Max = endIndex;
            column.Width = width;
            column.CustomWidth = true;
            columns.Append(column); // Add it to the list of columns

            // Make sure that the column info is inserted *before* the sheetdata

            GetWorkSheet(sheetId, spreadSheet).InsertBefore<Columns>(columns, GetWorkSheet(sheetId, spreadSheet).Where(x => x.LocalName == "sheetData").First());
            GetWorkSheet(sheetId, spreadSheet).Save();
            spreadSheet.WorkbookPart.Workbook.Save();

        }

        internal static Stylesheet GenerateStyleSheet()
        {
            return new Stylesheet(
                new Fonts(
                    new Font(                                                               // Index 0 - The default font.
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 1 - The bold font.
                        new Bold(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Italic font.
                        new Italic(),
                        new FontSize() { Val = 11 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Calibri" }),
                    new Font(                                                               // Index 2 - The Times Roman font. with 16 size
                        new FontSize() { Val = 16 },
                        new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                        new FontName() { Val = "Times New Roman" })
                ),
                new Fills(
                    new Fill(                                                           // Index 0 - The default fill.
                        new PatternFill() { PatternType = PatternValues.None }),
                    new Fill(                                                           // Index 1 - The default fill of gray 125 (required)
                        new PatternFill() { PatternType = PatternValues.Gray125 }),
                    new Fill(                                                           // Index 2 - The yellow fill.
                        new PatternFill(
                            new ForegroundColor() { Rgb = new HexBinaryValue() { Value = "FFFFFF00" } }
                        ) { PatternType = PatternValues.Solid })
                ),
                new Borders(
                    new Border(                                                         // Index 0 - The default border.
                        new LeftBorder(),
                        new RightBorder(),
                        new TopBorder(),
                        new BottomBorder(),
                        new DiagonalBorder()),
                    new Border(                                                         // Index 1 - Applies a Left, Right, Top, Bottom border to a cell
                        new LeftBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new RightBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new TopBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new BottomBorder(
                            new Color() { Auto = true }
                        ) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                ),
                new CellFormats(
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 0 },                         // Index 0 - The default cell style.  If a cell does not have a style index applied it will use this style combination instead
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 1 - Bold 
                    new CellFormat() { FontId = 2, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 2 - Italic
                    new CellFormat() { FontId = 3, FillId = 0, BorderId = 0, ApplyFont = true },       // Index 3 - Times Roman
                    new CellFormat() { FontId = 0, FillId = 2, BorderId = 0, ApplyFill = true },       // Index 4 - Yellow Fill
                    new CellFormat(                                                                    // Index 5 - Alignment
                        new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center }
                    ) { FontId = 0, FillId = 0, BorderId = 0, ApplyAlignment = true },
                    new CellFormat() { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true },      // Index 6 - Border
                    new CellFormat() { FontId = 1, FillId = 0, BorderId = 1, ApplyBorder = true },       // Index 7 - Border and Bold
                    new CellFormat(new Alignment() { WrapText = true }) // Index 8 - Wrap
                )
            );
        }
    }
}