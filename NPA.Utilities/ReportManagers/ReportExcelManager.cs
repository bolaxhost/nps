﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace NPS.Utilities.ReportManagers
{
    public sealed class ReportExcelManager : ReportManager
    {
        public override FileStreamResult PrintAction(byte[] file, IReportFieldsData dataReport)
        {
            var stream = new MemoryStream();

            stream.Write(file, 0, file.Length);

            using (var spreadSheet = SpreadsheetDocument.Open(stream, true))
            {

                WorkbookPart workBookPart = spreadSheet.WorkbookPart;

                foreach (var sheet in spreadSheet.WorkbookPart.Workbook.Descendants<Sheet>())
                {
                    var wsPart = workBookPart.GetPartById(sheet.Id) as WorksheetPart;
                    if (wsPart == null)
                        continue;

                    var sheetData = wsPart.Worksheet.GetFirstChild<SheetData>();

                    var reportFields = new Dictionary<string, ReportField>();

                    foreach (Row row in wsPart.Worksheet.Descendants<Row>())
                    {
                        foreach (Cell cell in row.Elements<Cell>())
                        {
                            string value = GetCellValue(cell, workBookPart);

                            if (String.IsNullOrEmpty(value))
                                continue;

                            string fieldname = GetFieldName(value);

                            if (string.IsNullOrEmpty(fieldname))
                                continue;

                            var reportField = new ReportField(fieldname);
                            if (reportField.FieldType == ReportFieldType.Unknown)
                                continue;

                            if (!reportFields.ContainsKey(reportField.FieldName))
                                reportFields.Add(reportField.FieldName, reportField);
                        }
                    }

                    if (reportFields.Count > 0)
                    {
                        Dictionary<string, object> data = dataReport.GetData(reportFields);

                        //param ==>
                        foreach (Row row in wsPart.Worksheet.Descendants<Row>())
                        {
                            foreach (Cell cell in row.Elements<Cell>())
                            {
                                string value = GetCellValue(cell, workBookPart);
                                string fieldname = GetFieldName(value);

                                if (string.IsNullOrEmpty(fieldname))
                                    continue;

                                if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                                    continue;

                                if ((reportFields[fieldname].FieldType == ReportFieldType.Parameter) ||
                                    (reportFields[fieldname].FieldType == ReportFieldType.TableField))
                                {
                                    if (data.ContainsKey(fieldname))
                                    {
                                        var dataValue = data[reportFields[fieldname].FieldName] as string;

                                        cell.CellValue = new CellValue(dataValue);
                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                    }
                                }
                            }
                        }
                        //<==

                        //List and Table
                        foreach (Row row in wsPart.Worksheet.Descendants<Row>())
                        {
                            var rowParams = new Dictionary<string, string>();

                            foreach (Cell cell in row.Elements<Cell>())
                            {
                                string value = GetCellValue(cell, workBookPart);
                                string fieldname = GetFieldName(value);

                                if (string.IsNullOrEmpty(fieldname))
                                    continue;

                                if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                                    continue;

                                rowParams.Add(GetColumnValueFromName(cell.CellReference.Value), fieldname);
                            }

                            if (rowParams.Count > 0)
                            {
                                var countMaxData = rowParams.Select(rowParam => data[reportFields[rowParam.Value].FieldName]).OfType<DataTable>().Select(dataTable => dataTable.Rows.Count).Concat(new[] { 0 }).Max();

                                for (var i = 0; i < countMaxData; i++)
                                {
                                    Row rowCur = i + 1 == countMaxData ? row : CopyToLine(row, row.RowIndex, sheetData);

                                    foreach (Cell cell in rowCur.Elements<Cell>())
                                    {
                                        string value = GetCellValue(cell, workBookPart);
                                        string fieldname = GetFieldName(value);

                                        if (string.IsNullOrEmpty(fieldname))
                                            continue;

                                        if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                                            continue;

                                        if (!rowParams.ContainsKey(GetColumnValueFromName(cell.CellReference.Value)))
                                            continue;

                                        if (data.ContainsKey(fieldname))
                                        {
                                            var colName = reportFields[fieldname].GetTableFieldName();
                                            var dataTable = data[fieldname] as DataTable;

                                            var dataValue = string.Empty;

                                            if (dataTable != null && dataTable.Rows.Count > i)
                                            {
                                                dataValue = dataTable.Rows[i][colName].ToString();
                                            }

                                            cell.CellValue = new CellValue(dataValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        }
                                    }
                                }
                            }
                        }
                        //<==
                    }

                    wsPart.Worksheet.Save();
                }


            }

            // get package bytes
            stream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(stream, "application/vnd.ms-excel") { FileDownloadName = string.Format("{0}.xltx", "Report") };
        }

        public override FileStreamResult MergeReport(FileStreamResult fileMergeTo, FileStreamResult[] filePartials)
        {
            throw new NotImplementedException();
        }

        internal static string GetCellValue(Cell cell, WorkbookPart workBookPart)
        {
            string value = string.Empty;

            if (cell == null)
                return value;

            value = cell.InnerText;

            if (cell.DataType != null)
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:
                        var stringTable =
                            workBookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                        if (stringTable != null)
                        {
                            value =
                                stringTable.SharedStringTable.ElementAt(int.Parse(value)).InnerText;
                        }
                        break;
                    case CellValues.Boolean:
                        switch (value)
                        {
                            case "0":
                                value = "FALSE";
                                break;
                            default:
                                value = "TRUE";
                                break;
                        }
                        break;
                }
            }

            return value;
        }

        internal static string GetFieldName(string value)
        {
            string fieldname = string.Empty;

            string valueTrim = value.Trim();

            if ((valueTrim.StartsWith("«")) && (valueTrim.EndsWith("»")))
            {
                fieldname = value.Replace("«", string.Empty).Replace("»", string.Empty);
            }

            return fieldname;
        }

        internal static int GetColumnIndexFromName(string columnNameOrCellReference)
        {
            var columnIndex = 0;
            var factor = 1;
            for (var pos = columnNameOrCellReference.Length - 1; pos >= 0; pos--)
            {
                if (Char.IsLetter(columnNameOrCellReference[pos]))
                {
                    columnIndex += factor * ((columnNameOrCellReference[pos] - 'A') + 1);
                    factor *= 26;
                }
            }
            return columnIndex;
        }

        internal static string GetColumnValueFromName(string columnNameOrCellReference)
        {
            var columnValue = string.Empty;
            for (var pos = columnNameOrCellReference.Length - 1; pos >= 0; pos--)
            {
                if (Char.IsLetter(columnNameOrCellReference[pos]))
                {
                    columnValue += columnNameOrCellReference[pos];
                }
            }
            return columnValue;
        }

        internal static Row CopyToLine(Row refRow, uint rowIndex, SheetData sheetData)
        {
            var newRow = (Row)refRow.CloneNode(true);

            IEnumerable<Row> rows = sheetData.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);
            foreach (Row row in rows)
            {
                uint newRowIndex = Convert.ToUInt32(row.RowIndex.Value + 1);

                foreach (Cell cell in row.Elements<Cell>())
                {
                    string cellReference = cell.CellReference.Value;
                    cell.CellReference = new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                row.RowIndex = new UInt32Value(newRowIndex);
            }

            sheetData.InsertBefore(newRow, refRow);
            return newRow;
        }
    }
}