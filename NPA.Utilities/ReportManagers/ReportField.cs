﻿using System;

namespace NPS.Utilities.ReportManagers
{
    public class ReportField
    {
        public ReportFieldType FieldType { get; internal set; }
        public string FieldName { get; internal set; }

        public ReportField(string fieldname)
        {
            FieldName = fieldname;
            FieldType = GetFieldType(FieldName);
        }

        public string GetTableName()
        {
            return GetTableOrFieldName();
        }

        public string GetTableFieldName()
        {
            return GetTableOrFieldName(false);
        }

        internal string GetTableOrFieldName(bool isTable = true)
        {
            switch (FieldType)
            {
                case ReportFieldType.TableField:
                    return FieldName.Split(new[] { "." }, StringSplitOptions.None)[isTable ? 0 : 1];
                case ReportFieldType.TableFieldRows:
                case ReportFieldType.TableRows:
                    return FieldName.Replace("[]", string.Empty).Split(new[] { "." }, StringSplitOptions.None)[isTable ? 0 : 1];
            }

            return string.Empty;
        }

        internal static ReportFieldType GetFieldType(string fieldName)
        {
            if (fieldName.Contains("[]"))
            {
                var fileNameSplit = fieldName.Split(new[] { "." }, StringSplitOptions.None);

                if (fileNameSplit.Length != 2)
                    return ReportFieldType.Unknown;

                if (fileNameSplit[0].Contains("[]"))
                    return ReportFieldType.TableRows;

                if (fileNameSplit[1].Contains("[]"))
                    return ReportFieldType.TableFieldRows;
            }

            return fieldName.Contains(".") ? ReportFieldType.TableField : ReportFieldType.Parameter;
        }
    }
}