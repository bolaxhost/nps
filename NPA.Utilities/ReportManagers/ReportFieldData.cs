﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.Utilities.ReportManagers
{
    public abstract class ReportFieldData : IReportFieldsData
    {
        public abstract void FormingReport(Dictionary<string, object> data, Dictionary<string, ReportField> fields);

        public Dictionary<string, object> GetData(Dictionary<string, ReportField> fields)
        {
            var data = new Dictionary<string, object>();
            FormingReport(data, fields);

            return data;
        }
    }
}
