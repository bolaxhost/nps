﻿namespace NPS.Utilities.ReportManagers
{
    public enum ReportFieldType
    {
        Parameter,
        TableField,
        TableFieldRows,
        TableRows,
        Unknown
    }
}