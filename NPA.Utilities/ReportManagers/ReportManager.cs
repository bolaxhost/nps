﻿using System;
using System.Web.Mvc;

namespace NPS.Utilities.ReportManagers
{
    public abstract class ReportManager : IReportManager
    {
        public abstract FileStreamResult PrintAction(byte[] file, IReportFieldsData dataReport);
        public abstract FileStreamResult MergeReport(FileStreamResult fileMergeTo, FileStreamResult[] filePartials);

        public static IReportManager Init(ReportOfficeType reportType)
        {
            switch (reportType)
            {
                case ReportOfficeType.Word:
                    return new ReportWordManager();
                case ReportOfficeType.Excel:
                    return new ReportExcelManager();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public FileStreamResult PrintAction(string fileName, IReportFieldsData dataReport)
        {
            return PrintAction(System.IO.File.ReadAllBytes(fileName), dataReport);
        }
    }
}