﻿using System.ComponentModel;
namespace NPS.Utilities.ReportManagers
{
    public enum ReportOfficeType
    {
        [Description("Word")]
        Word,
        [Description("Excel")]
        Excel
    }
}