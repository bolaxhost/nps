﻿using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace NPS.Utilities.ReportManagers
{
    public sealed class ReportWordManager : ReportManager
    {
        private static readonly Regex InstructionRegEx =
                     new Regex(
                                 @"^[\s]*MERGEFIELD[\s]+(?<name>[#\w\[\]\.]*){1}               # This retrieves the field's name (Named Capture Group -> name)
                            [\s]*(\\\*[\s]+(?<Format>[\w]*){1})?                # Retrieves field's format flag (Named Capture Group -> Format)
                            [\s]*(\\b[\s]+[""]?(?<PreText>[^\\]*){1})?         # Retrieves text to display before field data (Named Capture Group -> PreText)
                                                                                # Retrieves text to display after field data (Named Capture Group -> PostText)
                            [\s]*(\\f[\s]+[""]?(?<PostText>[^\\]*){1})?",
                                 RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);

        
        public override FileStreamResult PrintAction(byte[] file, IReportFieldsData dataReport)
        {
            var stream = new MemoryStream();

            stream.Write(file, 0, file.Length);

            using (WordprocessingDocument docx = WordprocessingDocument.Open(stream, true))
            {
                ConvertFieldCodes(docx.MainDocumentPart.Document);

                var reportFields = new Dictionary<string, ReportField>();

                foreach (var field in docx.MainDocumentPart.Document.Descendants<SimpleField>())
                {
                    string fieldname = GetFieldName(field);
                    if (!string.IsNullOrEmpty(fieldname))
                    {
                        var reportField = new ReportField(fieldname);
                        if (reportField.FieldType == ReportFieldType.Unknown)
                            continue;

                        if (!reportFields.ContainsKey(reportField.FieldName))
                            reportFields.Add(reportField.FieldName, reportField);
                    }
                }

                if (reportFields.Count > 0)
                {
                    //Tables ==>
                    Dictionary<string, object> data = dataReport.GetData(reportFields);
                    foreach (var field in docx.MainDocumentPart.Document.Descendants<SimpleField>())
                    {
                        string fieldname = GetFieldName(field);

                        if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                            continue;

                        if ((reportFields[fieldname].FieldType == ReportFieldType.TableRows) ||
                            (reportFields[fieldname].FieldType == ReportFieldType.TableFieldRows))
                        {
                            var wrow = GetFirstParent<TableRow>(field);
                            if (wrow == null)
                            {
                                continue;
                            }

                            var wtable = GetFirstParent<Table>(wrow);
                            if (wtable == null)
                            {
                                continue;
                            }

                            var props = new List<TableCellProperties>();
                            var cellcolumnnames = new List<string>();
                            var paragraphInfo = new List<string>();
                            var cellfields = new List<SimpleField>();

                            foreach (TableCell cell in wrow.Descendants<TableCell>())
                            {
                                props.Add(cell.GetFirstChild<TableCellProperties>());
                                var p = cell.GetFirstChild<Paragraph>();
                                if (p != null)
                                {
                                    var pp = p.GetFirstChild<ParagraphProperties>();
                                    if (pp != null)
                                    {
                                        paragraphInfo.Add(pp.OuterXml);
                                    }
                                    else
                                    {
                                        paragraphInfo.Add(null);
                                    }
                                }
                                else
                                {
                                    paragraphInfo.Add(null);
                                }

                                string colname = string.Empty;
                                SimpleField colfield = null;
                                foreach (SimpleField cellfield in cell.Descendants<SimpleField>())
                                {
                                    colfield = cellfield;
                                    colname = reportFields[GetFieldName(cellfield)].GetTableFieldName();
                                    break;
                                }

                                cellcolumnnames.Add(colname);
                                cellfields.Add(colfield);
                            }

                            var rprops = wrow.GetFirstChild<TableRowProperties>();

                            if (data.ContainsKey(fieldname))
                            {
                                var dataTable = data[fieldname] as DataTable;

                                if (dataTable != null)
                                {
                                    foreach (DataRow row in dataTable.Rows)
                                    {
                                        var nrow = new TableRow();

                                        if (rprops != null)
                                        {
                                            nrow.Append(new TableRowProperties(rprops.OuterXml));
                                        }

                                        for (int i = 0; i < props.Count; i++)
                                        {
                                            var cellproperties = new TableCellProperties(props[i].OuterXml);
                                            var cell = new TableCell();
                                            cell.Append(cellproperties);
                                            var p = new Paragraph(new ParagraphProperties(paragraphInfo[i]));
                                            cell.Append(p);

                                            if (!string.IsNullOrEmpty(cellcolumnnames[i]))
                                            {
                                                if (!dataTable.Columns.Contains(cellcolumnnames[i]))
                                                {
                                                    throw new Exception(
                                                        string.Format(
                                                            "Unable to complete template: column name '{0}' is unknown in parameter tables !",
                                                            cellcolumnnames[i]));
                                                }

                                                if (!row.IsNull(cellcolumnnames[i]))
                                                {
                                                    string val = row[cellcolumnnames[i]].ToString();
                                                    p.Append(GetRunElementForText(val, cellfields[i]));
                                                }
                                            }

                                            nrow.Append(cell);
                                        }

                                        wtable.Append(nrow);
                                    }
                                }
                            }


                            wrow.Remove();
                        }
                    }
                    //<==

                    //List ==>
                    foreach (var field in docx.MainDocumentPart.Document.Descendants<SimpleField>())
                    {
                        string fieldname = GetFieldName(field);

                        if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                            continue;

                        if ((reportFields[fieldname].FieldType == ReportFieldType.TableRows) ||
                            (reportFields[fieldname].FieldType == ReportFieldType.TableFieldRows))
                        {
                            var paragraph = GetFirstParent<Paragraph>(field);
                            if (paragraph == null)
                            {
                                continue;
                            }

                            var paragraphProperties = paragraph.GetFirstChild<ParagraphProperties>();

                            var colName = reportFields[fieldname].GetTableFieldName();

                            if (data.ContainsKey(fieldname))
                            {
                                var dataTable = data[fieldname] as DataTable;

                                if (dataTable != null)
                                {
                                    for (int i = 0; i < dataTable.Rows.Count; i++)
                                    {
                                        if (i + 1 == dataTable.Rows.Count)
                                        {
                                            field.Parent.ReplaceChild<SimpleField>(GetRunElementForText(dataTable.Rows[i][colName].ToString(), field),
                                                                                field);

                                            continue;
                                        }

                                        var run = new Run();
                                        var text = new Text();
                                        text.Text = dataTable.Rows[i][colName].ToString();

                                        run.Append(text);

                                        var paragraphPropertiesNew = new ParagraphProperties(paragraphProperties.OuterXml);

                                        var paragraphNew = new Paragraph(paragraphPropertiesNew, run);

                                        paragraph.InsertBeforeSelf(paragraphNew);
                                    }
                                }
                            }
                        }
                    }

                    //<==

                    //param ==>
                    foreach (var field in docx.MainDocumentPart.Document.Descendants<SimpleField>().ToArray())
                    {
                        string fieldname = GetFieldName(field);

                        if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                            continue;

                        if (data.ContainsKey(reportFields[fieldname].FieldName))
                        {
                            var value = data[reportFields[fieldname].FieldName] as string;

                            if (value != null)
                            {
                                field.Parent.ReplaceChild<SimpleField>(GetRunElementForText(value, field),
                                    field);

                            }
                        }
                    }

                    //<==

                    docx.MainDocumentPart.Document.Save();

                    // process header(s)
                    foreach (HeaderPart hpart in docx.MainDocumentPart.HeaderParts)
                    {
                        ConvertFieldCodes(hpart.Header);

                        foreach (var field in hpart.Header.Descendants<SimpleField>().ToArray())
                        {
                            string fieldname = GetFieldName(field);

                            if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                                continue;

                            if (data.ContainsKey(reportFields[fieldname].FieldName))
                            {
                                var value = data[reportFields[fieldname].FieldName] as string;

                                if (value != null)
                                {
                                    field.Parent.ReplaceChild<SimpleField>(GetRunElementForText(value, field),
                                        field);

                                }
                            }
                        }
                        hpart.Header.Save();
                    }

                    // process footer(s)
                    foreach (FooterPart fpart in docx.MainDocumentPart.FooterParts)
                    {
                        ConvertFieldCodes(fpart.Footer);

                        foreach (var field in fpart.Footer.Descendants<SimpleField>().ToArray())
                        {
                            string fieldname = GetFieldName(field);

                            if (string.IsNullOrEmpty(fieldname) || !reportFields.ContainsKey(fieldname))
                                continue;

                            if (data.ContainsKey(reportFields[fieldname].FieldName))
                            {
                                var value = data[reportFields[fieldname].FieldName] as string;

                                if (value != null)
                                {
                                    field.Parent.ReplaceChild<SimpleField>(GetRunElementForText(value, field),
                                        field);

                                }
                            }
                        }

                        fpart.Footer.Save();
                    }
                }
            }

            // get package bytes
            stream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(stream, "application/vnd.ms-word") { FileDownloadName = string.Format("{0}.doc", "Report") };
        }

        public override FileStreamResult MergeReport(FileStreamResult fileMergeTo, FileStreamResult[] filePartials)
        {
            using (var myDoc = WordprocessingDocument.Open(fileMergeTo.FileStream, true))
            {
                var newBody = XElement.Parse(myDoc.MainDocumentPart.Document.Body.OuterXml);

                foreach (var file in filePartials)
                {
                    var tempDocument = WordprocessingDocument.Open(file.FileStream, true);
                    var tempBody = XElement.Parse(tempDocument.MainDocumentPart.Document.Body.OuterXml);

                    newBody.Add(tempBody);
                    myDoc.MainDocumentPart.Document.Body = new Body(newBody.ToString());
                    myDoc.MainDocumentPart.Document.Save();
                    myDoc.Package.Flush();
                }
            }

            fileMergeTo.FileStream.Seek(0, SeekOrigin.Begin);

            return fileMergeTo;
        }

        internal static void ConvertFieldCodes(OpenXmlElement mainElement)
        {
            Run[] runs = mainElement.Descendants<Run>().ToArray();
            if (runs.Length == 0) return;

            var newfields = new Dictionary<Run, Run[]>();

            int cursor = 0;
            do
            {
                Run run = runs[cursor];

                if (run.HasChildren && run.Descendants<FieldChar>().Count() > 0
                    && (run.Descendants<FieldChar>().First().FieldCharType & FieldCharValues.Begin) == FieldCharValues.Begin)
                {
                    var innerRuns = new List<Run> { run };

                    bool found = false;
                    string instruction = null;
                    RunProperties runprop = null;
                    do
                    {
                        cursor++;
                        run = runs[cursor];

                        innerRuns.Add(run);
                        if (run.HasChildren && run.Descendants<FieldCode>().Count() > 0)
                            instruction += run.GetFirstChild<FieldCode>().Text;
                        if (run.HasChildren && run.Descendants<FieldChar>().Count() > 0
                            && (run.Descendants<FieldChar>().First().FieldCharType & FieldCharValues.End) == FieldCharValues.End)
                        {
                            found = true;
                        }
                        if (run.HasChildren && run.Descendants<RunProperties>().Count() > 0)
                            runprop = run.GetFirstChild<RunProperties>();
                    } while (found == false && cursor < runs.Length);

                    if (!found)
                        throw new Exception("Found a Begin FieldChar but no End !");

                    if (!string.IsNullOrEmpty(instruction))
                    {
                        var newrun = new Run();
                        if (runprop != null)
                            newrun.AppendChild(runprop.CloneNode(true));
                        var simplefield = new SimpleField { Instruction = instruction };
                        newrun.AppendChild(simplefield);

                        newfields.Add(newrun, innerRuns.ToArray());
                    }
                }

                cursor++;
            } while (cursor < runs.Length);

            foreach (KeyValuePair<Run, Run[]> kvp in newfields)
            {
                kvp.Value[0].Parent.ReplaceChild(kvp.Key, kvp.Value[0]);
                for (int i = 1; i < kvp.Value.Length; i++)
                    kvp.Value[i].Remove();
            }
        }

        internal static string GetFieldName(SimpleField field)
        {
            var a = field.GetAttribute("instr", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            string fieldname = string.Empty;
            string instruction = a.Value;

            if (string.IsNullOrEmpty(instruction)) return fieldname;

            var m = InstructionRegEx.Match(instruction);
            if (m.Success)
            {
                fieldname = m.Groups["name"].ToString().Trim();
            }

            return fieldname;
        }

        internal static T GetFirstParent<T>(OpenXmlElement element)
            where T : OpenXmlElement
        {
            if (element.Parent == null)
            {
                return null;
            }
            else if (element.Parent.GetType() == typeof(T))
            {
                return element.Parent as T;
            }
            else
            {
                return GetFirstParent<T>(element.Parent);
            }
        }

        internal static Run GetRunElementForText(string text, SimpleField placeHolder)
        {
            string rpr = null;
            if (placeHolder != null)
            {
                foreach (var placeholderrpr in placeHolder.Descendants<RunProperties>())
                {
                    rpr = placeholderrpr.OuterXml;
                    break;  // break at first
                }
            }

            var r = new Run();
            if (!string.IsNullOrEmpty(rpr))
            {
                r.Append(new RunProperties(rpr));
            }

            if (!string.IsNullOrEmpty(text))
            {
                // first process line breaks
                var split = text.Split(new string[] { "\n" }, StringSplitOptions.None);
                var first = true;
                foreach (var s in split)
                {
                    if (!first)
                    {
                        r.Append(new Break());
                    }

                    first = false;

                    // then process tabs
                    var firsttab = true;
                    var tabsplit = s.Split(new string[] { "\t" }, StringSplitOptions.None);
                    foreach (var tabtext in tabsplit)
                    {
                        if (!firsttab)
                        {
                            r.Append(new TabChar());
                        }

                        r.Append(new Text(tabtext));
                        firsttab = false;
                    }
                }
            }

            return r;
        }
    }
}