﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; 
using System.Xml;
using System.Xml.Serialization;

namespace NPS.Utilities
{
    public static class Serializing
    {
        public static byte[] SerializeToByteArray(object content)
        {
            XmlSerializer serializer = new XmlSerializer(content.GetType());
            MemoryStream ms = new MemoryStream();
            using (StreamWriter writer = new StreamWriter(ms, Encoding.UTF8))
            {
                serializer.Serialize(writer, content);
                return ms.ToArray();
            }
        }

        public static string SerializeToString(object content)
        {
            XmlSerializer serializer = new XmlSerializer(content.GetType());
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, content);
                return writer.ToString();
            }
        }

        public static object DeserializeFromString(string typeName, string content)
        {
            return DeserializeFromString(Type.GetType(typeName), content);
        }

        public static object DeserializeFromString(Type contentType, string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return Activator.CreateInstance(contentType);
            }

            XmlSerializer serializer = new XmlSerializer(contentType);
            using (StringReader stringReader = new StringReader(content))
            {
                using (XmlReader xmlReader = XmlReader.Create(stringReader, new XmlReaderSettings()))
                {
                    return serializer.Deserialize(xmlReader);
                }
            }
        }
    }

    public class SerializingCollectionItem
    {
        public string Key { get; set; }
        public string ItemType { get; set; }
        public string ItemValue { get; set; }
    }
}
