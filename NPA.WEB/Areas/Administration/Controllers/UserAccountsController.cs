﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/admin/accounts")]
    public class UserAccountsController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Administration/Views/UserAccounts/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Administration/Views/UserAccounts/Edit.cshtml");
        }

        [HttpGet]
        [Route("password-form")]
        public ActionResult PasswordForm()
        {
            return View("~/Areas/Administration/Views/UserAccounts/Password.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Administration/Views/UserAccounts/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Administration/Views/UserAccounts/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new UserAccountFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<UserAccountFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new UserAccountDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<UserAccount>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
               
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("adm.account.crud");
                
                //Формирование ответа
                var scope = new UserAccountScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new UserAccountItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = i.Data.name,
                    login = i.Data.login,  
                    branchId = i.Data.branchId,  

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();
                
                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "adm.account.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                return Json(new UserAccountModel(new UserAccount()), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "adm.account.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserAccountDocument doc = new UserAccountDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<UserAccountModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                UserAccountDocument doc = new UserAccountDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new UserAccountModel(doc.Main), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.account.crud||biz.account.update")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserAccountDocument doc = new UserAccountDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<UserAccountModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.account.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                UserAccountDocument doc = new UserAccountDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        //Создание данных по умолчанию
        [HttpGet]
        [Route("password/{login}")]
        public ActionResult GetPasswordModel(string login)
        {
            try
            {
                return Json(new UserPasswordModel(login), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("password/{login?}")]
        public ActionResult ChangePassword()
        {
            try
            {
                var passwordModel = JsonConvert.DeserializeObject<UserPasswordModel>(GetRequestContent());
                if (passwordModel.password != passwordModel.passwordConfirmation)
                    throw new Exception("Значение пароля и подтверждения пароля не совпадают!");

                var doc = new UserAccountDocument();
                doc.Identity = Security.SecurityService.CurrentUser;

                var account = doc.GetActualData<UserAccount>().FirstOrDefault(i => i.login == passwordModel.login);
                if (account == null)
                    throw new Exception(string.Format("Пользователя {0} не существует!", passwordModel.login));

                doc.Read(account.instance_id);
                doc.Main.ChangePassword(passwordModel.password);

                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}