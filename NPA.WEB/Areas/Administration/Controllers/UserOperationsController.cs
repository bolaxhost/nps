﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/admin/operations")]
    public class UserOperationsController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Administration/Views/UserOperations/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Administration/Views/UserOperations/Edit.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Administration/Views/UserOperations/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Administration/Views/UserOperations/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new UserOperationFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<UserOperationFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new UserOperationDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<UserOperation>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
               
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("adm.operation.crud");
                
                //Формирование ответа
                var scope = new UserOperationScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new UserOperationItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = i.Data.name,
                    code = i.Data.code,  

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();
                
                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "adm.operation.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                return Json(new UserOperationModel(new UserOperation()), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "adm.operation.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserOperationDocument doc = new UserOperationDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<UserOperationModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                UserOperationDocument doc = new UserOperationDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new UserOperationModel(doc.Main), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.operation.crud")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserOperationDocument doc = new UserOperationDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<UserOperationModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.operation.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                UserOperationDocument doc = new UserOperationDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}