﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/admin/roles")]
    public class UserRolesController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Administration/Views/UserRoles/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Administration/Views/UserRoles/Edit.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Administration/Views/UserRoles/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Administration/Views/UserRoles/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new UserRoleFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<UserRoleFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new UserRoleDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<UserRole>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
               
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("adm.role.crud");
                
                //Формирование ответа
                var scope = new UserRoleScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new UserRoleItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = i.Data.name,

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();
                
                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "adm.role.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                return Json(new UserRoleModel(new UserRole()), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "adm.role.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserRoleDocument doc = new UserRoleDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<UserRoleModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                UserRoleDocument doc = new UserRoleDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new UserRoleModel(doc.Main), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.role.crud")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                UserRoleDocument doc = new UserRoleDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<UserRoleModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "adm.role.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                UserRoleDocument doc = new UserRoleDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}