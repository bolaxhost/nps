﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;

namespace NPS.WEB.Areas.Models
{
    public class UserAccountModel : ItemModel
    {
        public UserAccountModel() { }
        public UserAccountModel(UserAccount data) 
        { 
            Load(data);

            roles = data.Scope.GetActualData<UserRole>().ToList().Select(i => new AccountRoleModel() { id = i.instance_id, name = i.name }).ToArray();

            var myRoles = data.Roles.ToDictionary(i => i.instance_id, i => i.name);
            foreach (var role in roles)
                role.isEnabled = myRoles.ContainsKey(role.id);
        }

        public void SaveDocument(UserAccountDocument doc)
        {
            this.Save(doc.Main);

            var myRoles = doc.Main.Roles.ToDictionary(i => i.instance_id, i => i.name);
            foreach (var role in roles)
                if (role.isEnabled)
                {
                    if (!myRoles.ContainsKey(role.id))
                        doc.Main.Roles.Add(doc.GetActualData<UserRole>().First(i => i.instance_id == role.id));
                }
                else
                {
                    if (myRoles.ContainsKey(role.id))
                        doc.Main.Roles.Remove(doc.GetActualData<UserRole>().First(i => i.instance_id == role.id));
                }
        }

        public string login { get; set; }
        public string name { get; set; }
        public int? branchId { get; set; }
        public AccountRoleModel[] roles { get; set; }
        public bool isBlocked { get; set; }
        public DateTime? passwordIsValidTill { get; set; }
        public DateTime? accountIsValidTill { get; set; }
    }

    public class AccountRoleModel
    {
        public Guid id;
        public string name { get; set; }
        public bool isEnabled { get; set; }
    }

    public class UserAccountScope : ListScopeModel<UserAccountItem, UserAccountFilter>
    {
    }

    public class UserAccountItem : ListScopeItem
    {
        public string login { get; set; }
        public int? branchId { get; set; }
    }

    public class UserAccountFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : UserAccount
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.login.ToLower().Contains(this.criteria.ToLower())
                    || i.name.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }

    public class UserPasswordModel
    {
        public UserPasswordModel(string login) 
        {
            this.login = login;
            this.currentUser = Security.SecurityService.CurrentUser;   
        }

        public string password { get; set; }
        public string passwordConfirmation { get; set; }

        public string login { get; set; }
        public string currentUser { get; set; }
    }
}