﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
      

namespace NPS.WEB.Areas.Models
{
    public class UserOperationModel : ItemModel
    {
        public UserOperationModel() { }
        public UserOperationModel(UserOperation data) 
        { 
            Load(data);
        }

        public void SaveDocument(UserOperationDocument doc)
        {
            this.Save(doc.Main);
        }

        public string code { get; set; }
        public string name { get; set; }
    }

    public class UserOperationScope : ListScopeModel<UserOperationItem, UserOperationFilter>
    {
    }

    public class UserOperationItem : ListScopeItem
    {
        public string code { get; set; }
    }

    public class UserOperationFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : UserOperation
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.name.ToLower().Contains(this.criteria.ToLower()) 
                    || i.code.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }
}