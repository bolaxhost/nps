﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
using NPS.DAL.Model;  

namespace NPS.WEB.Areas.Models
{
    public class UserRoleModel : ItemModel
    {
        public UserRoleModel() { }
        public UserRoleModel(UserRole data) 
        { 
            Load(data);

            operations = data.Scope.GetActualData<UserOperation>().ToList().Select(i => new RoleOperationModel() { id = i.instance_id,  name = i.name }).ToArray();

            var myOperations = data.Operations.ToDictionary(i => i.instance_id, i => i.name);
            foreach (var op in operations)
                op.isEnabled = myOperations.ContainsKey(op.id);
        }

        public void SaveDocument(UserRoleDocument doc)
        {
            this.Save(doc.Main);

            var myOperations = doc.Main.Operations.ToDictionary(i => i.instance_id, i => i.name);
            foreach (var op in operations)
                if (op.isEnabled)
                {
                    if (!myOperations.ContainsKey(op.id))
                        doc.Main.Operations.Add(doc.GetActualData<UserOperation>().First(i => i.instance_id == op.id));
                }
                else
                {
                    if (myOperations.ContainsKey(op.id))
                        doc.Main.Operations.Remove(doc.GetActualData<UserOperation>().First(i => i.instance_id == op.id));
                }

        }

        public string name { get; set; }
        public RoleOperationModel[] operations { get; set; } 
    }

    public class RoleOperationModel
    {
        public Guid id;
        public string name { get; set; }
        public bool isEnabled { get; set; }
    }

    public class UserRoleScope : ListScopeModel<UserRoleItem, UserRoleFilter>
    {
    }

    public class UserRoleItem : ListScopeItem
    {
    }

    public class UserRoleFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : UserRole
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.name.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }
}