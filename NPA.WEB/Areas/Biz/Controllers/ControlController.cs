﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;


using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/control")]
    public class ControlController : BaseAuthorizedController
    {
        //API
        //Параметры версионного режима текущего пользователя
        [HttpGet]
        [Route("vmds")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetVMDS()
        {
            try
            {
                //Свойства текущего пользователя
                UserEnvironment _bag = this.UserEnvironment;

                VMDSData vmds = new VMDSData();
                vmds.useVersioning = false;

                DateTime? _snapshot = (DateTime?)_bag["npavmds_snapshot"];
                vmds.snapshot = (_snapshot == null) ? null : _snapshot;

                if (vmds.snapshot != null && vmds.snapshot.Value.Kind == DateTimeKind.Local)
                    vmds.snapshot = vmds.snapshot.Value.ToUniversalTime();  

                //Возврат результата на форму
                return Json(vmds, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        [HttpPost]
        [Route("vmds")]
        [AuthorizeUser(Expression = "")]
        public ActionResult SetVMDS()
        {
            try
            {
                VMDSData vmds = JsonConvert.DeserializeObject<VMDSData>(GetRequestContent());

                if (vmds.snapshot != null && vmds.snapshot.Value.Kind == DateTimeKind.Utc)
                    vmds.snapshot = vmds.snapshot.Value.ToLocalTime();  

                //Свойства текущего пользователя
                UserEnvironment _bag = this.UserEnvironment;
                _bag["npavmds_snapshot"] = vmds.snapshot;

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPut]
        [Route("vmds")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GotoActual()
        {
            try
            {
                //Свойства текущего пользователя
                UserEnvironment _bag = this.UserEnvironment;
                _bag["npavmds_snapshot"] = null;

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Список версий объекта
        [HttpGet]
        [Route("vmds/versions/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetVersions(Guid id)
        {
            try
            {
                var result = new BizDocument().GetData<SystemSection>().Where(i => i.instance_id == id).OrderByDescending(i => i.majorVersion).ToList();
                var versions = new List<VersionData>();

                foreach (var v in result)
                    if (v.actual_till == null)
                    {
                        versions.Add(new VersionData()
                        {
                            number = v.majorVersion,
                            name = string.Format("Версия {0}: {1} {2} {3}", v.majorVersion, v.actual_from.ToShortDateString(), v.actual_from.ToShortTimeString(), v.createdBy),
                            isActual = true,
                        });
                    } 
                    else
                    {
                        versions.Add(new VersionData()
                        {
                            number = v.majorVersion,
                            name = string.Format("Версия {0}: {1} {2} {3}", v.majorVersion, v.actual_from.ToShortDateString(), v.actual_from.ToShortTimeString(), v.updatedBy),
                            isActual = false,
                        });
                    }

                //Возврат результата на форму
                return Json(versions.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Текущая версия объекта
        [HttpGet]
        [Route("vmds/version/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetVersion(Guid id)
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                VersionData version = new VersionData() { number = -1 };

                var result = uow.GetActualData<SystemSection>().Where(i => i.instance_id == id).FirstOrDefault();
                if (result != null)
                    version.number = result.majorVersion;  

                //Возврат результата на форму
                return Json(version, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        [HttpPost]
        [Route("vmds/version/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult SetVersion(Guid id)
        {
            try
            {
                VersionData version = JsonConvert.DeserializeObject<VersionData>(GetRequestContent());

                GotoObjectVersion(id, version.number);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPut]
        [Route("vmds/version/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Goto(Guid id, int version)
        {
            try
            {
                GotoObjectVersion(id, version);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }


    public class VMDSData
    {
        public bool useVersioning { get; set; }
        public DateTime? snapshot { get; set; }
    }
    public class VersionData
    {
        public int number { get; set; }
        public string name { get; set; }
        public bool isActual { get; set; }
    }
}