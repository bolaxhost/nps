﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Net; 
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/editor")]
    public class EditorController : BaseSpatialController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Biz/Views/Editor/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Biz/Views/Editor/Edit.cshtml");
        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("model")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetModel()
        {
            try
            {
                var user = SecurityService.CurrentAccount;
                var model = new EditorModel() { settings = this.Settings };

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("commit")]
        [AuthorizeUser(Expression = "data.digitize")]
        public ActionResult PostWFS(string data)
        {
            try
            {
                PostWFSData(Settings.wfsUrl, data);

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("data")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetWFS(string layer, string filter = "")
        {
            try
            {
                return Json(new { data = GetWFSData(this.Settings.wfsUrl, layer, filter) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}