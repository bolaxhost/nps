﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
using NPS.RDBDocumentModel;      
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/meta/enums")]
    public class MetaEnumsController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Biz/Views/Meta/Enums/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Biz/Views/Meta/Enums/Edit.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Biz/Views/Meta/Enums/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Biz/Views/Meta/Enums/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new MetaEnumFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<MetaEnumFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new MetaEnumDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<MetaEnum>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
               
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("meta.crud");
                
                //Формирование ответа
                var scope = new MetaEnumScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new MetaEnumItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = i.Data.displayName,
                    systemName = i.Data.systemName,

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();
                
                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Перечисление всех определений
        [HttpGet]
        [Route("enumeration")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetEnumeration()
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
                var enums = uow.GetActualData<MetaEnum>().OrderBy(i => i.displayName);

                return Json(enums.Select(i => new { id = i.instance_id, name = i.displayName }).ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Значения перечислений
        [HttpGet]
        [Route("values/{enumId}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetEnumValues(Guid enumId)
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var definitions = uow.GetActualData<MetaEnumValue>()
                    .Where(i => i.instance_id == enumId)
                    .OrderBy(i => i.instance_section_index)
                    .Select(i => new SimpleEnumerationItem<Guid?>() { id = i.instance_section_id, name = i.displayName })
                    .ToList();

                definitions.Add(new SimpleEnumerationItem<Guid?>() { id = null, name = " - " });

                return Json(definitions.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                return Json(new MetaEnumModel(new MetaEnum(), null), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaEnumDocument doc = new MetaEnumDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<MetaEnumModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                MetaEnumDocument doc = new MetaEnumDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new MetaEnumModel(doc.Main, doc.Values), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaEnumDocument doc = new MetaEnumDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<MetaEnumModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                MetaEnumDocument doc = new MetaEnumDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}