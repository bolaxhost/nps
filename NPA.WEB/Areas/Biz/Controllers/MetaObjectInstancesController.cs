﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
using NPS.RDBDocumentModel;      
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;   
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/meta/objects/instances")]
    public class MetaObjectInstancesController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Instances/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form/{definitionId}")]
        public ActionResult EditForm(Guid definitionId)
        {
            MetaObjectInstanceModel model = new MetaObjectInstanceModel(definitionId); 
            return View("~/Areas/Biz/Views/Meta/Objects/Instances/Edit.cshtml", new MetaObjectExemplarModel(model.Exemplar));
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Instances/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Instances/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new MetaObjectInstanceFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<MetaObjectInstanceFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new MetaObjectInstanceDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<MetaObjectInstance>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
                
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("meta.crud");
                
                //Формирование ответа
                var scope = new MetaObjectInstanceScope() { CanCreate = scopefilter.definitionId != null };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new MetaObjectInstanceItem() 
                {
                    id = i.Data.instance_id,
                    
                    name = i.Data.name,
                    definitionId = i.Data.definitionId,

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();

                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }


        //Список объектов
        [HttpGet]
        [Route("enumeration/{definitionId?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetEnumeration(Guid? definitionId)
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var instances = uow.GetActualData<MetaObjectInstance>()
                    .Where(i => definitionId == null || i.Definitions.Any(d => d.instance_id == definitionId))
                    .OrderBy(i => i.name)
                    .Select(i => new SimpleEnumerationItem<Guid?>() { id = i.instance_id, name = i.name })
                    .ToList();

                instances.Add(new SimpleEnumerationItem<Guid?>() { id = null, name = " - " });

                return Json(instances.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Определения объектов
        [HttpGet]
        [Route("definitions")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetDefinitions()
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var definitions = uow.GetActualData<MetaObject>()
                    .OrderBy(i => i.displayName) 
                    .Select(i => new SimpleEnumerationItem<Guid?>() { id = i.instance_id, name = i.displayName })
                    .ToList();

                definitions.Add(new SimpleEnumerationItem<Guid?>() { id = null, name = " - " });  

                return Json(definitions.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/{definitionId}/default")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult GetDefault(Guid definitionId)
        {
            try
            {
                return Json(new MetaObjectInstanceModel(definitionId), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item/{definitionId}")]
        [AuthorizeUser(Expression = "meta.crud")]
        
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaObjectInstanceDocument doc = new MetaObjectInstanceDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<MetaObjectInstanceModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{definitionId}/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                MetaObjectInstanceDocument doc = new MetaObjectInstanceDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new MetaObjectInstanceModel(doc), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{definitionId}/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaObjectInstanceDocument doc = new MetaObjectInstanceDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<MetaObjectInstanceModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{definitionId}/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                MetaObjectInstanceDocument doc = new MetaObjectInstanceDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}