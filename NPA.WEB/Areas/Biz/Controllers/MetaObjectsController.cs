﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
using NPS.RDBDocumentModel;      
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;   
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/meta/objects")]
    public class MetaObjectsController : BaseAuthorizedController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            ViewBag.SelectedSection = "admin";
            return View("~/Areas/Biz/Views/Meta/Objects/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Edit.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Biz/Views/Meta/Objects/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new MetaObjectFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<MetaObjectFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new MetaObjectDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<MetaObject>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);

                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("meta.crud");

                //Формирование ответа
                var scope = new MetaObjectScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new MetaObjectItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = i.Data.displayName,
                    systemName = i.Data.systemName,

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();

                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Определения объектов
        [HttpGet]
        [Route("enumeration")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetEnumeration()
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var definitions = uow.GetActualData<MetaObject>()
                    .OrderBy(i => i.displayName)
                    .Select(i => new SimpleEnumerationItem<Guid?>() { id = i.instance_id, name = i.displayName })
                    .ToList();

                definitions.Add(new SimpleEnumerationItem<Guid?>() { id = null, name = " - " });

                return Json(definitions.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Типы расширений
        [HttpGet]
        [Route("extension-types")]
        public ActionResult GetExtensionTypes()
        {
            try
            {
                return Json(typeof(EBizDocumentTypes).ToSelectOptions(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Типы свойств
        [HttpGet]
        [Route("property-types")]
        public ActionResult GetPropertyTypes()
        {
            try
            {
                return Json(typeof(EMetaPropertyType).ToSelectOptions(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Типы свойств
        [HttpGet]
        [Route("property-classifications")]
        public ActionResult GetPropertyClassification()
        {
            try
            {
                return Json(typeof(EMetaPropertyClassification).ToSelectOptions(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Кандидаты на роль базового объекта
        //TODO: исключить зацикливание
        [HttpGet]
        [Route("parent-candidates/{id?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetParentCandidatesOf(Guid? id)
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var parents = uow.GetActualData<MetaObject>();

                if (id != null)
                    parents = parents.Where(i => i.instance_id != id);

                return Json(parents.Select(i => new { id = i.instance_id, name = i.displayName }).ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Кандидаты на роль внедренного объекта
        //TODO: исключить зацикливание
        [HttpGet]
        [Route("nested-candidates/{id?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetNestedCandidatesOf(Guid? id)
        {
            try
            {
                var uow = new BizDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();

                var nested = uow.GetActualData<MetaObject>();

                if (id != null)
                    nested = nested.Where(i => i.instance_id != id);

                return Json(nested.Select(i => new { id = i.instance_id, name = i.displayName }).ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                return Json(new MetaObjectModel(new MetaObject(), null), JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaObjectDocument doc = new MetaObjectDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                JsonConvert.DeserializeObject<MetaObjectModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                MetaObjectDocument doc = new MetaObjectDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new MetaObjectModel(doc.Main, doc.Properties), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("item/{id}/compile")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Compile(Guid id)
        {
            try
            {
                MetaObjectDocument doc = new MetaObjectDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                return Json(new MetaObjectExemplar(doc).CreateModel(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                MetaObjectDocument doc = new MetaObjectDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                JsonConvert.DeserializeObject<MetaObjectModel>(GetRequestContent()).SaveDocument(doc); 
                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "meta.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                MetaObjectDocument doc = new MetaObjectDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}