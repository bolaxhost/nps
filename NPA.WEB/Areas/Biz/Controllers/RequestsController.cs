﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net; 
using Newtonsoft.Json;

using PagedList;

using NPS.DAL;
  

using NPS.WEB.Core;
using NPS.WEB.Security;
using NPS.WEB.Filters;

using NPS.WEB.Models;
using NPS.WEB.Areas.Models;

namespace NPS.WEB.Areas.Controllers
{
    [RoutePrefix("api/biz/requests")]
    public class RequestsController : BaseSpatialController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View("~/Areas/Biz/Views/Requests/Index.cshtml");
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            MetaObjectInstanceModel extensionModel = new MetaObjectInstanceModel();

            //Поиск расширения модели данных
            var extension = GetExtensionDefinition(EBizDocumentTypes.Request);
            if (extension != null)
                extensionModel = new MetaObjectInstanceModel(extension.instance_id);

            extensionModel.settings = this.Settings;
            return View("~/Areas/Biz/Views/Requests/Edit.cshtml", extensionModel);
        }

        [HttpGet]
        [Route("print-form")]
        public ActionResult PrintForm()
        {
            return View("~/Areas/Biz/Views/Requests/Print.cshtml");
        }

        [HttpGet]
        [Route("delete-form")]
        public ActionResult DeleteForm()
        {
            return View("~/Areas/Biz/Views/Requests/Delete.cshtml");
        }

        [HttpGet]
        [Route("search-form")]
        public ActionResult SearchForm()
        {
            return View("~/Areas/Biz/Views/Requests/Search.cshtml");
        }


        //API
        //Предоставление данных по запросу
        [HttpGet]
        [Route("query/{page?}/{pageSize?}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Query(int page = 0, int pageSize = 0, string filter = "")
        {
            try
            {
                //Инициализация фильтра
                var scopefilter = new RequestFilter();
                if (!string.IsNullOrWhiteSpace(filter))
                {
                    scopefilter = JsonConvert.DeserializeObject<RequestFilter>(filter);
                    page = page == 0 ? scopefilter.page : page; 
                }

                //Получение актуальных данных и фильтрация
                var uow = new RequestDocument();
                uow.Identity = SecurityHelper.GetCurrentUser();
  
                var result = BizDocument.GetSystemAndData<Request>(uow.GetActualSystemData(), scopefilter.Apply(uow.GetMainData()));

                var user = SecurityService.CurrentAccount;
                if (user != null && !SecurityService.isOperationPermitted("biz.scope.all"))
                    result = result.Where(i => i.Data.branchId == user.branchId);

                //Сортировка
                result = (scopefilter.Sorting.HasItems) 
                    ? scopefilter.Sorting.DoOrderBy(result) 
                    : result.OrderByDescending(i => i.System.createdDt);
               
                //Разрешение на ведение данных
                bool crud = SecurityService.isOperationPermitted("biz.request.crud");
                
                //Формирование ответа
                var scope = new RequestScope() { CanCreate = true };
                scope.page = (page == 0) ? 1 : page;
                scope.pageSize = pageSize = ViewServices.GetPageSize(pageSize);

                scope.items = result.ToPagedList(scope.page, scope.pageSize).Select(i => new RequestItem() 
                {
                    id = i.Data.instance_id,
                    
                    displayName = string.Format("Заявка № {0} от {1}", i.Data.number,  i.Data.applicant),

                    branchId = i.Data.branchId,  
                    number = i.Data.number,
                    applicant = i.Data.applicant,
                    applicantAddress = i.Data.applicantAddress, 
                    cadasterNumber = i.Data.cadasterNumber,
                    isPreferential = i.Data.isPreferential, 
                    

                    createdDt = i.System.createdDt.ToShortDateString(),
                    updatedDt = i.System.updatedDt == null ? String.Empty : i.System.updatedDt.Value.ToShortDateString(),

                    CanUpdate = crud, 
                    CanDelete = crud   
                }).ToArray();
                
                //Разрешение на создание нового объекта
                scope.CanCreate = crud;

                //Сохранение номера текущей страницы в фильтре
                scopefilter.page = scope.page;  

                scope.itemsCount = result.Count();
                scope.filter = scopefilter;

                //Возврат результата на форму
                return Json(scope, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }

        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("item/default")]
        [AuthorizeUser(Expression = "biz.request.crud")]
        public ActionResult GetDefault()
        {
            try
            {
                var user = SecurityService.CurrentAccount;
                var model = new RequestModel(new Request() 
                { 
                    branchId = (user != null) ? user.branchId : null, 
                    LocationType = ELocationType.ltCity, 
                    VoltageLevel = EVoltageLevel.vlLV, 
                    ReliabilityCategory = EReliabilityCategory.rc3 
                }) { settings = this.Settings };

                //Поиск и инициализация расширения модели данных
                var extension = GetExtensionDefinition(EBizDocumentTypes.Request);
                if (extension != null)
                    model.extension = new MetaObjectInstanceModel(extension.instance_id);

                return Json(model, JsonRequestBehavior.AllowGet);
            } 
            catch(Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPut]
        [Route("item")]
        [AuthorizeUser(Expression = "biz.request.crud")]
        public ActionResult Create()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                RequestDocument doc = new RequestDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();

                var model = JsonConvert.DeserializeObject<RequestModel>(GetRequestContent());
                model.SaveDocument(doc);

                //Сохранение данных расширения модели
                if (model.extension.hasDefinition())
                    model.extension.SaveDocument(doc.ExtensionDocument);

                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("wfs")]
        [AuthorizeUser(Expression = "")]
        public ActionResult GetWFS(string filter, string layer = "")
        {
            try
            {
                if (string.IsNullOrWhiteSpace(layer))
                    layer = Settings.layerLinesLVName;

                return Json(new { data = GetWFSData(this.Settings.wfsUrl, layer, filter) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        [HttpGet]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Read(Guid id)
        {
            try
            {
                RequestDocument doc = new RequestDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                var model = new RequestModel(doc) { settings = this.Settings };

                //Чтение/создание данных расширения модели
                if (doc.ExtensionDefinition != null)
                    model.extension = new MetaObjectInstanceModel(doc.ExtensionDocument);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "biz.request.crud")]
        public ActionResult Update(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                RequestDocument doc = new RequestDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                var model = JsonConvert.DeserializeObject<RequestModel>(GetRequestContent());
                model.SaveDocument(doc);

                //Сохранение данных расширения модели
                if (model.extension.hasDefinition())
                    model.extension.SaveDocument(doc.ExtensionDocument);

                doc.Save();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("item/{id}")]
        [AuthorizeUser(Expression = "biz.request.crud")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                RequestDocument doc = new RequestDocument();
                doc.Identity = SecurityHelper.GetCurrentUser();
                doc.Read(id);

                doc.Delete();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}