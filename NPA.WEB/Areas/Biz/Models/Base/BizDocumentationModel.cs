﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
 

//Общая функциональность модели документации организации (см. DAL)
namespace NPS.WEB.Areas.Models
{
    //Расширяемая модель (поддержка расширений на базе метаобъектов)
    public class ExtensibleItemModel : ItemModel
    {
        public ExtensibleItemModel()
            :base()
        {

        }
        public ExtensibleItemModel(BizDocument document)
            :base(document)
        {
        }

        internal MetaObjectInstanceModel extension = new MetaObjectInstanceModel();

        public string extensionName
        {
            get
            {
                return this.extension.name;
            }
            set
            {
                this.extension.name = value;
            }
        }

        public Guid extensionDefinitionId
        {
            get
            {
                return this.extension.definitionId;
            }
            set
            {
                this.extension.definitionId = value;
            }
        }

        //Сериализованный экземпляр метаобъекта
        public object extensionInstance
        {
            get
            {
                return this.extension.instance;
            }
            set
            {
                this.extension.instance = value;
            }
        }
    }
}