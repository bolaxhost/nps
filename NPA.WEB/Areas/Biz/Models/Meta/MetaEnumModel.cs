﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
using NPS.RDBDocumentModel;      

namespace NPS.WEB.Areas.Models
{
    public class MetaEnumModel : MetaFeatureModel
    {
        public MetaEnumModel() { }
        public MetaEnumModel(MetaEnum data, ICollection<MetaEnumValue> valuesCollection) 
        { 
            Load(data);

            if (valuesCollection == null)
            {
                values = new MetaEnumValueModel[0];   
                return;
            }

            values = valuesCollection.Select(p => new MetaEnumValueModel(p)).ToArray();   
        }

        public MetaEnumValueModel[] values { get; set; }

        public void SaveDocument(MetaEnumDocument doc)
        {
            this.Save(doc.Main);
            this.SaveCollection(values, doc.Values, doc);
        }
    }

    public class MetaEnumValueModel : MetaFeatureCollectionItemModel 
    {
        public MetaEnumValueModel() { }
        public MetaEnumValueModel(MetaEnumValue data) 
        { 
            Load(data);
        }

    }

    public class MetaEnumScope : ListScopeModel<MetaEnumItem, MetaEnumFilter>
    {
    }

    public class MetaEnumItem : MetaFeatureScopeItem
    {
    }

    public class MetaEnumFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : MetaEnum
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.displayName.ToLower().Contains(this.criteria.ToLower())
                    || i.systemName.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }

}