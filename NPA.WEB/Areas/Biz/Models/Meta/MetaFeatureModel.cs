﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;    

namespace NPS.WEB.Areas.Models
{
    public class MetaFeatureModel : ItemModel
    {
        public string systemName { get; set; }
    }

    public class MetaFeatureCollectionItemModel : CollectionItemModel
    {
        public string systemName { get; set; }
    }

    public class MetaFeatureScopeItem : ListScopeItem
    {
        public string systemName { get; set; }
    }
}