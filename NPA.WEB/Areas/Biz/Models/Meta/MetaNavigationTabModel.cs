﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
using NPS.RDBDocumentModel;      


namespace NPS.WEB.Areas.Models
{
    public class MetaNavigationTabModel : MetaFeatureModel
    {
        public MetaNavigationTabModel() { }
        public MetaNavigationTabModel(MetaNavigationTab data, ICollection<MetaNavigationTabSection> valuesCollection) 
        { 
            Load(data);

            if (valuesCollection == null)
            {
                values = new MetaNavigationTabSectionModel[0];   
                return;
            }

            values = valuesCollection.Select(p => new MetaNavigationTabSectionModel(p)).ToArray();   
        }

        public MetaNavigationTabSectionModel[] values { get; set; }

        public void SaveDocument(MetaNavigationTabDocument doc)
        {
            this.Save(doc.Main);
            this.SaveCollection(values, doc.Values, doc);
        }
    }

    public class MetaNavigationTabSectionModel : MetaFeatureCollectionItemModel 
    {
        public MetaNavigationTabSectionModel() { }
        public MetaNavigationTabSectionModel(MetaNavigationTabSection data) 
        { 
            Load(data);
        }

    }

    public class MetaNavigationTabScope : ListScopeModel<MetaNavigationTabItem, MetaNavigationTabFilter>
    {
    }

    public class MetaNavigationTabItem : MetaFeatureScopeItem
    {
    }

    public class MetaNavigationTabFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : MetaNavigationTab
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.displayName.ToLower().Contains(this.criteria.ToLower())
                    || i.systemName.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }

}