﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using NPS.WEB.Models;
using NPS.DAL;
using NPS.RDBDocumentModel;      


namespace NPS.WEB.Areas.Models
{
    public class MetaObjectInstanceModel : ItemModel
    {
        public MetaObjectInstanceModel() 
        {
        }

        public MetaObjectInstanceModel(Guid _definitionId)
        {
            this.definitionId = _definitionId;
            this.instance = this.Exemplar.SerializeExemplar();
        }

        public MetaObjectInstanceModel(MetaObjectInstanceDocument doc)
        {
            Load(doc.Main);

            this.Exemplar.Load(doc);  
            this.instance = this.Exemplar.SerializeExemplar();  
        }

        private MetaObjectDocument _definition = null;
        internal MetaObjectDocument Definition 
        {
            get
            {
                if (!this.hasDefinition())
                    return null;

                if (_definition == null)
                {
                    _definition = new MetaObjectDocument();
                    _definition.Identity = SecurityHelper.GetCurrentUser();
                    _definition.Read(this.definitionId);  
                }

                return _definition; 
            }
        }

        private MetaObjectExemplar _exemplar = null;
        internal MetaObjectExemplar Exemplar
        {
            get
            {
                if (!this.hasDefinition())
                    return null;

                if (_exemplar == null)
                    _exemplar = new MetaObjectExemplar(Definition);

                return _exemplar; 
            }
        }

        public MetaObjectExemplar GetExemplar()
        {
            if (!this.hasDefinition())
                return null;

            return this.Exemplar;  
        }

        public void SaveDocument(MetaObjectInstanceDocument doc)
        {
            //Сохранение главного раздела
            this.Save(doc.Main);

            //Конвертация JObject в Object
            if (this.instance.GetType().Name == "JObject")
                this.instance = ((JObject)this.instance).ToObject(this.Exemplar.CreateModel().GetType());

            //Сохранение свойств экземпляра метаобъекта
            this.Exemplar.DeserializeExemplar(this.instance);
            this.Exemplar.Save(doc);
        }

        public string name { get; set; }
        public Guid definitionId { get; set; }

        public bool hasDefinition()
        {
            return this.definitionId != Guid.Empty; 
        }

        //Сериализованный экземпляр метаобъекта
        public object instance { get; set; }
    }

    public class MetaObjectInstanceScope : ListScopeModel<MetaObjectInstanceItem, MetaObjectInstanceFilter>
    {
    }

    public class MetaObjectInstanceItem : ListScopeItem
    {
        public string name { get; set; }
        public Guid definitionId { get; set; }
    }

    public class MetaObjectInstanceFilter : ListScopeFilter
    {
        public Guid? definitionId { get; set; }

        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : MetaObjectInstance
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.name.ToLower().Contains(this.criteria.ToLower()));

            if (this.definitionId != null)
                data = data.Where(i => i.definitionId == this.definitionId); 

            return data;
        }
    }

    public class MetaObjectExemplarModel
    {
        public MetaObjectExemplar Exemplar { get; set; }

        private IEnumerable<MetaObjectPropertyInstance> _Properties = null;
        public IEnumerable<MetaObjectPropertyInstance> Properties 
        { 
            get
            {
                if (_Properties == null)
                    _Properties = this.Exemplar.propertyInstances;

                return _Properties; 
            }
            set
            {
                _Properties = value;
            }
        }
        public string formName { get; set; }
        public string instanceModelName { get; set; }
        public bool isRestrictedMode { get; set; }

        public MetaObjectExemplarModel(MetaObjectExemplar exemplar)
        {
            this.Exemplar = exemplar;
            
            this.formName = "form";
            this.instanceModelName = "instance";
            this.isRestrictedMode = false;
        }
        public MetaObjectExemplarModel(MetaObjectExemplar exemplar, IEnumerable<MetaObjectPropertyInstance> properties)
            : this(exemplar)
        {
            this.Properties = properties; 
        }
        public MetaObjectExemplarModel(MetaObjectExemplarModel model, IEnumerable<MetaObjectPropertyInstance> properties)
            : this(model.Exemplar, properties)
        {
            this.formName = model.formName;
            this.instanceModelName = model.instanceModelName;
            this.isRestrictedMode = model.isRestrictedMode;
        }
    }

}