﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
using NPS.RDBDocumentModel;      
   

namespace NPS.WEB.Areas.Models
{
    public class MetaObjectModel : MetaFeatureModel
    {
        public MetaObjectModel() 
        {
        }

        public MetaObjectModel(MetaObject data, ICollection<MetaObjectProperty> propertiesCollection)
        { 
            Load(data);

            if (propertiesCollection == null)
            {
                properties = new MetaObjectPropertyModel[0];   
                return;
            }

            properties = propertiesCollection.Select(p => new MetaObjectPropertyModel(p)).ToArray();
        }

        public MetaObjectPropertyModel[] properties { get; set; }

        public void SaveDocument(MetaObjectDocument doc)
        {
            this.Save(doc.Main);
            this.SaveCollection(properties, doc.Properties, doc);
        }

        public MetaObjectPropertyModel property = new MetaObjectPropertyModel();
        public bool isAbstract { get; set; }

        //Базовый объект
        public Guid? parentId { get; set; }

        //Тип расширения
        public int extensionType { get; set; }

        //Подтип расширения
        public int extensionSubtype { get; set; }
    }

    public class MetaObjectPropertyModel : MetaFeatureCollectionItemModel 
    {
        public MetaObjectPropertyModel() 
        {
            isVisible = true;
            isRequired = false;
            propertyType = 0;
        }
        public MetaObjectPropertyModel(MetaObjectProperty data) 
        { 
            Load(data);
        }

        public bool isVisible { get; set; }
        public bool isRequired { get; set; }
        public int? propertyType { get; set; }
        public int? propertyClassification { get; set; }
        public string valueFormat { get; set; }

        public double? minRealValue { get; set; }
        public double? maxRealValue { get; set; }

        public int? minIntegerValue { get; set; }
        public int? maxIntegerValue { get; set; }

        public int? maxLength { get; set; }

        //Связанное перечисление
        public Guid? enumId {get; set;}

        //Связанное (внедренное) определение объекта
        public Guid? nestedObjectId { get; set; }

        //Раздел
        public Guid? tabId { get; set; }

        //Подраздел
        public Guid? tabSectionId { get; set; }
    }

    public class MetaObjectScope : ListScopeModel<MetaObjectItem, MetaObjectFilter>
    {
    }

    public class MetaObjectItem : MetaFeatureScopeItem
    {
    }

    public class MetaObjectFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : MetaObject
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.displayName.ToLower().Contains(this.criteria.ToLower())
                    || i.systemName.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }

}