﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.WEB.Models;
using NPS.DAL;
      

namespace NPS.WEB.Areas.Models
{
    public class RequestModel : ExtensibleItemModel
    {
        public SettingsModel settings { get; set; }

        public RequestModel() { }
        public RequestModel(Request data) 
        { 
            Load(data);
        }

        public RequestModel(RequestDocument data)
        {
            Load(data.Main);
            Files.Load(data);
        }

        public void SaveDocument(RequestDocument doc)
        {
            this.Save(doc.Main);
            this.SaveFiles(doc);
        }

        public int? branchId { get; set; }

        public string number { get; set; }
        public string applicant { get; set; }
        public string cadasterNumber { get; set; }
        public string applicantAddress { get; set; }
        public double powerRequested { get; set; }
        public int voltageLevel { get; set; }
        public int reliabilityCategory { get; set; }
        public int locationType { get; set; }
        public string comments { get; set; }

        public bool isPreferential { get; set; }

        public string publicMapReference { get; set; }
        public string nativeMapContext { get; set; }
        public string regionGeometry { get; set; }
        public string bufferGeometry { get; set; }


        public SimpleEnumerationItem<int>[] voltageLevels = typeof(EVoltageLevel).ToSelectOptions();
        public SimpleEnumerationItem<int>[] reliabilityCategories = typeof(EReliabilityCategory).ToSelectOptions();
        public SimpleEnumerationItem<int>[] locationTypes = typeof(ELocationType).ToSelectOptions();
    }

    public class RequestScope : ListScopeModel<RequestItem, RequestFilter>
    {
    }

    public class RequestItem : ListScopeItem
    {
        public string number { get; set; }
        public string applicant { get; set; }
        public string applicantAddress { get; set; }
        public string cadasterNumber { get; set; }
        public int? branchId { get; set; }
        public bool isPreferential { get; set; }
    }

    public class RequestFilter : ListScopeFilter
    {
        public new IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : Request
        {
            if (!string.IsNullOrWhiteSpace(this.criteria))
                data = data.Where(i => i.number.ToLower().Contains(this.criteria.ToLower())
                    || i.cadasterNumber.ToLower().Contains(this.criteria.ToLower())
                    || i.applicant.ToLower().Contains(this.criteria.ToLower())
                    || i.applicantAddress.ToLower().Contains(this.criteria.ToLower()));

            return data;
        }
    }
}