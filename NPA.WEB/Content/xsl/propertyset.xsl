<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" />
  <xsl:template match="/">
    <div class="propertySet">
      <xsl:apply-templates select="*" />
    </div>
  </xsl:template>
  <xsl:template match="entity">
    <xsl:call-template name="entity" />
  </xsl:template>
  <xsl:template name="entity">
    <h3>
      <xsl:value-of select="@description" />
    </h3>
    <table>
      <xsl:apply-templates select="*" />
    </table>
  </xsl:template>
  <xsl:template match="attributes">
    <xsl:call-template name="attributes" />
  </xsl:template>
  <xsl:template match="sections">
    <xsl:call-template name="sections" />
  </xsl:template>
  <xsl:template name="attributes">
    <xsl:for-each select="*">
      <xsl:call-template name="row" />
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="sections">
    <xsl:for-each select="*">
      <xsl:call-template name="section" />
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="section">
    <tr class="nested">
      <th>
        <xsl:value-of select="name()" />
      </th>
      <td>
        <xsl:for-each select="*">
          <xsl:call-template name="entity" />
        </xsl:for-each>
      </td>
    </tr>
  </xsl:template>
  <xsl:template name="row">
    <tr>
      <th>
        <xsl:value-of select="name()" />
      </th>
      <td>
        <xsl:value-of select="text()" />
      </td>
    </tr>
  </xsl:template>
</xsl:stylesheet>