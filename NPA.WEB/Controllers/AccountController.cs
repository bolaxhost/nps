﻿using System; 
using System.Web.Mvc;
using Newtonsoft.Json;

using NPS.DAL;
using NPS.WEB.Core;
using NPS.WEB.Models;
using NPS.WEB.Security;

namespace NPS.WEB.Controllers
{
    public class AccountController : BaseController
    {
        private IAuthenticationProvider _authenticationProvider = null;

        public AccountController(IAuthenticationProvider provider)
        {
            _authenticationProvider = provider;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("LogIn");
        }



        [HttpGet]
        [Route("api/admin/login-form")]
        public ActionResult LoginForm()
        {
            return View("LogInForm");
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return View("Index");
        }




        //Создание данных по умолчанию
        [HttpGet]
        [Route("api/admin/login")]
        public ActionResult GetLoginModel()
        {
            try
            {
                return Json(new LoginModel(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPost]
        [Route("api/admin/login")]
        public ActionResult TryToLogIn()
        {
            try
            {
                var login = JsonConvert.DeserializeObject<LoginModel>(GetRequestContent());

                //Попытка зайти без пароля по учетной записи Windows
                if (login.login == SecurityHelper.GetWindowsUserName() && _authenticationProvider.AuthenticateByIntranetAccount())
                    return EmptySuccessResult();

                //Попытка зайти с паролем по внутренней учетной записи
                if (_authenticationProvider.Authenticate(login.login, login.password, login.rememberMe))
                    return EmptySuccessResult();

                return ErrorResult("Неверные логин или пароль или учетная запись заблокирована!");
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }


        //Создание данных по умолчанию
        [HttpGet]
        [Route("api/admin/account-info")]
        public ActionResult GetAccountInfoModel()
        {
            try
            {
                return Json(new AccountInfoModel(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        //Создание нового документа
        [HttpPost]
        [Route("api/admin/logout")]
        public ActionResult LogOut()
        {
            try
            {
                _authenticationProvider.SignOut();

                return EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}
