﻿using System; 
using System.Web.Mvc;
using Newtonsoft.Json;


using NPS.WEB.Core;
using NPS.WEB.Models;
using NPS.WEB.Security;
using NPS.WEB.Filters;

namespace NPS.WEB.Controllers
{
    [RoutePrefix("api/biz/dashboard")]
    public class DashboardController : BaseAuthorizedController
    {
        public DashboardController()
        {
        }

        [Route("view")]
        [AuthorizeUser(Expression = "")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("main-form")]
        public ActionResult DashboardMainForm()
        {
            return View("Main");
        }

        
        [HttpGet]
        [Route("profile")]
        public ActionResult GetDashboardModel()
        {
            try
            {
                return Json(new DashboardModel(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}
