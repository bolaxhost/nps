﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.IO;

using NPS.DAL;


using NPS.WEB.Core;
using NPS.WEB.Filters;
using NPS.WEB.Security;
using NPS.WEB.Models;
using NPS.Utilities.Cryptography;

namespace NPS.WEB.Controllers
{

    public class FilesController : BaseAuthorizedController
    {
        public FilesController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload()
        {
            int _maxFileSize = 10;

            var uow = new BizDocument();
            uow.Identity = SecurityHelper.GetCurrentUser();

            var status = new ViewDataUploadFilesResult();
            var headers = Request.Headers;

            if (Request.Files.Count <= 0)
                return new EmptyResult();

            if (Request.Files[0].InputStream.Length > Math.Pow(1024, 2) * _maxFileSize)
                return Json(new { isUploaded = false, message = string.Format("{0} ({1}Мб)", "Размер выбранного файла превышает максимально допустимый", _maxFileSize) }, "text/html");
            if (Request.Files[0].InputStream.Length == 0)
                return Json(new { isUploaded = false, message = "Размер файла слишком мал." }, "text/html");

            if (string.IsNullOrEmpty(headers["X-File-Name"]))
            {
                UploadWholeFile(Request, status);
            }
            else
            {
                UploadPartialFile(headers["X-File-Name"], Request, status);
            }

            if (headers["Content-Range"] == null)
            {
                var fileSize = Math.Round(Convert.ToDecimal(Request.Files[0].ContentLength) / 1024);
                string fileName = string.Format("{0} ({1} KB)", Request.Files[0].FileName, fileSize);

                var file = SaveFile(uow, status.name, Request.Files[0].FileName, Request.Files[0].ContentType);
                return Json(new { isUploaded = true, message = "File uploaded", fileName = fileName, id = file.id, hash = file.hash, fileSize = fileSize, attachmentName = fileName }, "text/html");
            }

            var contentRange = headers["Content-Range"];
            var contentRangePartial = contentRange.Split(new[] { "/" }, StringSplitOptions.None);
            var contentRangeSizePartial = contentRangePartial[0].Split(new[] { "-" }, StringSplitOptions.None);

            var contentRangeTotal = contentRangePartial[1];
            var contentRangeSize = contentRangeSizePartial[1];

            if (Convert.ToDecimal(contentRangeTotal) - Convert.ToDecimal(contentRangeSize) == 1)
            {
                var fileSize = Math.Round(Convert.ToDecimal(contentRangeTotal) / 1024);
                var fileName = Request.Files[0].FileName;

                var file = SaveFile(uow, status.name, Request.Files[0].FileName, Request.Files[0].ContentType);
                return Json(new { isUploaded = true, message = "File uploaded", fileName = fileName, id = file.id, hash = file.hash, fileSize = fileSize, attachmentName = fileName }, "text/html");
            }

            return Json(status, "text/html");
        }

        [HttpGet]
        public FileResult Download(Guid id)
        {
            NPS.DAL.File file = new BizDocument().MyContext.DbStorage.Set<NPS.DAL.File>().First(f => f.id == id);

            if (file.fileContent == null)
                return File(file.fileReference, file.fileContentType, file.fileName);

            return File(file.fileContent, file.fileContentType, file.fileName);
        }

        #region Files API

        public static NPS.DAL.File CreateFile(BizDocument uow, string contentType, byte[] content, string name, string extenstion, string description)
        {
            NPS.DAL.File newFile = CreateFile(contentType, content, name, extenstion, description, uow.Identity);
            newFile.fileContent = content;

            uow.MyContext.DbStorage.Set<NPS.DAL.File>().Add(newFile);
            uow.MyContext.DbStorage.SaveChanges();  

            return newFile;
        }

        public static NPS.DAL.File CreateFile(BizDocument uow, string contentType, byte[] content, string reference, string name, string extenstion, string description)
        {
            NPS.DAL.File newFile = CreateFile(contentType, content, name, extenstion, description, uow.Identity);
            newFile.fileReference = reference;

            uow.MyContext.DbStorage.Set<NPS.DAL.File>().Add(newFile);
            uow.MyContext.DbStorage.SaveChanges();  

            return newFile;
        }

        private static NPS.DAL.File CreateFile(string contentType, byte[] content, string name, string extenstion, string description, string user)
        {
            var hash = GostHash.GetHashBytes(content);

            NPS.DAL.File newFile = new NPS.DAL.File();
            newFile.id = Guid.NewGuid();
            newFile.fileContentType = contentType;
            newFile.fileName = name;
            newFile.fileExtension = extenstion;
            newFile.fileDescription = description;
            newFile.createdDt = DateTime.Now;
            newFile.createdBy = user;
            newFile.hash = hash;

            return newFile;
        }

        private void UploadWholeFile(HttpRequestBase request, ViewDataUploadFilesResult status)
        {
            var file = request.Files[0];
            var fileName = string.Format("{0}{1}", Guid.NewGuid().ToString(), Path.GetExtension(file.FileName));

            status.name = UploadPartialFile(fileName, file.InputStream);

        }

        private void UploadPartialFile(string fileName, HttpRequestBase request, ViewDataUploadFilesResult status)
        {
            if (request.Files.Count != 1)
                throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");

            var file = request.Files[0];
            var inputStream = file.InputStream;

            status.name = UploadPartialFile(fileName, inputStream);
        }

        public static string UploadPartialFile(string fileName, System.IO.Stream stream)
        {
            using (var storage = System.IO.IsolatedStorage.IsolatedStorageFile.GetMachineStoreForDomain())
            {
                using (var isoFileStream = new System.IO.IsolatedStorage.IsolatedStorageFileStream(fileName, System.IO.FileMode.Append, System.IO.FileAccess.Write, storage))
                {
                    var buffer = new byte[1024];

                    var l = stream.Read(buffer, 0, 1024);
                    while (l > 0)
                    {
                        isoFileStream.Write(buffer, 0, l);
                        l = stream.Read(buffer, 0, 1024);
                    }

                    isoFileStream.Flush();
                    isoFileStream.Close();
                }

                storage.Close();
            }
            return fileName;
        }

        public static byte[] GetFileFromIsolatedStorage(string fileName)
        {
            byte[] output;
            using (var storage = System.IO.IsolatedStorage.IsolatedStorageFile.GetMachineStoreForDomain())
            {
                using (var isoFileStream = storage.OpenFile(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    output = new byte[isoFileStream.Length];
                    isoFileStream.Read(output, 0, output.Length);
                    isoFileStream.Flush();
                    isoFileStream.Close();
                }

                storage.Close();
            }

            return output;
        }

        public static void CopyFileFromIsolatedStorage(string sourceFileName, string destinationFileName)
        {
            using (var storage = System.IO.IsolatedStorage.IsolatedStorageFile.GetMachineStoreForDomain())
            {
                using (var isoFileStream = storage.OpenFile(sourceFileName, System.IO.FileMode.Open))
                {
                    var contents = new System.IO.BinaryReader(isoFileStream).ReadBytes((int)isoFileStream.Length);
                    using (var bw = new System.IO.BinaryWriter(System.IO.File.Open(destinationFileName, System.IO.FileMode.Create)))
                    {
                        bw.Write(contents);
                        bw.Flush();
                        bw.Close();
                    }

                    isoFileStream.Close();
                }

                storage.Close();
            }
        }

        public static NPS.DAL.File SaveFile(BizDocument uow, string fileName, string origFileName, string contentType)
        {
            if (file_storageMode == 0)
            {
                return CreateFile(uow, contentType, GetFileFromIsolatedStorage(fileName), System.IO.Path.GetFileName(origFileName), System.IO.Path.GetExtension(fileName), null);
            }
            else
            {
                string filename = System.IO.Path.GetFileName(origFileName);
                var path = GetFilePath(filename);

                CopyFileFromIsolatedStorage(fileName, path);

                return CreateFile(uow, contentType, GetFileFromIsolatedStorage(fileName), path, System.IO.Path.GetFileName(origFileName), System.IO.Path.GetExtension(fileName), null);
            }
        }

        public static string GetFilePath(string filename)
        {
            string path = file_storagePath;

            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);

            return System.IO.Path.Combine(path, string.Format("{0}.{1}", Guid.NewGuid(), System.IO.Path.GetExtension(filename)));
        }
        #endregion

        private static string file_storagePath = "%TEMP%";
        private static int file_storageMode = 0;
    }



    public class ViewDataUploadFilesResult
    {
        public string name { get; set; }
    }
}
