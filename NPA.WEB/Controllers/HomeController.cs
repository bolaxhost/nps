﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using NPS.WEB.Security;

namespace NPS.WEB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (SecurityService.isAuthorized)
                return new RedirectResult("api/biz/dashboard/view");

            return new RedirectResult("api/biz/public/view");
        }

        public ActionResult Error(HandleErrorInfo errorInfo)
        {
            return View("Error", errorInfo);
        }

        public ActionResult AccessDenied()
        {
            return View("AccessDenied");
        }
    }
}
