﻿using System; 
using System.Web.Mvc;
using Newtonsoft.Json;

using NPS.DAL;
using NPS.WEB.Core;
using NPS.WEB.Models;
using NPS.WEB.Security;
using NPS.WEB.Filters;

namespace NPS.WEB.Controllers
{
    [RoutePrefix("api/biz/public")]
    public class PublicController : BaseController
    {
        public PublicController()
        {
        }

        [Route("view")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("main-form")]
        public ActionResult PublicMainForm()
        {
            return View("Main");
        }
    }
}
