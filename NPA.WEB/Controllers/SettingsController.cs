﻿using System; 
using System.Web.Mvc;
using Newtonsoft.Json;


using NPS.WEB.Core;
using NPS.WEB.Models;
using NPS.WEB.Security;
using NPS.WEB.Filters;

namespace NPS.WEB.Controllers
{
    [RoutePrefix("api/admin/settings")]
    public class SettingsController : BaseController
    {
        //Формы
        //Представление Single Page Application
        [HttpGet]
        [Route("view")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("edit-form")]
        public ActionResult EditForm()
        {
            return View("Edit");
        }

        //Создание данных по умолчанию
        [HttpGet]
        [Route("edit")]
        [AuthorizeUser(Expression = "adm.settings.crud")]
        public ActionResult GetSettings()
        {
            try
            {
                return Json(this.Settings, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("edit")]
        [AuthorizeUser(Expression = "adm.settings.crud")]
        public ActionResult SetSettings()
        {
            if (!ModelState.IsValid)
            {
                return base.EmptySuccessResult();
            }

            try
            {
                SaveSettings(JsonConvert.DeserializeObject<SettingsModel>(GetRequestContent()));
                return base.EmptySuccessResult();
            }
            catch (Exception ex)
            {
                return ErrorResult(ex);
            }
        }
    }
}
