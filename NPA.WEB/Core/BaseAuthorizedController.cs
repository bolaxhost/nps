﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NPS.WEB.Security;

using NPS.DAL;
using NPS.RDBDocumentModel;


using NPS.WEB.Filters;
using NPS.WEB.Models;
 
namespace NPS.WEB.Core
{
    public class BaseAuthorizedController : BaseController
    {
        public void GotoObjectVersion(Guid id, int? version = null)
        {
            if (version == null)
                return;

            var uow = new BizDocument();
            var system = uow.GetData<SystemSection>().FirstOrDefault(i => i.instance_id == id && i.majorVersion == version);

            if (system == null)
                return;

            GotoVersion(system); 
        }

        public void GotoVersion(DocumentAttributes data)
        {
            DateTime? snapshot = null;
            if (data.actual_till != null)
                snapshot = data.actual_till;

            //Свойства текущего пользователя
            UserEnvironment _bag = this.UserEnvironment;
            _bag["npavmds_snapshot_default"] = _bag["npavmds_snapshot"];
            _bag["npavmds_snapshot"] = snapshot;
        }

        public void ResetVersion()
        {
            UserEnvironment _bag = this.UserEnvironment;
            _bag["npavmds_snapshot"] = _bag["npavmds_snapshot_default"];
        }

        public MetaObject GetExtensionDefinition(BizDocument uow, EBizDocumentTypes type, int? subType = null)
        {
            return BizDocument.GetExtensionDefinition(uow, type, subType);  
        }

        public MetaObject GetExtensionDefinition(EBizDocumentTypes type, int? subType = null)
        {
            BizDocument uow = new BizDocument();
            uow.Identity = SecurityHelper.GetCurrentUser();

            return GetExtensionDefinition(uow, type, subType); 
        }

        private UserEnvironment _UserEnvironment;
        public UserEnvironment UserEnvironment
        {
            get
            {
                if (_UserEnvironment == null)
                    _UserEnvironment = new UserEnvironment(SecurityHelper.GetCurrentUser());

                return _UserEnvironment; 
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                var controllerName = filterContext.RouteData.Values["controller"] as String;
                var actionName = filterContext.RouteData.Values["action"] as String;
                
                var model = new HandleErrorInfo(
                    filterContext.Exception,
                    controllerName,
                    actionName);

                var result = new ViewResult
                {
                    ViewName = "Error",
                    MasterName = string.Empty,
                    ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                    TempData = filterContext.Controller.TempData
                };

                filterContext.Result = result;
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            }
        }
    }
}