﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;

using NPS.WEB.Security;
using NPS.RDBDocumentModel.IoC;  

namespace NPS.WEB.Core
{
    public class ControllerFactory : DefaultControllerFactory 
    {
        private DefaultServiceFactory _classFactory = new DefaultServiceFactory();

        public ControllerFactory()
            : base()
        {
            AddBindings(); 
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)_classFactory.Create(controllerType);
        }

        private void AddBindings()
        {
            _classFactory.Bind<AuthenticationProvider, IAuthenticationProvider>();
        }
    }
}