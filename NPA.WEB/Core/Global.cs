﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPS.DAL;
 
using NPS.WEB.Security;

namespace NPS.WEB.Core
{
    public static class Constants
    {
        public static List<int> PAGE_SIZES = new List<int>() { 10, 25, 50, 100 };
    }

    public static class ViewServices
    {
        private static int _PageSize = 0;
        public static int GetPageSize(int PageSize = 0)
        {
            if (_PageSize > 0 && _PageSize == PageSize)
                return _PageSize;

            UserEnvironment _bag = GetUserEnvironment();

            if (_bag == null)
                return 10;

            if (PageSize == 0)
            {
                if (_bag["_PageSize"] == null)
                    _bag["_PageSize"] = 10;
            }
            else
                _bag["_PageSize"] = PageSize;

            _PageSize = (int)_bag["_PageSize"];
            return _PageSize;
        }

        public static UserEnvironment GetUserEnvironment()
        {
            string currentUser = SecurityHelper.GetCurrentUser();

            if (string.IsNullOrWhiteSpace(currentUser))
                return null;

            return new UserEnvironment(currentUser);
        }
    }
}