﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Configuration;
using System.Security.Principal;

using NPS.DAL; 
 

namespace NPS.WEB.Security
{
    public static class SecurityService
    {

        public static string CurrentUser
        {
            get
            {
                return SecurityHelper.GetCurrentUser(); 
            }
        }

        public static UserAccount CurrentAccount
        {
            get
            {
                return SecurityHelper.GetCurrentUserAccount();
            }
        }

        public static bool isAuthorized
        {
            get
            {
                return SecurityHelper.isAuthorized;
            }
        }

        public static bool isExpressionPermitted(string expression)
        {
            if (isSupervisor)
                return true;

            return SecurityHelper.isExpressionPermitted(expression);
        }

        public static bool isOperationPermitted(string operation)
        {
            if (isSupervisor)
                return true;

            return SecurityHelper.isOperationPermitted(operation);
        }

        public static bool isSupervisor
        {
            get
            {
                return isAuthorized && CurrentAccount == null;
            }
        }
    }

    public interface IAuthenticationProvider
    {
        bool Authenticate(string user, string password, bool rememberMe = false);
        bool AuthenticateByIntranetAccount();
        void SignOut();
    }

    public class AuthenticationProvider : IAuthenticationProvider
    {
        public bool AuthenticateByIntranetAccount()
        {
            string user = SecurityHelper.GetUserByIntranetAccount();

            if (string.IsNullOrWhiteSpace(user))
                return false;

            FormsAuthentication.SetAuthCookie(user, false);
            return true;
        }

        public bool Authenticate(string user, string password, bool rememberMe = false)
        {
            if (ValidateUser(user, password))
            {
                FormsAuthentication.SetAuthCookie(user, rememberMe);
                return true;
            }

            return false; 
        }

        private bool ValidateUser(string user, string password)
        {
            var account = SecurityHelper.GetUserAccount(user);  

            if (account == null)
                return (user == "sys" && password == "superuser");

            if (account.isBlocked)
                return false;

            if (account.accountIsValidTill != null && account.accountIsValidTill < DateTime.Now)
                return false;

            return account.IsValidPassword(password);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut(); 
        }
    }
}