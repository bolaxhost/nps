﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

 
using NPS.WEB.Security;

namespace NPS.WEB.Filters
{
    public class AuthorizeUserAttribute : AuthorizeAttribute 
    {
        public string Expression { get; set; }
        public ActionResult UnauthorizedResult { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return SecurityService.isExpressionPermitted(Expression);     
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                if (UnauthorizedResult == null)
                    filterContext.Result = new RedirectResult(new UrlHelper(filterContext.RequestContext).Action("AccessDenied", "Home", new {area = ""}));
                else
                    filterContext.Result = UnauthorizedResult;
            }
        }
    }
}