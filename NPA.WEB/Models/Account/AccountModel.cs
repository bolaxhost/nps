﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.DAL; 
 

namespace NPS.WEB.Models
{
    public class LoginModel
    {
        public string login { get; set; }
        public string password { get; set; }
        public bool rememberMe { get; set; }
        public bool passwordIsOptional { get; set; }

        public LoginModel()
        {
            this.login = SecurityHelper.GetWindowsUserName();
            this.passwordIsOptional = !string.IsNullOrWhiteSpace(this.login); 
        }
    }

    public class AccountInfoModel
    {
        public Guid? id { get; set; }
        public string login { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public bool isAuthorized { get; set; }
        public bool isSupervisor { get; set; }

        public AccountInfoModel()
        {
            this.login = SecurityHelper.GetCurrentUser();  
            this.isAuthorized = !string.IsNullOrWhiteSpace(this.login); 
 
            if (this.isAuthorized)
            {
                var account = SecurityHelper.GetUserAccount(this.login);
                if (account != null)
                {
                    this.id = account.id;
                    this.name = account.name;
                    this.operations = account.Roles.SelectMany(r => r.Operations).Select(o => o.code).ToArray(); 
                }
            }

            this.displayName = string.IsNullOrWhiteSpace(this.name) ? this.login : this.name;
            this.isSupervisor = Security.SecurityService.isSupervisor;   
        }

        public string[] operations { get; set; }
    }
}