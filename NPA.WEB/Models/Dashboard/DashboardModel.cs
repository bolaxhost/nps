﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.DAL; 

using NPS.WEB.Security;  

namespace NPS.WEB.Models
{
    public class DashboardModel
    {
        public Guid? userId { get; set; }
        public string userName { get; set; }
        public int? department { get; set; }
        public string bizDescription { get; set; }
        public int requestsCount { get; set; }

        public DashboardModel() 
        {
            var userAccount = SecurityHelper.GetCurrentUserAccount();
            this.userName = userAccount == null ? SecurityHelper.GetCurrentUser() : (string.IsNullOrWhiteSpace(userAccount.name) ? userAccount.login : userAccount.name);

            var requests = new BizDocument().GetActualData<Request>();

            if (userAccount != null)
            {
                this.userId = userAccount.instance_id;  
                this.department = userAccount.branchId;

                foreach (var role in userAccount.Roles)
                    this.bizDescription += string.IsNullOrWhiteSpace(this.bizDescription) ? role.name : string.Format("; {0}", role.name);

                if (this.department != null && !SecurityService.isOperationPermitted("biz.scope.all"))
                    requests = requests.Where(i => i.branchId == this.department);
            }
            else if (SecurityService.isSupervisor)
                this.bizDescription = "Супервизор";

            this.requestsCount = requests.Count(); 
        }
    }
}