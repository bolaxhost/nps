﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.ComponentModel;

namespace NPS.WEB.Models
{
    //Возможно, целесообразно перенести в общую библиотеку infrastructure
    public static class Extensions
    {
        public static SimpleEnumerationItem<int>[] ToSelectOptions(this Type type, int[] except = null)
        {
            if (!type.IsEnum)
                throw new InvalidOperationException("enum expected");

            var values = Enum.GetValues(type).Cast<object>();

            if (except != null)
                values = values.Where(i => !except.Contains((int)i));

            return values.Select(i => new SimpleEnumerationItem<int>() { id = (int)i, name = i.GetDescription() }).ToArray();
        }

        public static string GetDescription(this object enumerationValue)
        {
            Type type = enumerationValue.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");
            }

            MemberInfo[] memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return enumerationValue.ToString();

        }
        public static KeyValuePair<int, string>[] ToKeyValuePairs(this Type type)
        {
            if (!type.IsEnum)
                throw new InvalidOperationException("enum expected");

            return Enum.GetValues(type).Cast<object>().Select(i => new KeyValuePair<int, string>((int)i, i.GetDescription())).ToArray();
        }
    }
}