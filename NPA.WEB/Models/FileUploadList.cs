﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;

using NPS.DAL;   
  

namespace NPS.WEB.Models
{
    public class BizFileModel : ItemModel
    {
        public Guid? attachmentId { get; set; }
        public string attachmentName { get; set; }

        public string fileName { get; set; }
        public string fileExtension { get; set; }
        public string fileDescription { get; set; }

        public decimal? fileSize { get; set; }
        public string hash { get; set; }

        public BizFileModel()
        {
        }

        public BizFileModel(NPS.DAL.Attachment attachment)
            : this()
        {
            var file = attachment.File;

            this.Load(file);
            this.fileSize = GetFileSize(file);

            this.attachmentId = attachment.id; 
            this.attachmentName = attachment.attachmentName;
        }

        public static decimal GetFileSize(NPS.DAL.File file)
        {
            if (file.fileContent != null)
                return Math.Round(Convert.ToDecimal(file.fileContent.Length) / 1024);
            else
            {
                var fileInfo = new System.IO.FileInfo(file.fileReference);
                if (!fileInfo.Exists)
                    return 0;

                return Math.Round(Convert.ToDecimal(fileInfo.Length) / 1024);
            }
        }
    }
    public class BizFileList
    {
        public BizFileModel[] ExistingFiles { get; set; }
        public BizFileModel[] AddedFiles { get; set; }
        public Guid[] DeletedFiles { get; set; }

        public BizFileList() 
        {
            ExistingFiles = new BizFileModel[0];
            AddedFiles = new BizFileModel[0];
            DeletedFiles = new Guid[0];
        }

        public BizFileList(BizDocument uow)
            : this()
        {
            Load(uow);
        }

        public void Load(BizDocument uow)
        {
            var existingFiles = new List<BizFileModel>();
            foreach (var attachment in uow.Attachments)
                existingFiles.Add(new BizFileModel(attachment));

            this.ExistingFiles = existingFiles.ToArray();  
        }

        public void Load(BizDocument uow, Guid documentId)
        {
            var existingFiles = new List<BizFileModel>();
            foreach (var attachment in uow.GetActualData<Attachment>().Where(a => a.instance_id == documentId))
                existingFiles.Add(new BizFileModel(attachment));

            this.ExistingFiles = existingFiles.ToArray();
        }
    }
}