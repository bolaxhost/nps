﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Вспомогательные модели для формирования форм на стороне сервера
namespace NPS.WEB.Models
{
    //Обобщенная модель input на форме
    public abstract class InputModel
    {
        //Подпись
        public string label { get; set; }
        public string placeholder { get; set; }

        //Обязательность
        public bool required { get; set; }
        public string requiredWarning { get; set; }

        //Форма
        public string formName { get; set; }
        public abstract string formVariableName
        {
            get;
        }

        public bool readOnly { get; set; }

        private string _readonlyScopeName = "readonly";
        public string ReadonlyScopeName
        {
            get { return _readonlyScopeName; }
            set { _readonlyScopeName = value; }
        }
        public string changeExpression { get; set; }

        public InputModel(string _label, bool _required)
        {
            label = _label;

            required = _required;
            requiredWarning = string.Format("Значение '{0}' должно быть определено!", label);

            formName = "form";
        }
    }
    
    public abstract class VariableInputModel : InputModel
    {
        //Имя переменной
        public string variableName { get; set; }
        //Модель
        public string variableModel { get; set; }
        //Input Id
        public string variableInputId { get; set; }
        
        public override string formVariableName
        {
            get
            {
                return string.Format("{0}.{1}", this.formName, this.variableName);
            }
        }        

        public VariableInputModel(string _name, string _label, bool _required) : base(_label, _required)
        {
            bool isLocalVariable = _name.StartsWith("#");

            if (isLocalVariable)
                _name = _name.Substring(1); 

            variableName = _name.Replace(".", "_");

            if (isLocalVariable)
                variableModel = string.Format("{0}", _name);
            else
                variableModel = string.Format("data.{0}", _name);

            variableInputId = _name;
        }
    }

    public class TextInputModel : VariableInputModel
    {
        public int? minLength { get; set; }

        private string _minLengthWarning = "";
        public string minLengthWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_minLengthWarning))
                    _minLengthWarning = string.Format("Значение '{0}' должно быть не меньше {1}!", label, minLength);

                return _minLengthWarning;
            }
            set
            {
                _minLengthWarning = value;
            }
        }

        public int? maxLength { get; set; }

        private string _maxLengthWarning = "";
        public string maxLengthWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_maxLengthWarning))
                    _maxLengthWarning = string.Format("Значение '{0}' должно быть не больше {1}!", label, maxLength);

                return _maxLengthWarning;
            }
            set
            {
                _maxLengthWarning = value;
            }
        }

        public string mask { get; set; }

        private string _maskWarning = "";
        public string maskWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_maskWarning))
                    _maskWarning = string.Format("Значение '{0}' должно соответствовать шаблону \"{1}\"!", label, mask);

                return _maskWarning;
            }
            set
            {
                _maskWarning = value;
            }
        }


        public string matchField { get; set; }
        private string _matchWarning = "";
        private string matchFieldName { get; set; }
        public string matchWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_matchWarning))
                    _matchWarning = string.Format("Значение '{0}' должно совпадать со значением '{1}'!", label, matchFieldName);

                return _matchWarning;
            }
            set
            {
                _matchWarning = value;
            }
        }

        public TextInputModel(string _name, string _label, bool _required, string matchField = "", string matchFieldName = "")
            : base(_name, _label, _required)
        {
            this.matchField = matchField;
            this.matchFieldName = matchFieldName;
        }
    }
    public class NumberInputModel<T> : VariableInputModel
        where T : struct
    {
        public NumberInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }

        public T? minValue { get; set; }
        public T? maxValue { get; set; }

        private string _minWarning = "";
        public string minWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_minWarning))
                    _minWarning = string.Format("Значение '{0}' должно быть не меньше {1}!", label, minValue);

                return _minWarning;
            }
            set
            {
                _minWarning = value;
            }
        }

        private string _maxWarning = "";
        public string maxWarning
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_maxWarning))
                    _maxWarning = string.Format("Значение '{0}' должно быть не больше {1}!", label, maxValue);

                return _maxWarning;
            }
            set
            {
                _maxWarning = value;
            }
        }
    }
    public class IntegerInputModel : NumberInputModel<int>
    {
        public IntegerInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }
    }
    public class RealInputModel : NumberInputModel<double>
    {
        public RealInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }
    }
    public class CurrencyInputModel : NumberInputModel<decimal>
    {
        public CurrencyInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }
    }
    public class BooleanInputModel : VariableInputModel
    {
        public BooleanInputModel(string _name, string _label, bool _required, LabelCheckboxOrder order = LabelCheckboxOrder.LabelFirst)
            : base(_name, _label, _required)
        {
            Order = order;
        }

        public enum LabelCheckboxOrder
        {
            LabelFirst,
            CheckboxFirst
        }

        public LabelCheckboxOrder Order { get; set; }
    }
    public class SelectModel : VariableInputModel
    {
        public string selectValuesModel { get; set; }
        
        public SelectModel(string _name, string _label, bool _required, string _selectValuesModel)
            : base(_name, _label, _required)
        {
            selectValuesModel = _selectValuesModel;
        }
    }
    public class DateTimeInputModel : DateInputModel
    {
        public DateTimeInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        { }
    }
    public class DateInputModel : VariableInputModel
    {
        public bool specialDateValidation { get; set; }
        public string specialDateWarning { get; set; }
        public DateInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        { }
    }
    public class MonthInputModel : VariableInputModel
    {
        public MonthInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }
    }
    public class TimeInputModel : VariableInputModel
    {
        public TimeInputModel(string _name, string _label, bool _required)
            : base(_name, _label, _required)
        {

        }
    }
}