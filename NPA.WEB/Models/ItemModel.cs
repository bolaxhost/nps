﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.DAL;
using NPS.DAL.Model;
using NPS.RDBDocumentModel; 

//Модель формы CRUD (создание, редактирование, удаление объектов)
namespace NPS.WEB.Models
{
    public class ItemModel
    {
        public SettingsModel settings { get; set; }

        public string versionName { get; set; }
        public ItemModel()
        {

        }
        public ItemModel(BizDocument document)
        {
            versionName = document.System.majorVersion.ToString();
        }

        //Возможность редактировать элемент
        public bool CanUpdate = true;
        //Возможность удалить элемент
        public bool CanDelete = true;


        //Признак валидности
        public bool isValid = true;
        //Описание результатов валидации
        public string validationResults = "";


        //Уникальный идентификатор документа/раздела документа
        public Guid? id { get; set; }
        //Подпись данных модели
        public string displayName { get; set; }

        /// <summary>
        /// Копирование одноименных свойств source и target из source в target
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        protected virtual void CopyProperties(object source, object target)
        {
            BaseSection.SimpleCopy(source, target); 
        }

        public void ConvertDateTimeTo(DateTimeKind kind, object model)
        {
            Type _modelType = model.GetType();
            foreach (var p in _modelType.GetProperties())
            {
                DateTime? value = null;
                if (p.PropertyType == typeof(DateTime))
                    value = (DateTime)p.GetValue(model);
                if (p.PropertyType == typeof(DateTime?))
                    value = ((DateTime?)p.GetValue(model));

                if (value != null && value.Value.Kind != kind)
                {
                    switch (kind)
                    {
                        case DateTimeKind.Local:
                            {
                                value = value.Value.ToLocalTime();
                                break;
                            }
                        case DateTimeKind.Utc:
                            {
                                value = value.Value.ToUniversalTime();
                                break;
                            }
                    }
                
                    p.SetValue(model, value);
                }
            }
        }

        /// <summary>
        /// Загрузка свойств объекта в модель
        /// </summary>
        /// <param name="source"></param>
        public void Load(object source)
        {
            Load(source, this);
        }
        public void Load(object source, object model)
        {
            CopyProperties(source, model);
            ConvertDateTimeTo(DateTimeKind.Utc, model);
        }

        /// <summary>
        /// Сохранение модели в раздел документа
        /// </summary>
        /// <typeparam name="TSection"></typeparam>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="target"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public TSection Save<TSection>(TSection target)
            where TSection : BaseSection
        {
            return Save(this, target);
        }

        /// <summary>
        /// Сохранение раздела модели в раздел документа
        /// </summary>
        /// <typeparam name="TSection"></typeparam>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="model"></param>
        /// <param name="target"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public TSection Save<TSection>(object model, TSection target)
            where TSection : BaseSection
        {
            ConvertDateTimeTo(DateTimeKind.Local, model);
            CopyProperties(model, target);
            return target;
        }

        public TSubType SaveSubType<TSubType>(object model, TSubType target)
            where TSubType : class
        {
            ConvertDateTimeTo(DateTimeKind.Local, model);
            CopyProperties(model, target);
            return target;
        }

        /// <summary>
        /// Сохранение данных модели в коллекцию разделов документа, когда новые элементы model не имеют значений id
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TSection"></typeparam>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="model"></param>
        /// <param name="data"></param>
        /// <param name="document"></param>
        public void SaveCollection<TModel, TSection, TDocument>(TModel[] model, ICollection<TSection> data, TDocument document, bool skipRemoval = false)
            where TModel : CollectionItemModel
            where TSection : DocumentSectionCollectionAttributes, new()
            where TDocument : BaseDocument
        {
            //Обновление индексов
            for (int i = 0; i < model.Length; i++)
                model[i].instance_section_index = i;

            //removal
            if (!skipRemoval)
            {
                var Ids = model.Where(v => v.id != null).Select(v => v.id).ToArray();
                foreach (var value in data.Where(v => !Ids.Contains(v.instance_section_id)).ToArray())
                    data.Remove(value);
            }

            //update
            foreach (var value in model.Where(v => v.id != null))
                value.Save(data.First(v => v.instance_section_id == value.id));

            //addition
            foreach (var value in model.Where(v => v.id == null))
            {
                var newItem = new TSection();
                newItem.SynchronizeContext(document);
 
                data.Add(value.Save(newItem));
            }

            //sorting
            document.OrderBy(data, data.OrderBy(i => i.instance_section_index).ToList()); 
        }

        /// <summary>
        /// Сохранение данных модели в коллекцию разделов документа, когда новые элементы model уже имеют значений id
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TSection"></typeparam>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="model"></param>
        /// <param name="data"></param>
        /// <param name="document"></param>
        public void SaveCollectionWithId<TModel, TSection, TDocument>(TModel[] model, ICollection<TSection> data, TDocument document, bool skipRemoval = false)
            where TModel : CollectionItemModel
            where TSection : DocumentSectionCollectionAttributes, new()
            where TDocument : BizDocument
        {
            //Обновление индексов
            for (int i = 0; i < model.Length; i++)
                model[i].instance_section_index = i;

            //removal
            if (!skipRemoval)
            {
                var Ids = model.Select(v => v.id).ToArray();
                foreach (var value in data.Where(v => !Ids.Contains(v.instance_section_id)).ToArray())
                    data.Remove(value);
            }

            //update and addition
            foreach (var value in model)
            {
                var dataItem = data.FirstOrDefault(v => v.instance_section_id == value.id);

                if (dataItem == null)
                {
                    var newItem = new TSection() { instance_section_id = value.id.Value };
                    newItem.SynchronizeContext(document); 

                    data.Add(value.Save(newItem));
                }
                else
                    value.Save(dataItem);
            }

            //sorting
            document.OrderBy(data, data.OrderBy(i => i.instance_section_index).ToList());
        }


        //Структуры для работы с файлами
        private BizFileList _files = null;
        public BizFileList Files
        {
            get
            {
                if (_files == null)
                    _files = new BizFileList();

                return _files;
            }

            set
            {
                _files = value;
            }
        }

        public void SaveFiles<TDocument>(TDocument document)
            where TDocument : BizDocument
        {
            var attachments = document.Attachments;

            //Удаление ссылок
            foreach (var a in attachments.ToList())
                if (!a.Files.Any() || this.Files.DeletedFiles.Contains(a.File.id))
                    attachments.Remove(a);
  
            //Обновление подписей
            foreach (var a in attachments)
                a.attachmentName = this.Files.ExistingFiles.First(f => f.attachmentId == a.id).attachmentName;   

            //Добавление новых ссылок
            foreach (var file in this.Files.AddedFiles)
            {
                var a = new Attachment()
                {
                    attachmentName = file.attachmentName,                    
                    attachmentType = 0                     
                };

                a.Files.Add(document.GetData<File>().First(f => f.id == file.id));
                attachments.Add(a);  
            }

        }
    }

    public class CollectionItemModel : ItemModel
    {
        public int instance_section_index { get; set; }
    }

    public class SimpleEnumerationItem<T>
    {
        public T id { get; set; }
        public string name { get; set; }
        public string extraInfo { get; set; }
    }

    public class EnumerationCountItem<T>
    {
        public T Type { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_name))
                {
                    _name = Type.GetDescription();
                }

                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public int Count { get; set; }
    }
}