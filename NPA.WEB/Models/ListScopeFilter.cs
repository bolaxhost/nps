﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.RDBDocumentService; 

//Модель фильтра списка объектов
namespace NPS.WEB.Models
{
    public class ListScopeFilter
    {
        //Признак временного фильтра
        public bool isTemporary { get; set; }

        //Текущий номер страницы
        public int page { get; set; }

        //Критерий текстового поиска
        public string criteria { get; set; }
        //Текущая строка сортировки
        public string orderByExpression { get; set; }
        //Новый элемент сортировки
        public string orderByItem { get; set; }

        private SelectSetItem[] _selectSet = new SelectSetItem[0];
        public SelectSetItem[] selectSet
        {
            get
            {
                return _selectSet; 
            }

            set
            {
                _selectSet = value;
            }
        }

        public bool filterBySelectSet { get; set; }

        public IQueryable<T> Apply<T>(IQueryable<T> data)
            where T : class, IDocumentAttributesNonVMDS
        {
            if (!this.filterBySelectSet)
                return data;

            if (this._selectSet == null)
                return data;

            if (this._selectSet.Length == 0)
                return data;

            var ids = this._selectSet.Select(s => s.id).ToArray();
            return data.Where(i => ids.Contains(i.instance_id)); 
        }

        //Модель сортировки
        private OrderByModel _sortingModel;
        public OrderByModel Sorting
        {
            get
            {
                if (_sortingModel == null)
                {
                    _sortingModel = new OrderByModel(orderByExpression, orderByItem);

                    this.orderByExpression = _sortingModel.ToString();
                    this.orderByItem = string.Empty;
                }

                return _sortingModel;
            }
        }
    }

    public class SelectSetItem
    {
        public Guid id { get; set; }
        public int? type { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
}