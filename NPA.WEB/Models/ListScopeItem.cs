﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Модель элемента списка объектов
namespace NPS.WEB.Models
{
    public class ListScopeItem
    {
        //Информация о версии и соответствии текущему срезу
        public int vmdsVersion { get; set; }
        public bool currentSnapShot { get; set; }

        //Запрет на добавление элемента в набор выбранных объектов
        public bool disableScopeItemSelection { get; set; }
        
        //Версия
        public string versionName { get; set; }

        //Возможность редактировать элемент
        public bool CanUpdate = true;
        //Возможность удалить элемент
        public bool CanDelete = true;
        //Возможность просмотреть элемент
        public bool CanView = true;

       
        //Уникальный идентификатор элемента
        public Guid id { get; set; }
        //Тип документа элемента
        public int? type { get; set; }

        public string displayName { get; set; }

        public string createdDt { get; set; }
        public string updatedDt { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }

        public bool hasAttachments { get; set; }

        
        //Структуры для работы с файлами
        private BizFileList _files = null;
        public BizFileList Files
        {
            get
            {
                if (_files == null)
                    _files = new BizFileList();

                return _files;
            }

            set
            {
                _files = value;
            }
        }
    }
}