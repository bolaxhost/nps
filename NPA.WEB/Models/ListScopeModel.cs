﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Модель списка объектов (список, фильтр, параметры страницы)
namespace NPS.WEB.Models
{
    public class ListScopeModel<TItem, TFilter>
        where TItem : ListScopeItem
        where TFilter : ListScopeFilter 
    {
        //Текущий номер страницы
        public int page { get; set; }
        //Количество записей на странице
        public int pageSize { get; set; }

        //Запрет на все операции редактирования
        public bool isReadonlyView { get; set; }

        //Общее количество элементов в результатах запроса
        public int itemsCount { get; set; }
        //Элементы на странице
        public TItem[] items { get; set; }
        //Фильтр
        public TFilter filter { get; set; }

        //Возможность добавлять новые элементы
        public bool CanCreate { get; set; }

        //Использование специальных организаций
        public bool useSpecialOrganizations { get; set; }
        public bool hasSpecialOrganization { get; set; }
    }
}