﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Reflection;  

using PagedList; 
using NPS.RDBDocumentService; 

namespace NPS.WEB.Models
{
 
    //Модель данных динамической сортировки
    public enum EOrderByMode
    {
        obmNone = 0,
        obmAsc = 1,
        obmDesc = 2
    }

    public class OrderByItem
    {
        public string propertyName { get; set; }
        public EOrderByMode orderByMode { get; set; }
        public int index { get; set; }
    }

    public class OrderByModel
    {
        private Dictionary<string, OrderByItem> _items;

        public OrderByModel()
        {
            _items = new Dictionary<string, OrderByItem>(); 
        }

        public OrderByModel(string model) : this()
        {
            Load(model);
        }

        public OrderByModel(string model, string newItem)
            : this(model)
        {
            Append(newItem);
        }

        public void Load(string model)
        {
            _items.Clear();

            if (string.IsNullOrWhiteSpace(model))
                return;

            foreach (string item in model.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                int spos = item.IndexOf(':');
                if (spos > 0)
                    _items.Add(item.Substring(0, spos), new OrderByItem() { index = _items.Count, propertyName = item.Substring(0, spos) , orderByMode = (EOrderByMode)int.Parse(item.Substring(spos + 1, 1)) });
            }
        }

        public void Append(string item)
        {
            if (string.IsNullOrWhiteSpace(item))
                return;

            if (_items.ContainsKey(item))
            {
                if (_items[item].orderByMode == EOrderByMode.obmAsc)
                    _items[item].orderByMode = EOrderByMode.obmDesc;
                else _items.Remove(item); 
            }
            else
                _items.Add(item, new OrderByItem() { index = _items.Count, propertyName = item, orderByMode = EOrderByMode.obmAsc });
        }

        public string StyleName(string item)
        {
            if (!_items.ContainsKey(item))
                return "orderBy";

            if (_items[item].orderByMode == EOrderByMode.obmAsc)
                return "orderBy-asc";

            return "orderBy-desc";
        }

        public bool HasItems { get { return _items.Any(); } }

        public List<OrderByItem> ToList()
        {
            return _items.Select(i => i.Value).OrderBy(i => i.index).ToList();   
        }

        public override string ToString()
        {
            StringBuilder sBuilder = new StringBuilder();

            foreach (var item in this.ToList())
                sBuilder.Append(string.Format("{0}:{1};", item.propertyName, (int)item.orderByMode));

            return sBuilder.ToString();            
        }

        private IOrderedQueryable<T> DoOrderBy<T>(IQueryable<T> items, string propertyName, EOrderByMode mode, bool ThenBy = false) where T : class
        {
            if (!ThenBy)
            {
                if (mode == EOrderByMode.obmAsc)                      
                    return items.OrderBy(propertyName);

                if (mode == EOrderByMode.obmDesc)
                    return items.OrderByDescending(propertyName);
            }
            else
            {
                if (mode == EOrderByMode.obmAsc)
                    return (items as IOrderedQueryable<T>).ThenBy(propertyName);  

                if (mode == EOrderByMode.obmDesc)
                    return (items as IOrderedQueryable<T>).ThenByDescending(propertyName);
            }

            return items as IOrderedQueryable<T>;
        }

        public IQueryable<T> DoOrderBy<T>(IQueryable<T> items, Func<T, object> defaultOrdering = null) where T : class
        {
            bool ThenBy = false;
            foreach (var orderByItem in this.ToList())
            {
                items = DoOrderBy(items, orderByItem.propertyName, orderByItem.orderByMode, ThenBy);

                if (!ThenBy) 
                    ThenBy = true;
            }

            return items;
        }
    }
}