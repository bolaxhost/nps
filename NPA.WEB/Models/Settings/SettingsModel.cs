﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using NPS.DAL; 
 

namespace NPS.WEB.Models
{
    public class SettingsModel
    {
        public string wmsUrl { get; set; }
        public string wfsUrl { get; set; }

        public string rosregUrl { get; set; }
        public string rosregPortalUrl { get; set; }


        public string layerLinesHVName { get; set; }
        public string layerLinesMVName { get; set; }
        public string layerLinesLVName { get; set; }

        public string layerPoleTransmissionName { get; set; }
        public string layerPoleDistributionName { get; set; }

        public string layerSubstationPointHVName { get; set; }
        public string layerSubstationPointMVName { get; set; }

        public string layerSubstationAreaName { get; set; }

        public SettingsModel() 
        {
            this.rosregUrl = "http://maps.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/";
            this.rosregPortalUrl = "http://maps.rosreestr.ru/PortalOnline/";

        }
    }
}