﻿$(document).ready(function () {
    DatePickersInit("");
});

 
function DatePickersInit(context) {
    var allowedDates = new Object();

    $(context + " .datepickerDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd.mm.yy',
        altFormat: "dd.mm.yy",
        beforeShowDay: function (day) {
        var id = $(this).attr('id');

        var d = day.getDate();
        var m = day.getMonth() + 1;
        var y = day.getFullYear();
        if (d < 10) {
            d = "0" + d;
        }
        if (m < 10) {
            m = "0" + m;
        }

        var dateStr = d + "." + m + "." + y;

        if (allowedDates[id] != undefined) {
            if ($.inArray(dateStr, allowedDates[id]) == -1) {
                return [true, 'good_date', 'Рабочий день'];
            }
            else {
                return [true, 'dayOff', 'Выходной день'];
            }
        }

        return [true, 'good_date', 'This date is selectable'];
    },
    beforeShow: function (input) {
        var currentDate = $(input).datepicker('getDate');

        var id = $(input).attr('id');
        if (currentDate == null) {
            currentDate = new Date();
        }

        queryAllowedDates(currentDate.getFullYear(), currentDate.getMonth() + 1, id);
    },
    onChangeMonthYear: function (year, month, inst) {
        queryAllowedDates(year, month, inst.input.attr('id'));
    },
    onSelect: function (value, date) {
        if ($(this).hasClass("dayOnclick") && $.inArray(value, allowedDates.date) != -1) {
            $(this).addClass("dayOff");
        }
        else {
            $(this).removeClass("dayOff");
        }
    }
    });

    $(context + " .datepickerYearMonth").datepicker({
        changeMonth: true,
        changeYear: true,
        //showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        beforeShow: function (input, inst) {
            if ((datestr = $(this).val()).length > 0) {
                year = datestr.substring(datestr.length - 4, datestr.length);
                month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        }
    });

    $(context + " .datepickerYearMonth").focus(function () {
        $(".ui-datepicker-calendar").hide();
    });

    $(context + " .datepickerFactory").datepicker({
        dateFormat: 'dd.mm.yy',
        altFormat: "dd.mm.yy",
        changeMonth: true,
        changeYear: true,

        beforeShowDay: function (day) {
            var id = $(this).attr('id');

            var d = day.getDate();
            var m = day.getMonth() + 1;
            var y = day.getFullYear();
            if (d < 10) {
                d = "0" + d;
            }
            if (m < 10) {
                m = "0" + m;
            }

            var dateStr = d + "." + m + "." + y;

            if (allowedDates[id] != undefined) {
                if ($.inArray(dateStr, allowedDates[id]) == -1) {
                    return [true, 'good_date', 'This date is selectable'];
                }
                else {
                    return [false, 'bad_date', 'This date is NOT selectable'];
                }
            }

            return [true, 'good_date', 'This date is selectable'];
        },
        beforeShow: function (input) {
            var currentDate = $(input).datepicker('getDate');

            var id = $(input).attr('id');
            if (currentDate == null) {
                currentDate = new Date();
            }

            queryAllowedDates(currentDate.getFullYear(), currentDate.getMonth() + 1, id);
        },
        onChangeMonthYear: function (year, month, inst) {
            queryAllowedDates(year, month, inst.input.attr('id'));
        }
    });

    $(context + " .datetimepickerFactory").datetimepicker({
        dateFormat: 'dd.mm.yy',
        altFormat: "dd.mm.yy",
        timeFormat: 'HH:mm',
        showSecond: null,
        timeOnlyTitle: 'Выберите время',
        timeText: 'Время',
        hourText: 'Часы',
        minuteText: 'Минуты',
        secondText: 'Секунды',
        currentText: 'Сейчас',
        closeText: 'Закрыть',
        hourGrid: 4,
        minuteGrid: 10,
        beforeShowDay: function (day) {
            var id = $(this).attr('id');

            var d = day.getDate();
            var m = day.getMonth() + 1;
            var y = day.getFullYear();
            if (d < 10) {
                d = "0" + d;
            }
            if (m < 10) {
                m = "0" + m;
            }

            var dateStr = d + "." + m + "." + y;

            if (allowedDates[id] != undefined) {
                if ($.inArray(dateStr, allowedDates[id]) == -1) {
                    return [true, 'good_date', 'This date is selectable'];
                }
                else {
                    return [false, 'bad_date', 'This date is NOT selectable'];
                }
            }

            return [true, 'good_date', 'This date is selectable'];
        },
        beforeShow: function (input) {
            var currentDate = $(input).datepicker('getDate');

            var id = $(input).attr('id');
            if (currentDate == null) {
                currentDate = new Date();
            }

            queryAllowedDates(currentDate.getFullYear(), currentDate.getMonth() + 1, id);
        },
        onChangeMonthYear: function (year, month, inst) {
            queryAllowedDates(year, month, inst.input.attr('id'));
        }
    });

    function queryAllowedDates(year, month, id) {
        $.ajax({
            type: 'POST',
            url: '/Home/InitFactoryCalendar',
            dataType: 'json',
            success: function (response) {
                allowedDates[id] = response.allowedDates;
            },
            data: { year: year, month: month },
            async: false
        });
    }

    $(context + " .datetimepicker").datetimepicker({
        dateFormat: 'dd.mm.yy',
        altFormat: "dd.mm.yy",
        timeFormat: 'HH:mm',
        timeOnlyTitle: 'Выберите время',
        timeText: 'Время',
        hourText: 'Часы',
        minuteText: 'Минуты',
        secondText: 'Секунды',
        currentText: 'Сейчас',
        closeText: 'Закрыть',
        changeMonth: true,
        changeYear: true,
        hourGrid: 4,
        minuteGrid: 10,
            beforeShowDay: function (day) {
                var id = $(this).attr('id');

                var d = day.getDate();
                var m = day.getMonth() + 1;
                var y = day.getFullYear();
                if (d < 10) {
                    d = "0" + d;
                }
                if (m < 10) {
                    m = "0" + m;
                }

                var dateStr = d + "." + m + "." + y;

                if (allowedDates[id] != undefined) {
                    if ($.inArray(dateStr, allowedDates[id]) == -1) {
                        return [true, 'good_date', 'This date is selectable'];
                    }
                    else {
                        return [true, 'dayOff', 'This date is NOT selectable'];
                    }
                }

                return [true, 'good_date', 'This date is selectable'];
            },
    beforeShow: function (input) {
        var currentDate = $(input).datepicker('getDate');

        var id = $(input).attr('id');
        if (currentDate == null) {
            currentDate = new Date();
        }

        queryAllowedDates(currentDate.getFullYear(), currentDate.getMonth() + 1, id);
    },
    onChangeMonthYear: function (year, month, inst) {
        queryAllowedDates(year, month, inst.input.attr('id'));
    }
    });


    $(context + " form").submit(function (e, t) {
        var valid = true;

        $(context + " .datepickerDate.inputRequired, .datetimepicker.inputRequired, .datepickerYearMonth.inputRequired, .datepickerFactory.inputRequired, .datetimepickerFactory.inputRequired").each(function () {
            if ($(this).val() == "") {
                valid = false;
                var span = $(this).parent().find("span");

                if (span.children().length == 0) {
                    span.append("<span for='" + span.attr("data-valmsg-for") + "'>Поле обязательно для ввода</span>");
                    span.addClass("field-validation-error");
                }
            }
        });

        if (!valid) {
            return false;
        }

    });

    $(context + " .datepickerDate.inputRequired, .datetimepicker.inputRequired, .datepickerYearMonth.inputRequired, .datepickerFactory.inputRequired").change(function () {
        var span = $(this).parent().find("span");

        if ($(this).val() != "") {
            span.children().remove();
            span.removeClass("field-validation-error").addClass("field-validation-valid");
        }
        else {
            if (span.children().length == 0) {
                span.append("<span for='" + span.attr("data-valmsg-for") + "'>Поле обязательно для ввода</span>");
                span.addClass("field-validation-error");
            }
        }
    });
}