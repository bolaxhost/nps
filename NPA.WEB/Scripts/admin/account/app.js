﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/login', {
                  templateUrl: '/api/admin/login-form',
                  controller: 'LoginCtrl'
              }).
              otherwise({
                  redirectTo: '/login'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();