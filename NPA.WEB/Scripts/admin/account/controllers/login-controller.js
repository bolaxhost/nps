﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('LoginCtrl', ['$scope', '$resource', '$route', '$routeParams', '$location', 'BizControllerHelper', LoginController]);

    function LoginController($scope, $resource, $route, $routeParams, $location, BizControllerHelper) {
        BizControllerHelper.initBase($scope);

        var resource = $resource('/api/admin/login', {}, {
            'getLogin': { method: 'GET' },
            'tryToLogin': { method: 'POST' },
        });

        $scope.data = resource.getLogin();
        $scope.submit = function () {
            $scope.data.$tryToLogin().then($scope.createRequestResultHandler(function () {
                $scope.gotoHref('http:/');
            }), $scope.resourceErrorHandler);
        }
    }
})();