﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/search', {
                  templateUrl: '/api/admin/accounts/search-form',
                  controller: 'ListCtrl'
              }).
              when('/create', {
                  templateUrl: '/api/admin/accounts/edit-form',
                  controller: 'ItemCtrl',
                  mode: "create"
              }).
              when('/update/:id', {
                  templateUrl: '/api/admin/accounts/edit-form',
                  controller: 'ItemCtrl',
                  mode: "update"
              }).
              when('/account/:id', {
                  templateUrl: '/api/admin/accounts/edit-form',
                  controller: 'ItemCtrl',
                  mode: "update-light"
              }).when('/delete/:id', {
                  templateUrl: '/api/admin/accounts/delete-form',
                  controller: 'ItemCtrl',
                  mode: "delete"
              }).
              when('/password/:login', {
                  templateUrl: '/api/admin/accounts/password-form',
                  controller: 'PasswordCtrl',
              }).
              otherwise({
                  redirectTo: '/search'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();