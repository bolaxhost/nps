﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'admin' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);
        $scope.returnUrl = "#/search";
        $scope.lightMode = false;

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание новой учетной записи';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование учетной записи';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'update-light') {
            $scope.title = 'Редактирование учетной записи';
            $scope.read($routeParams.id);
            $scope.returnUrl = "http:/api/biz/dashboard/view";
            $scope.lightMode = true;
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление учетной записи';
            $scope.read($routeParams.id);
        }

        if (!$scope.lightMode)
            $scope.tabs = ["Основные параметры", "Роли"];

        $scope.selectedTabIndex = 0;
    }
})();