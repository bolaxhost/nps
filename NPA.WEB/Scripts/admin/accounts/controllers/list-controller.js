﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ListCtrl', ['$scope', '$rootScope', 'BizQueryService', 'BizControllerHelper', ListController]);

    function ListController($scope, $rootScope, BizQueryService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'admin' };

        BizControllerHelper.initList($scope, BizQueryService);

        //Получение данных
        $scope.extraFilterEnabled = false;
        $scope.query();
    }
})();