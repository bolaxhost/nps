﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('PasswordCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', '$resource', 'BizControllerHelper', PasswordController]);

    function PasswordController($scope, $rootScope, $route, $routeParams, $location, $resource, BizControllerHelper) {
        BizControllerHelper.initBase($scope);

        $scope.login = $routeParams.login;
        $scope.returnUrl = "#/search";

        var resource = $resource('/api/admin/accounts/password/:login', { login: $scope.login }, {
            'getModel': { method: 'GET' },
            'changePassword': { method: 'POST' },
        });

        $scope.data = resource.getModel();
        $scope.data.$promise.then(function () {
            if ($scope.data.login == $scope.data.currentUser)
                $scope.returnUrl = "/api/biz/dashboard/view";
        });

        $scope.submit = function () {
            $scope.data.$changePassword().then($scope.createRequestResultHandler(function () {
                $scope.gotoHref($scope.returnUrl);
            }), $scope.resourceErrorHandler);
        }
    }
})();