﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizCRUDService', ['CRUDServiceApi',
        function (CRUDServiceApi) {
            return new CRUDServiceApi('/api/admin/accounts/item');
        }])
        .factory('BizQueryService', ['QueryServiceApi',
        function (QueryServiceApi) {
            return new QueryServiceApi('/api/admin/accounts/query');
        }])
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }]);
})();