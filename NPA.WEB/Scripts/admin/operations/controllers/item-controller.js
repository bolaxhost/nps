﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'admin' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);
        $scope.returnUrl = "#/search";

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание новой операции';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование операции';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление операции';
            $scope.read($routeParams.id);
        }
    }
})();