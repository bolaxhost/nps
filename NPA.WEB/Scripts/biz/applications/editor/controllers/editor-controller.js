﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('EditorCtrl', ['$scope', '$rootScope', '$resource', '$route', '$sce', '$routeParams', '$location', 'BizControllerHelper', EditorController]);

    function EditorController($scope, $rootScope, $resource, $route, $sce, $routeParams, $location, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'data' };


        //Базовая инициализация
        BizControllerHelper.initBase($scope);





        //Загрузка настроек редактора
        var resource = $resource('/api/biz/editor/model', {}, {
            'getModel': { method: 'GET' },
        });

        $scope.data = resource.getModel();









        //Список слоев и текущий слой
        $scope.currentLayer = null;
        $scope.vectorLayers = [];
        $scope.objectName = "";

        $scope.selectCurrentLayer = function(layer) {
            $scope.currentLayer = layer;
        }

        //Центр карты и масштаб по умолчанию
        var defaultCenter = [4187136.7833, 7509542.7662];
        var defaultZoom = 8;

        //Загрузка карты
        window.setTimeout(function () {
            $scope.initializeMap();
            $scope.finalMapInitialing();
            $scope.mapData.synchronizeCenter();
            $scope.mapData.synchronizeZoom();
        }, 1000);

        //Первоначальная загрузка карты
        $scope.initializeMap = function () {
            $scope.mapData = { layers: {}, sources: {} };




            //Временные данные редактора
            $scope.currentVectorStyle = new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(0, 128, 0, 0.5)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(0, 128, 0, 1)',
                    width: 4
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 128, 0, 1)'
                    })
                })
            })
            $scope.mapData.sources.vector = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.vector = new ol.layer.Vector({
                source: $scope.mapData.sources.vector,
                style: $scope.currentVectorStyle,
            });

            //Функция формирования текста для подписей
            var getGeometryText = function (feature, resolution, field, maxResolution) {
                if (resolution > maxResolution)
                    return '';

                var text = feature.get(field);
                return text ? text : '';
            };


            //Функция формирования стиля подписи
            var createLineTextStyle = function (feature, resolution, font, color, maxResolution) {
                var rotation = feature.get("az");
                if (rotation > 3.14)
                    rotation = rotation - 3.14;

                rotation = rotation - 1.57

                return new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'bottom',
                    font: font,
                    text: getGeometryText(feature, resolution, 'name', maxResolution),
                    fill: new ol.style.Fill({ color: '#ffffff' }),
                    stroke: new ol.style.Stroke({ color: color, width: 4 }),
                    offsetX: 0,
                    offsetY: 0,
                    rotation: rotation,
                });
            };

            var createLineTextStyleFunction = function (font, color, maxResolution) {
                return function (feature, resolution) {
                    var style = new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 0
                        }),
                        text: createLineTextStyle(feature, resolution, font, color, maxResolution)
                    });
                    return [style];
                };
            };

            var createPoleTextStyle = function (feature, resolution, font, color, maxResolution) {
                return new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'bottom',
                    font: font,
                    text: getGeometryText(feature, resolution, 'num1', maxResolution),
                    fill: new ol.style.Fill({ color: '#ffffff' }),
                    stroke: new ol.style.Stroke({ color: color, width: 4 }),
                    offsetX: 0,
                    offsetY: 0,
                    rotation: 0
                });
            };

            var createPoleTextStyleFunction = function (image, font, color, maxResolution) {
                return function (feature, resolution) {
                    var style = new ol.style.Style({
                        image: image,
                        text: createPoleTextStyle(feature, resolution, font, color, maxResolution)
                    });
                    return [style];
                };
            };

            var createSubPointTextStyle = function (feature, resolution, font, color, maxResolution) {
                return new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'bottom',
                    font: font,
                    text: getGeometryText(feature, resolution, 'name', maxResolution),
                    fill: new ol.style.Fill({ color: '#ffffff' }),
                    stroke: new ol.style.Stroke({ color: color, width: 4 }),
                    offsetX: 0,
                    offsetY: -5,
                    rotation: 0
                });
            };

            var createSubPointTextStyleFunction = function (image, font, color, maxResolution) {
                return function (feature, resolution) {
                    var style = new ol.style.Style({
                        image: image,
                        text: createSubPointTextStyle(feature, resolution, font, color, maxResolution)
                    });
                    return [style];
                };
            };

            var loadData = function (extent, resolution, projection, layerName, source) {
                var resource = $resource('/api/biz/editor/data', {}, {
                    'getData': { method: 'GET' },
                });

                resource.getData({ layer: layerName, filter: "&SRSNAME=urn:x-ogc:def:crs:EPSG:3857&BBOX=" + extent.join(",") + ",EPSG:3857" }).$promise.then(function (response) {
                    source.addFeatures(new ol.format.GeoJSON().readFeatures(JSON.parse(response.data)));
                });
            }



            //Описание слоев
            //Линии ВН
            if ($scope.data.settings.layerLinesHVName && $scope.data.settings.layerLinesHVName.length > 0) {
                $scope.mapData.sources.linesHV = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesHVName, $scope.mapData.sources.linesHV);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesHV = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesHV,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(255, 0, 0, 1.0)',
                            width: 2
                        })
                    })
                });

                $scope.mapData.sources.linesHV_txt = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesHVName + "_txt", $scope.mapData.sources.linesHV_txt);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesHV_txt = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesHV_txt,
                    style: createLineTextStyleFunction('Normal 12px Arial', '#FF0000', 30)
                });
            }

            //Линии СН
            if ($scope.data.settings.layerLinesMVName && $scope.data.settings.layerLinesMVName.length > 0) {
                $scope.mapData.sources.linesMV = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesMVName, $scope.mapData.sources.linesMV);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesMV = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesMV,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(0, 128, 0, 1.0)',
                            width: 2
                        })
                    })
                });

                $scope.mapData.sources.linesMV_txt = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesMVName + "_txt", $scope.mapData.sources.linesMV_txt);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesMV_txt = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesMV_txt,
                    style: createLineTextStyleFunction('Normal 12px Arial', 'rgba(0, 128, 0, 1.0)', 10)
                });
            }

            //Линии НН
            if ($scope.data.settings.layerLinesLVName && $scope.data.settings.layerLinesLVName.length > 0) {
                $scope.mapData.sources.linesLV = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesLVName, $scope.mapData.sources.linesLV);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesLV = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesLV,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(0, 0, 255, 1.0)',
                            width: 2
                        })
                    })
                });

                $scope.mapData.sources.linesLV_txt = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerLinesLVName + "_txt", $scope.mapData.sources.linesLV_txt);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.linesLV_txt = new ol.layer.Vector({
                    source: $scope.mapData.sources.linesLV_txt,
                    style: createLineTextStyleFunction('Normal 12px Arial', 'rgba(0, 0, 255, 1.0)', 10)
                });
            }


            //Опоры питающей сети
            if ($scope.data.settings.layerPoleTransmissionName && $scope.data.settings.layerPoleTransmissionName.length > 0) {
                $scope.mapData.sources.polesT = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerPoleTransmissionName, $scope.mapData.sources.polesT);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.polesT = new ol.layer.Vector({
                    source: $scope.mapData.sources.polesT,
                    style: createPoleTextStyleFunction(new ol.style.Circle({
                        radius: 5,
                        fill: new ol.style.Fill({
                            color: '#FF0000'
                        })
                    }), 'Normal 12px Arial', '#FF0000', 30),
                });
            }

            //Опоры распределительной сети
            if ($scope.data.settings.layerPoleDistributionName && $scope.data.settings.layerPoleDistributionName.length > 0) {
                $scope.mapData.sources.polesD = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerPoleDistributionName, $scope.mapData.sources.polesD);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.polesD = new ol.layer.Vector({
                    source: $scope.mapData.sources.polesD,
                    style: createPoleTextStyleFunction(new ol.style.Circle({
                        radius: 3,
                        fill: new ol.style.Fill({
                            color: '#008000'
                        })
                    }), 'Normal 12px Arial', '#008000', 10)
                });
            }

            //Символ центра питания
            if ($scope.data.settings.layerSubstationPointHVName && $scope.data.settings.layerSubstationPointHVName.length > 0) {
                $scope.mapData.sources.subPointHV = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerSubstationPointHVName, $scope.mapData.sources.subPointHV);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.subPointHV = new ol.layer.Vector({
                    source: $scope.mapData.sources.subPointHV,
                    style: createSubPointTextStyleFunction(new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#FF0000'
                        })
                    }), 'Normal 14px Arial', '#FF0000', 50),
                });
            }

            //Символ подстанции 6-35 кВ
            if ($scope.data.settings.layerSubstationPointMVName && $scope.data.settings.layerSubstationPointMVName.length > 0) {
                $scope.mapData.sources.subPointMV = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerSubstationPointMVName, $scope.mapData.sources.subPointMV);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.subPointMV = new ol.layer.Vector({
                    source: $scope.mapData.sources.subPointMV,
                    style: createSubPointTextStyleFunction(new ol.style.Circle({
                        radius: 5,
                        fill: new ol.style.Fill({
                            color: '#008000'
                        })
                    }), 'Normal 12px Arial', '#008000', 30),
                });
            }

            //Контура подстанций
            if ($scope.data.settings.layerSubstationAreaName && $scope.data.settings.layerSubstationAreaName.length > 0) {
                $scope.mapData.sources.subArea = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    loader: function (extent, resolution, projection) {
                        loadData(extent, resolution, projection, $scope.data.settings.layerSubstationAreaName, $scope.mapData.sources.subArea);
                    },
                    //strategy: ol.loadingstrategy.bbox,
                    strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
                        maxZoom: 19
                    }))
                });

                $scope.mapData.layers.subArea = new ol.layer.Vector({
                    source: $scope.mapData.sources.subArea,
                    style: new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(128, 0, 0, 0.5)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: 'rgba(128, 0, 0, 1)',
                            width: 2
                        }),
                    })
                });
            }


            //Проекция
            var mapProjection = new ol.proj.Projection({
                code: 'EPSG:3857',
                units: 'm'
            });


            //Представление
            $scope.mapData.view = new ol.View({
                center: defaultCenter,
                zoom: defaultZoom,
                maxZoom: 21,
                projection: mapProjection
            });





            //Список слоев и конфигураций
            var layers = [
                    $scope.mapData.layers.vector,
            ];

            var getGMLFormat = function (layerName) {
                var names = layerName.split(':');
                return new ol.format.GML({
                    featureNS: names[0],
                    featureType: names[1],
                    srsName: 'EPSG:4326'
                });
            }

            if ($scope.mapData.layers.linesHV) {
                layers.push($scope.mapData.layers.linesHV);
                layers.push($scope.mapData.layers.linesHV_txt);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerLinesHVName,
                    name: "ВЛ 110-500 кВ",
                    gml: getGMLFormat($scope.data.settings.layerLinesHVName),
                    layer: $scope.mapData.layers.linesHV,

                    objectName: true,
                    objectNameField: "name",
                    objectNameDefaultValue: "Новая ВЛ ВН",

                    snap: [$scope.mapData.sources.polesT ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.polesT
                    }) : null],

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'LineString',
                    }),

                    dependentLayers: [$scope.mapData.layers.polesT, $scope.mapData.layers.linesHV_txt],
                })
            }

            if ($scope.mapData.layers.linesMV) {
                layers.push($scope.mapData.layers.linesMV);
                layers.push($scope.mapData.layers.linesMV_txt);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerLinesMVName,
                    name: "ВЛ 6-35 кВ",
                    gml: getGMLFormat($scope.data.settings.layerLinesMVName),
                    layer: $scope.mapData.layers.linesMV,

                    objectName: true,
                    objectNameField: "name",
                    objectNameDefaultValue: "Новая ВЛ СН",

                    snap: [$scope.mapData.sources.polesT ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.polesT
                    }) : null,
                    $scope.mapData.sources.polesD ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.polesD
                    }) : null],

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'LineString',
                    }),

                    dependentLayers: [$scope.mapData.layers.polesT, $scope.mapData.layers.polesD, $scope.mapData.layers.linesMV_txt],
                })
            }

            if ($scope.mapData.layers.linesLV) {
                layers.push($scope.mapData.layers.linesLV);
                layers.push($scope.mapData.layers.linesLV_txt);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerLinesLVName,
                    name: "ВЛ 0.4 кВ",
                    gml: getGMLFormat($scope.data.settings.layerLinesLVName),
                    layer: $scope.mapData.layers.linesLV,

                    objectName: true,
                    objectNameField: "name",
                    objectNameDefaultValue: "Новая ВЛ НН",

                    snap: [$scope.mapData.sources.polesD ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.polesD
                    }) : null],

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'LineString',
                    }),

                    dependentLayers: [$scope.mapData.layers.polesD, $scope.mapData.layers.linesLV_txt],
                })
            }


            if ($scope.mapData.layers.polesT) {
                layers.push($scope.mapData.layers.polesT);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerPoleTransmissionName,
                    name: "Опора ВЛ 110-500 кВ",
                    gml: getGMLFormat($scope.data.settings.layerPoleTransmissionName),
                    layer: $scope.mapData.layers.polesT,

                    objectName: true,
                    objectNameField: "num1",
                    objectNameDefaultValue: "№",

                    snap: [$scope.mapData.sources.linesHV ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.linesHV
                    }) : null,
                    $scope.mapData.sources.linesMV ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.linesMV
                    }) : null],

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'Point',
                    }),

                    dependentLayers: [$scope.mapData.layers.linesHV, $scope.mapData.layers.linesHV_txt, $scope.mapData.layers.linesMV, $scope.mapData.layers.linesMV_txt],
                })
            }

            if ($scope.mapData.layers.polesD) {
                layers.push($scope.mapData.layers.polesD);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerPoleDistributionName,
                    name: "Опора ВЛ 0.4-35 кВ",
                    gml: getGMLFormat($scope.data.settings.layerPoleDistributionName),
                    layer: $scope.mapData.layers.polesD,

                    objectName: true,
                    objectNameField: "num1",
                    objectNameDefaultValue: "№",

                    snap: [$scope.mapData.sources.linesMV ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.linesMV
                    }) : null,
                    $scope.mapData.sources.linesLV ? new ol.interaction.Snap({
                        source: $scope.mapData.sources.linesLV
                    }) : null],

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'Point',
                    }),

                    dependentLayers: [$scope.mapData.layers.linesMV, $scope.mapData.layers.linesMV_txt, $scope.mapData.layers.linesLV, $scope.mapData.layers.linesLV_txt],
                })
            }

            if ($scope.mapData.layers.subPointHV) {
                layers.push($scope.mapData.layers.subPointHV);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerSubstationPointHVName,
                    name: "Символ центра питания",
                    gml: getGMLFormat($scope.data.settings.layerSubstationPointHVName),
                    layer: $scope.mapData.layers.subPointHV,

                    objectName: true,
                    objectNameField: "name",
                    objectNameDefaultValue: "ЦП",

                    snap: null,

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'Point',
                    }),

                    dependentLayers: null,
                })
            }

            if ($scope.mapData.layers.subPointMV) {
                layers.push($scope.mapData.layers.subPointMV);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerSubstationPointMVName,
                    name: "Подстанция 6-35 кВ",
                    gml: getGMLFormat($scope.data.settings.layerSubstationPointMVName),
                    layer: $scope.mapData.layers.subPointMV,

                    objectName: true,
                    objectNameField: "name",
                    objectNameDefaultValue: "Подстанция",

                    snap: null,

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'Point',
                    }),

                    dependentLayers: null,
                })
            }

            if ($scope.mapData.layers.subArea) {
                layers.push($scope.mapData.layers.subArea);

                $scope.vectorLayers.push({
                    id: $scope.data.settings.layerSubstationAreaName,
                    name: "Контур подстанции",
                    gml: getGMLFormat($scope.data.settings.layerSubstationAreaName),
                    layer: $scope.mapData.layers.subArea,

                    snap: null,

                    draw: new ol.interaction.Draw({
                        source: $scope.mapData.sources.vector,
                        type: 'Polygon',
                    }),
                })
            }

            $scope.mapData.layersGroup = new ol.layer.Group({
                layers: layers,
            });





            //Инициализация карты openlayers
            $scope.mapData.map = new ol.Map({
                target: 'olmap',
                view: $scope.mapData.view
            });






            //Инициализация карты google
            $scope.mapData.gmap = new google.maps.Map(document.getElementById('gmap'), {
                disableDefaultUI: true,

                keyboardShortcuts: false,
                draggable: false,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                streetViewControl: false,

                center: new google.maps.LatLng(defaultCenter[1], defaultCenter[0]),
                zoom: defaultZoom,

                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });


            //Синхронизация карт
            $scope.mapData.synchronizeCenter = function () {
                var center = ol.proj.transform($scope.mapData.view.getCenter(), 'EPSG:3857', 'EPSG:4326');
                $scope.mapData.gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
            }

            $scope.mapData.view.on('change:center', function () {
                $scope.mapData.synchronizeCenter();
            });

            $scope.mapData.synchronizeZoom = function () {
                var zoom = $scope.mapData.view.getZoom();
                $scope.mapData.gmap.setZoom(zoom);
                $scope.updateUseZoomFiltration();
            }

            $scope.mapData.view.on('change:resolution', function () {
                $scope.mapData.synchronizeZoom();
            });





            //Подсветка и выбор по умолчанию
            $scope.mapData.highlightInteraction = new ol.interaction.Select({
                condition: ol.events.condition.pointerMove
            });

            $scope.mapData.selectedFeature = null;
            $scope.onFeatureSelected = function (e) {
                $scope.$apply(function () {
                    $scope.mapData.selectedFeature = e.element;
                    $scope.currentLayer = $scope.getLayerByFeature($scope.mapData.selectedFeature);

                    if ($scope.currentLayer) {
                        if ($scope.currentLayer.objectName) {
                            $scope.objectName = $scope.mapData.selectedFeature.get($scope.currentLayer.objectNameField)
                        }
                    }
                });
            }

            $scope.mapData.selectInteraction = new ol.interaction.Select({});
            $scope.mapData.selectInteraction.getFeatures().on("add", function (e) {
                $scope.onFeatureSelected(e);
            });

            $scope.mapData.map.addInteraction($scope.mapData.highlightInteraction);
            $scope.mapData.map.addInteraction($scope.mapData.selectInteraction);





            //Добавление слоев и завершение инициализации
            $scope.mapData.map.setLayerGroup($scope.mapData.layersGroup);
            $scope.mapIsInitialized = true;
        }

        $scope.switchMapType = function () {
            if ($scope.mapData.mapType == undefined)
                $scope.mapData.mapType = 0;

            $scope.mapData.mapType++;

            if ($scope.mapData.mapType == 4)
                $scope.mapData.mapType = 0;

            switch ($scope.mapData.mapType) {
                case 0: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.ROADMAP); break;
                case 1: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.SATELLITE); break;
                case 2: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.HYBRID); break;
                case 3: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.TERRAIN); break;
            }
        }


        //Восстановление сохраненных параметров карты
        $scope.finalMapInitialing = function () {
        }






        //Оцифровка учатска и построение буфера
        $scope.digitizeMode = false;
        $scope.digitizeType = 0;

        //Масштабная фильтрация
        $scope.useZoomFiltration = true;
        $scope.switchUseZoomFiltration = function () {
            $scope.useZoomFiltration = !$scope.useZoomFiltration;
            $scope.updateUseZoomFiltration();
        }

        $scope.updateUseZoomFiltration = function () {
            if (!$scope.mapData)
                return;

            var zoom = $scope.mapData.view.getZoom();

            //Масштабная фильтрация
            if ($scope.mapData.layers.linesHV) {
                $scope.mapData.layers.linesHV.setVisible(!$scope.useZoomFiltration || zoom > 6)
                $scope.mapData.layers.linesHV_txt.setVisible(!$scope.useZoomFiltration || zoom > 6)
            }

            if ($scope.mapData.layers.linesMV) {
                $scope.mapData.layers.linesMV.setVisible(!$scope.useZoomFiltration || zoom > 12)
                $scope.mapData.layers.linesMV_txt.setVisible(!$scope.useZoomFiltration || zoom > 12)
            }

            if ($scope.mapData.layers.linesLV) {
                $scope.mapData.layers.linesLV.setVisible(!$scope.useZoomFiltration || zoom > 12)
                $scope.mapData.layers.linesLV_txt.setVisible(!$scope.useZoomFiltration || zoom > 12)
            }

            if ($scope.mapData.layers.polesT)
                $scope.mapData.layers.polesT.setVisible(!$scope.useZoomFiltration || zoom > 10)

            if ($scope.mapData.layers.polesD)
                $scope.mapData.layers.polesD.setVisible(!$scope.useZoomFiltration || zoom > 12)

            if ($scope.mapData.layers.subPointHV)
                $scope.mapData.layers.subPointHV.setVisible(!$scope.useZoomFiltration || zoom > 6)

            if ($scope.mapData.layers.subPointMV)
                $scope.mapData.layers.subPointMV.setVisible(!$scope.useZoomFiltration || zoom > 10)

            if ($scope.mapData.layers.subArea)
                $scope.mapData.layers.subArea.setVisible(!$scope.useZoomFiltration || zoom > 10)
        }


        //Обработка выбора текущего слоя
        $scope.lf = null;
        $scope.lr = false;
        $scope.selectFilter = function (feature, layer) {
            if ($scope.lf == feature)
                return $scope.lr;

            $scope.lf = feature;
            $scope.lr = layer == $scope.currentLayer.layer;

            return $scope.lr;
        }

        $scope.getLayerByFeature = function (feature) {
            var tableName = feature.getId().split('.')[0];
            for (var i = 0; i < $scope.vectorLayers.length; i++) {
                var layer = $scope.vectorLayers[i];
                var layerName = layer.id.split(':')[1];

                if (tableName == layerName)
                    return layer;
            }

            return null;
        }

        $scope.$watch("digitizeMode", function () {
            if (!$scope.mapData)
                return;

            $scope.mapData.selectedFeature = null;

            $scope.lf = null;
            $scope.lr = false;

            if ($scope.mapData.highlightInteraction)
                $scope.mapData.map.removeInteraction($scope.mapData.highlightInteraction);

            if ($scope.mapData.selectInteraction)
                $scope.mapData.map.removeInteraction($scope.mapData.selectInteraction);

            if ($scope.digitizeMode) {
                $scope.mapData.highlightInteraction = new ol.interaction.Select({
                    condition: ol.events.condition.pointerMove,
                    filter: $scope.selectFilter,
                });
            } else {
                $scope.mapData.highlightInteraction = new ol.interaction.Select({
                    condition: ol.events.condition.pointerMove
                });

                $scope.mapData.selectInteraction = new ol.interaction.Select({});
                $scope.mapData.selectInteraction.getFeatures().on("add", function (e) {
                    $scope.onFeatureSelected(e);
                });

                $scope.mapData.map.addInteraction($scope.mapData.selectInteraction);
            }

            $scope.mapData.map.addInteraction($scope.mapData.highlightInteraction);
        });


        //objectName is changed
        $scope.objectNameChanged = function () {
            if (!$scope.digitizeMode)
                return;

            if ($scope.digitizeType != 2)
                return;

            if (!$scope.dirty || !$scope.dirtyIds)
                return;

            var features = $scope.selectToEditInteraction.getFeatures().getArray();
            for (var i = 0; i < features.length; i++) {
                var feature = features[i];
                if (!$scope.dirtyIds[feature.getId()]) {
                    $scope.dirtyIds[feature.getId()] = true;
                    $scope.dirty.push(feature);
                }
            }
        }


        //insertion
        $scope.startInsertion = function () {
            $scope.digitizeMode = true;
            $scope.digitizeType = 1;
            $scope.mapData.sources.vector.clear();

            $scope.mapData.map.addInteraction($scope.currentLayer.draw);

            if ($scope.currentLayer.snap)
                $scope.currentLayer.snap.forEach(function (e, i, a) { if (e) $scope.mapData.map.addInteraction(e); })

            if ($scope.currentLayer.objectName) {
                $scope.objectName = $scope.currentLayer.objectNameDefaultValue;
            }
        }
        $scope.stopInsertion = function (commit) {
            $scope.digitizeMode = false;
            $scope.digitizeType = 0;

            $scope.mapData.map.removeInteraction($scope.currentLayer.draw);

            if ($scope.currentLayer.snap)
                $scope.currentLayer.snap.forEach(function (e, i, a) { if (e) $scope.mapData.map.removeInteraction(e); })

            if (commit) {
                var features = $scope.mapData.sources.vector.getFeatures();
                if (features.length > 0)
                    transactWFS('insert', features);
            }

            $scope.mapData.sources.vector.clear();
        }
        $scope.updateDependentLayers = function () {
            var dependentLayers = $scope.currentLayer.dependentLayers;

            if (!dependentLayers)
                return;

            for (var i = 0; i < dependentLayers.length; i++) {
                var dl = dependentLayers[i];

                if (dl)
                    dl.getSource().clear();
            }
        }




        //deletion
        $scope.selectToDeleteInteraction = null;
        $scope.startDeletion = function () {
            $scope.digitizeMode = true;
            $scope.digitizeType = 3;

            $scope.selectToDeleteInteraction = new ol.interaction.Select({
                filter: $scope.selectFilter,
                style: $scope.currentVectorStyle,
            })

            $scope.mapData.map.addInteraction($scope.selectToDeleteInteraction);

            if ($scope.mapData.selectedFeature) {
                $scope.selectToDeleteInteraction.getFeatures().push($scope.mapData.selectedFeature);
            }
        }
        $scope.stopDeletion = function (commit) {
            $scope.digitizeMode = false;
            $scope.digitizeType = 0;
            $scope.mapData.map.removeInteraction($scope.selectToDeleteInteraction);

            if (commit) {
                var features = $scope.selectToDeleteInteraction.getFeatures().getArray();
                if (features.length > 0)
                    transactWFS('delete', features);
            }

            $scope.selectToDeleteInteraction.getFeatures().clear();
        }



        //update
        $scope.selectToEditInteraction = null;
        $scope.modifyInteraction = null;
        $scope.startUpdate = function () {
            $scope.digitizeMode = true;
            $scope.digitizeType = 2;

            $scope.selectToEditInteraction = new ol.interaction.Select({
                filter: $scope.selectFilter,
                style: $scope.currentVectorStyle,
            })

            $scope.modifyInteraction = new ol.interaction.Modify({
                features: $scope.selectToEditInteraction.getFeatures()
            })

            $scope.mapData.map.addInteraction($scope.selectToEditInteraction);
            $scope.mapData.map.addInteraction($scope.modifyInteraction);

            if ($scope.currentLayer.snap)
                $scope.currentLayer.snap.forEach(function (e, i, a) { if (e) $scope.mapData.map.addInteraction(e); })

            $scope.dirty = [];
            $scope.dirtyIds = {};
            $scope.selectToEditInteraction.getFeatures().on('add', function (e) {
                e.element.on('change', function (e) {
                    if (!$scope.dirtyIds[e.target.getId()]) {
                        $scope.dirtyIds[e.target.getId()] = true;
                        $scope.dirty.push(e.target);
                    }
                });
            });

            if ($scope.mapData.selectedFeature) {
                $scope.selectToEditInteraction.getFeatures().push($scope.mapData.selectedFeature);
            }
        }
        $scope.stopUpdate = function (commit) {
            $scope.digitizeMode = false;
            $scope.digitizeType = 0;
            $scope.mapData.map.removeInteraction($scope.selectToEditInteraction);
            $scope.mapData.map.removeInteraction($scope.modifyInteraction);

            if ($scope.currentLayer.snap)
                $scope.currentLayer.snap.forEach(function (e, i, a) { if (e) $scope.mapData.map.removeInteraction(e); })

            if (commit) {
                var features = $scope.dirty;
                var clones = [];

                for (var i = 0; i < features.length; i++) {
                    var f = features[i];
                    var fp = f.getProperties();

                    delete fp.boundedBy;

                    var clone = new ol.Feature(fp);

                    clone.setId(f.getId());
                    clones.push(clone);
                }

                if (clones.length > 0)
                    transactWFS('update', clones);
            }

            $scope.selectToEditInteraction.getFeatures().clear();
            $scope.currentLayer.layer.getSource().clear();
        }





        //Сохранение данных
        var formatWFS = new ol.format.WFS();
        var transactWFS = function (p, features) {
            for (var i = 0; i < features.length; i++) {
                var f = features[i];
                f.setGeometry(f.getGeometry().transform('EPSG:3857', 'EPSG:4326'));

                if ($scope.currentLayer.objectName) {
                    f.set($scope.currentLayer.objectNameField, $scope.objectName)
                }
            }

            var swapXY = false;
            var node = null;
            switch (p) {
                case 'insert':
                    node = formatWFS.writeTransaction(features, null, null, $scope.currentLayer.gml);
                    break;
                case 'update':
                    swapXY = true;
                    node = formatWFS.writeTransaction(null, features, null, $scope.currentLayer.gml);
                    break;
                case 'delete':
                    node = formatWFS.writeTransaction(null, null, features, $scope.currentLayer.gml);
                    break;
            }

            if (swapXY) {
                var posLists = Array.prototype.slice.call(node.getElementsByTagName("posList"))
                                                    .concat(Array.prototype.slice.call(node.getElementsByTagName("pos")));

                for (var i = 0; i < posLists.length; i++) {
                    var posList = posLists[i].textContent;
                    var crds = posList.split(' ');

                    posList = "";
                    for (var j = 0; j < crds.length; j = j + 2) {
                        if (posList.length > 0)
                            posList = posList + " ";

                        posList = posList + crds[j + 1] + " " + crds[j];
                    }

                    posLists[i].textContent = posList;
                }
            }

            var s = new XMLSerializer();
            var str = s.serializeToString(node);

            //Загрузка настроек редактора
            var wfs = $resource('/api/biz/editor/commit', {}, {
                'commit': { method: 'POST' },
            });

            var result = wfs.commit({ data: str }).$promise.then(function () {
                $scope.currentLayer.layer.getSource().clear();
                $scope.updateDependentLayers();
            });
        }
    }
})();