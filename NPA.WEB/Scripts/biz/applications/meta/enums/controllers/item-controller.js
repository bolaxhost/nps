﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'meta' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание нового перечисления';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование перечисления';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление перечисления';
            $scope.read($routeParams.id);
        }

        $scope.tabs = ["Основные параметры", "Значения"];
        $scope.selectedTabIndex = 0;

        //Работа с коллекцией свойств
        $scope.enumValuesCntx = {};
        $scope.addEnumValue = function () {
            if ($scope.data.enumValue == undefined)
                $scope.data.enumValue = {}

            $scope.addValue($scope.enumValuesCntx, $scope.data.values, $scope.data.enumValue);
        }
        $scope.submitEditEnumValue = function () {
            $scope.submitEditValue($scope.enumValuesCntx, $scope.data.values, $scope.data.enumValue);
        }
        $scope.beginEditEnumValue = function (index) {
            if ($scope.data.enumValue == undefined)
                $scope.data.enumValue = {}

            $scope.beginEditValue($scope.enumValuesCntx, $scope.data.values, index, $scope.data.enumValue);
        }
        $scope.removeEnumValue = function (index) {
            $scope.removeValue($scope.enumValuesCntx, $scope.data.values, index);
        }
        $scope.moveUpEnumValue = function (index) {
            $scope.moveUpValue($scope.enumValuesCntx, $scope.data.values, index);
        }
        $scope.moveDownEnumValue = function (index) {
            $scope.moveDownValue($scope.enumValuesCntx, $scope.data.values, index);
        }
    }
})();