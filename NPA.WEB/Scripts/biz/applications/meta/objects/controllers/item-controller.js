﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizEnumService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizEnumService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'meta' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание нового определения объекта';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование определения объекта';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление определения объекта';
            $scope.read($routeParams.id);
        }

        $scope.tabs = ["Основные параметры", "Свойства"];
        $scope.selectedTabIndex = 0;

        //Инициализация сервиса перечислений
        BizEnumService.initResourceByDefault();

        //Типы расширений
        $scope.extensionTypes = BizEnumService.query('extension-types');
        $scope.getExtensionTypeName = function (id) {
            return $scope.getItemName($scope.extensionTypes, id);
        }

        //Типы свойств
        $scope.propertyTypes = BizEnumService.query('property-types');
        $scope.getPropertyTypeName = function (id) {
            return $scope.getItemName($scope.propertyTypes, id);
        }

        //Типы классификации
        $scope.propertyClassifications = BizEnumService.query('property-classifications');
        $scope.getPropertyClassificationName = function (id) {
            return $scope.getItemName($scope.propertyClassifications, id);
        }

        //Переинициализация сервиса перечислений с учётом id
        if ($routeParams.id != undefined) {
            BizEnumService.initResourceWithId($routeParams.id);
        } else {
            BizEnumService.initResource();
        }

        //Кандидаты базового объекта
        $scope.parentCandidates = BizEnumService.query('parent-candidates');
        $scope.getParentCandidateName = function (id) {
            return $scope.getItemName($scope.parentCandidates, id);
        }

        //Кандидаты вложенного объекта
        $scope.nestedObjectCandidates = BizEnumService.query('nested-candidates');
        $scope.getNestedObjectCandidateName = function (id) {
            return $scope.getItemName($scope.nestedObjectCandidates, id);
        }

        //Переинициализация сервиса перечислений для enum
        BizEnumService.initCustomResource("/api/biz/meta/enums/:name");

        //Перечисления
        $scope.propertyEnums = BizEnumService.query('enumeration');
        $scope.getPropertyEnumName = function (id) {
            return $scope.getItemName($scope.propertyEnums, id);
        }

        //Переинициализация сервиса перечислений для tab
        BizEnumService.initCustomResource("/api/biz/meta/tabs/:name");

        //Разделы
        $scope.formTabs = BizEnumService.query('enumeration', true);
        $scope.getTabName = function (id) {
            return $scope.getItemName($scope.formTabs, id);
        }

        //Подразделы
        $scope.tabSections = function (tabId) {
            if (tabId == null) { return null;}

            var methodName = "tabSectionsFor_" + tabId;

            if ($scope[methodName] === undefined) {
                BizEnumService.initCustomResource("/api/biz/meta/tabs/:name/:tabId", { tabId: tabId });
                $scope[methodName] = BizEnumService.query('sections');
            }

            return $scope[methodName];
        }
        $scope.getTabSectionName = function (tabId, id) {
            if (tabId == null) { return ""; }

            return $scope.getItemName($scope.tabSections(tabId), id);
        }

        //Обработка изменения типа свойства
        $scope.$watch('data.property.propertyType', function () {
            if ($scope.data == undefined)
                return;

            if ($scope.data.property == undefined)
                return;

            if ($scope.data.property.propertyType < 80 || $scope.data.property.propertyType > 82) { $scope.data.property.nestedObjectId = null; };
            if ($scope.data.property.propertyType != 50) { $scope.data.property.enumId = null; };
            if ($scope.data.property.propertyType != 60) { $scope.data.property.propertyClassification = null; };
        });

        //Работа с коллекцией свойств
        $scope.propertiesCntx = {};
        $scope.addProperty = function () {
            if ($scope.data.property == undefined)
                $scope.data.property = {}

            $scope.addValue($scope.propertiesCntx, $scope.data.properties, $scope.data.property);
        }
        $scope.submitEditProperty = function () {
            $scope.submitEditValue($scope.propertiesCntx, $scope.data.properties, $scope.data.property);
        }
        $scope.beginEditProperty = function (index) {
            if ($scope.data.property == undefined)
                $scope.data.property = {}

            $scope.beginEditValue($scope.propertiesCntx, $scope.data.properties, index, $scope.data.property);
        }
        $scope.removeProperty = function (index) {
            $scope.removeValue($scope.propertiesCntx, $scope.data.properties, index);
        }
        $scope.moveUpProperty = function (index) {
            $scope.moveUpValue($scope.propertiesCntx, $scope.data.properties, index);
        }
        $scope.moveDownProperty = function (index) {
            $scope.moveDownValue($scope.propertiesCntx, $scope.data.properties, index);
        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.forms = [
                $scope.form,
                $scope.properties
            ];
        })
    }
})();