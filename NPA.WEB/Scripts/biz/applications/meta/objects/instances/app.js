﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/search', {
                  templateUrl: '/api/biz/meta/objects/instances/search-form',
                  controller: 'ListCtrl'
              }).
              when('/create/:definitionId', {
                  templateUrl: function (params) { return '/api/biz/meta/objects/instances/edit-form/' + params.definitionId },
                  controller: 'ItemCtrl',
                  mode: "create",
              }).
              when('/update/:definitionId/:id', {
                  templateUrl: function (params) { return '/api/biz/meta/objects/instances/edit-form/' + params.definitionId },
                  controller: 'ItemCtrl',
                  mode: "update",
              }).
              when('/delete/:definitionId/:id', {
                  templateUrl: '/api/biz/meta/objects/instances/delete-form',
                  controller: 'ItemCtrl',
                  mode: "delete"
              }).
              otherwise({
                  redirectTo: '/search'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();