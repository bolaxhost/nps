﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizEnumService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizEnumService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'meta' };

        BizCRUDService.initResource({ definitionId: $routeParams.definitionId });
        BizControllerHelper.initCRUD($scope, BizCRUDService);

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание нового экземпляра объекта';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование экземпляра объекта';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление экземпляра объекта';
            $scope.read($routeParams.id);
        }
    }
})();