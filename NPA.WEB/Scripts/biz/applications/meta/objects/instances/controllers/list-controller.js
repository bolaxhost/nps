﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ListCtrl', ['$scope', '$rootScope', '$location', 'BizQueryService', 'BizControllerHelper', 'BizEnumService', ListController]);

    function ListController($scope, $rootScope, $location, BizQueryService, BizControllerHelper, BizEnumService) {
        $rootScope.pageHeader = { selected: 'meta' };

        BizControllerHelper.initList($scope, BizQueryService);

        //Определения объектов
        $scope.definitions = BizEnumService.query('definitions');
        $scope.getDefinitionName = function (id) {
            return $scope.getItemName($scope.definitions, id);
        }

        //Обновление значения текущего определения
        $scope.$watch('data.filter.definitionId', function (value) {
            $scope.definitionId = value;
        });

        $scope.doCreate = function () {
            $location.path("/create/" + $scope.definitionId);
        }

        //Получение данных
        $scope.extraFilterEnabled = true;
        $scope.extraFilter = false;
        $scope.query();
    }
})();