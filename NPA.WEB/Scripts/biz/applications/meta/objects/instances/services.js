﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizCRUDService', ['CRUDServiceApi',
        function (CRUDServiceApi) {
            return new CRUDServiceApi('/api/biz/meta/objects/instances/item/:definitionId');
        }])
        .factory('BizQueryService', ['QueryServiceApi',
        function (QueryServiceApi) {
            return new QueryServiceApi('/api/biz/meta/objects/instances/query');
        }])
        .factory('BizEnumService', ['EnumServiceApi',
        function (EnumServiceApi) {
            return new EnumServiceApi('/api/biz/meta/objects/instances');
        }])
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }]);
})();