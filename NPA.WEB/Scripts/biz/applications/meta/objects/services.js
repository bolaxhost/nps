﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizCRUDService', ['CRUDServiceApi',
        function (CRUDServiceApi) {
            return new CRUDServiceApi('/api/biz/meta/objects/item');
        }])
        .factory('BizQueryService', ['QueryServiceApi',
        function (QueryServiceApi) {
            return new QueryServiceApi('/api/biz/meta/objects/query');
        }])
        .factory('BizEnumService', ['EnumServiceApi',
        function (EnumServiceApi) {
            return new EnumServiceApi('/api/biz/meta/objects');
        }])
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }]);
})();