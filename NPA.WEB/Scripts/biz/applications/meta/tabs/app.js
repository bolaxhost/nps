﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/search', {
                  templateUrl: '/api/biz/meta/tabs/search-form',
                  controller: 'ListCtrl'
              }).
              when('/create', {
                  templateUrl: '/api/biz/meta/tabs/edit-form',
                  controller: 'ItemCtrl',
                  mode: "create"
              }).
              when('/update/:id', {
                  templateUrl: '/api/biz/meta/tabs/edit-form',
                  controller: 'ItemCtrl',
                  mode: "update"
              }).
              when('/delete/:id', {
                  templateUrl: '/api/biz/meta/tabs/delete-form',
                  controller: 'ItemCtrl',
                  mode: "delete"
              }).
              otherwise({
                  redirectTo: '/search'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();