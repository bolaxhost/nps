﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$location', 'BizCRUDService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $route, $routeParams, $location, BizCRUDService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'meta' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание нового набора закладок';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование набора закладок';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление набора закладок';
            $scope.read($routeParams.id);
        }

        $scope.tabs = ["Основные параметры", "Значения"];
        $scope.selectedTabIndex = 0;

        //Работа с коллекцией свойств
        $scope.tabValuesCntx = {};
        $scope.addTabValue = function () {
            if ($scope.data.tabValue == undefined)
                $scope.data.tabValue = {}

            $scope.addValue($scope.tabValuesCntx, $scope.data.values, $scope.data.tabValue);
        }
        $scope.submitEditTabValue = function () {
            $scope.submitEditValue($scope.tabValuesCntx, $scope.data.values, $scope.data.tabValue);
        }
        $scope.beginEditTabValue = function (index) {
            if ($scope.data.tabValue == undefined)
                $scope.data.tabValue = {}

            $scope.beginEditValue($scope.tabValuesCntx, $scope.data.values, index, $scope.data.tabValue);
        }
        $scope.removeTabValue = function (index) {
            $scope.removeValue($scope.tabValuesCntx, $scope.data.values, index);
        }
        $scope.moveUpTabValue = function (index) {
            $scope.moveUpValue($scope.tabValuesCntx, $scope.data.values, index);
        }
        $scope.moveDownTabValue = function (index) {
            $scope.moveDownValue($scope.tabValuesCntx, $scope.data.values, index);
        }
    }
})();