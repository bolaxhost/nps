﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizCRUDService', ['CRUDServiceApi',
        function (CRUDServiceApi) {
            return new CRUDServiceApi('/api/biz/meta/tabs/item');
        }])
        .factory('BizQueryService', ['QueryServiceApi',
        function (QueryServiceApi) {
            return new QueryServiceApi('/api/biz/meta/tabs/query');
        }])
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }]);
})();