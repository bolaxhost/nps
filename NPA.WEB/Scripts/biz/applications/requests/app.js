﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/search', {
                  templateUrl: '/api/biz/requests/search-form',
                  controller: 'ListCtrl'
              }).
              when('/create', {
                  templateUrl: '/api/biz/requests/edit-form',
                  controller: 'ItemCtrl',
                  mode: "create"
              }).
              when('/update/:id', {
                  templateUrl: '/api/biz/requests/edit-form',
                  controller: 'ItemCtrl',
                  mode: "update"
              }).
              when('/delete/:id', {
                  templateUrl: '/api/biz/requests/delete-form',
                  controller: 'ItemCtrl',
                  mode: "delete"
              }).
              when('/print/:id', {
                  templateUrl: '/api/biz/requests/print-form',
                  controller: 'ItemCtrl',
                  mode: "print"
              }).
              otherwise({
                  redirectTo: '/search'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();