﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('ItemCtrl', ['$scope', '$rootScope', '$resource', '$route', '$sce', '$routeParams', '$location', 'BizCRUDService', 'BizControllerHelper', ItemController]);

    function ItemController($scope, $rootScope, $resource, $route, $sce, $routeParams, $location, BizCRUDService, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'biz' };

        BizControllerHelper.initCRUD($scope, BizCRUDService);

        //Инициализация данных
        if ($route.current.mode == 'create') {
            $scope.title = 'Создание новой заявки';
            $scope.create();
        } else if ($route.current.mode == 'update') {
            $scope.title = 'Редактирование заявки';
            $scope.read($routeParams.id);
        } else if ($route.current.mode == 'print') {
            $scope.title = 'Печать заявки';
            $scope.view($routeParams.id);
        } else if ($route.current.mode == 'delete') {
            $scope.title = 'Удаление заявки';
            $scope.read($routeParams.id);
        }

        $scope.tabs = ["Заполнение параметров", "Определение категории заявки", "Прикрепление файлов", "Поиск на публичной кадастровой карте"];
        $scope.selectedTabIndex = 0;
        $scope.nowDt = new Date(Date.now());

        $scope.wfsData = {};
        $scope.getWFS = function (filter) {
            var resource = $resource('/api/biz/requests/wfs', {}, {
                'query': { method: 'POST', params: { filter: filter } }
            });

            try{
                resource.query().$promise.then(function (data) {
                    $scope.wfsData = JSON.parse(data.data);
                    $scope.data.isPreferential = ($scope.wfsData.totalFeatures > 0);
                }
                , function () {
                    $scope.wfsData = { totalFeatures: 0 };
                    $scope.data.isPreferential = false;
                });

            } catch (ex) {
                $scope.wfsData = { totalFeatures: 0 };
                $scope.data.isPreferential = false;
            }
        }

        $scope.clearMapReference = function() {
            $scope.data.publicMapReference = "";
        }

        $scope.gotoPublicMapReference = function () {
            if (!$scope.data.publicMapReference)
                return;

            if ($scope.data.publicMapReference.length == 0)
                return;

            document.getElementById('iframe').src = $scope.data.publicMapReference;
        }

        $scope.gotoMapReference = function () {

            if (!$scope.data.publicMapReference) {
                if (!$scope.data.cadasterNumber)
                    return;

                var pmap = $resource('http://pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/exts/GKNServiceExtension/online/parcel/find', {}, {
                    'find': {
                        method: 'GET',
                        params: {
                            cadNum: $scope.data.cadasterNumber,
                            f: 'pjson'
                        }
                    }
                });

                alert("find")

                pmap.find().$promise.then(function (data) {
                    alert("goto2");
                    try {
                        var result = data.features[0].attributes;
                        $scope.mapData.view.setCenter([result.XC, result.YC]);

                        var extent = [result.XMIN, result.YMIN, result.XMAX, result.YMAX];
                        $scope.mapData.view.fit(extent, $scope.mapData.map.getSize());

                    } catch (err) { alert(err); return; }
                }, function (err) { alert("error"); })

                return;
            }

            if ($scope.data.publicMapReference.length == 0)
                return;

            try {
                var l = parseInt($scope.data.publicMapReference.match(/l=[0-9]*/i).toString().substring(2));
                var x = parseFloat($scope.data.publicMapReference.match(/x=[0-9]*.[0-9]*/i).toString().substring(2));
                var y = parseFloat($scope.data.publicMapReference.match(/y=[0-9]*.[0-9]*/i).toString().substring(2));

                $scope.mapData.view.setCenter([x, y]);
                $scope.mapData.view.setZoom(l);
            } catch (err) { return; }
        }


        $scope.submitRequest = function () {
            //Сохранение контекста карты, если она была загружена
            if ($scope.mapData) {
                var mapContext = { center: $scope.mapData.view.getCenter(), zoom: $scope.mapData.view.getZoom() }
                $scope.data.nativeMapContext = JSON.stringify(mapContext);

                var geojson = new ol.format.GeoJSON();

                var regionGeometry = geojson.writeFeatures($scope.mapData.sources.vector.getFeatures());
                $scope.data.regionGeometry = JSON.stringify(regionGeometry);

                var bufferGeometry = geojson.writeFeatures($scope.mapData.sources.buffer.getFeatures());
                $scope.data.bufferGeometry = JSON.stringify(bufferGeometry);
            }

            $scope.submit();
        }

        $scope.switchPreferential = function () {
            $scope.data.isPreferential = !$scope.data.isPreferential;
        }

        $scope.switchLocationType = function () {
            if ($scope.data.locationType == 0)
                $scope.data.locationType = 1
            else
                $scope.data.locationType = 0

            $scope.calculateBuffer();
        }

        $scope.printRequest = function () {
            window.print();
        }

        $scope.switchPrintMode = function () {
            $rootScope._printMode = !$rootScope._printMode
        }













        //Загрузка карты
        if ($route.current.mode == 'print') {
            window.setTimeout(function () {
                $scope.initializeMap();
                $scope.finalMapInitialing();
                $scope.mapData.synchronizeCenter();
                $scope.mapData.synchronizeZoom();
            }, 1000);
        }

        $scope.onTabSelected = function (index) {
            if (index == 1 && !$scope.mapIsInitialized) {
                window.setTimeout(function () {
                    $scope.initializeMap();
                    $scope.finalMapInitialing();
                    $scope.mapData.synchronizeCenter();
                    $scope.mapData.synchronizeZoom();
                }, 1000);
            }
        };

        var defaultCenter = [4187136.7833, 7509542.7662];
        var defaultZoom = 5;

        //Первоначальная загрузка карты
        $scope.initializeMap = function () {
            $scope.mapData = { layers: {}, sources: {} };

            //Яндекс карты
            //proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs');

            //$scope.mapData.layers.yandexMapLayerMap = new ol.layer.Tile({
            //    'title': 'Яндекс Карты',
            //    type: 'base',
            //    source: new ol.source.XYZ({
            //        projection: 'EPSG:3395',
            //        url: 'http://vec02.maps.yandex.net?l=map&v=2.8.3&z={z}&x={x}&y={y}'
            //    })
            //});

            //$scope.mapData.layers.yandexMapLayerSat = new ol.layer.Tile({
            //    source: new ol.source.XYZ({
            //        projection: 'EPSG:3395',
            //        url: 'http://sat02.maps.yandex.net?l=sat&v=2.8.3&z={z}&x={x}&y={y}'
            //    })
            //});


            //Данные по сети
            if ($scope.data.settings.layerLinesHVName && $scope.data.settings.layerLinesHVName.length > 0) {
                $scope.mapData.sources.linesHV = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerLinesHVName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.linesHV = new ol.layer.Tile({
                    source: $scope.mapData.sources.linesHV
                });
            }

            if ($scope.data.settings.layerLinesMVName && $scope.data.settings.layerLinesMVName.length > 0) {
                $scope.mapData.sources.linesMV = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerLinesMVName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.linesMV = new ol.layer.Tile({
                    source: $scope.mapData.sources.linesMV
                });
            }


            if ($scope.data.settings.layerLinesLVName && $scope.data.settings.layerLinesLVName.length > 0) {
                $scope.mapData.sources.linesLV = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerLinesLVName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.linesLV = new ol.layer.Tile({
                    source: $scope.mapData.sources.linesLV
                });
            }

            if ($scope.data.settings.layerPoleTransmissionName && $scope.data.settings.layerPoleTransmissionName.length > 0) {
                $scope.mapData.sources.polesT = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerPoleTransmissionName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.polesT = new ol.layer.Tile({
                    source: $scope.mapData.sources.polesT
                });
            }

            if ($scope.data.settings.layerPoleDistributionName && $scope.data.settings.layerPoleDistributionName.length > 0) {
                $scope.mapData.sources.polesD = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerPoleDistributionName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.polesD = new ol.layer.Tile({
                    source: $scope.mapData.sources.polesD
                });
            }


            if ($scope.data.settings.layerSubstationPointHVName && $scope.data.settings.layerSubstationPointHVName.length > 0) {
                $scope.mapData.sources.subPointHV = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerSubstationPointHVName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.subPointHV = new ol.layer.Tile({
                    source: $scope.mapData.sources.subPointHV
                });
            }


            if ($scope.data.settings.layerSubstationPointMVName && $scope.data.settings.layerSubstationPointMVName.length > 0) {
                $scope.mapData.sources.subPointMV = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerSubstationPointMVName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.subPointMV = new ol.layer.Tile({
                    source: $scope.mapData.sources.subPointMV
                });
            }

            if ($scope.data.settings.layerSubstationAreaName && $scope.data.settings.layerSubstationAreaName.length > 0) {
                $scope.mapData.sources.subArea = new ol.source.TileWMS({
                    url: $scope.data.settings.wmsUrl,
                    params: {
                        'FORMAT': 'image/png',
                        'VERSION': '1.1.1',
                        tiled: true,
                        LAYERS: $scope.data.settings.layerSubstationAreaName,
                        STYLES: '',
                    }
                });

                $scope.mapData.layers.subArea = new ol.layer.Tile({
                    source: $scope.mapData.sources.subArea
                });
            }

            //ОСМ
            //$scope.mapData.layers.topo = new ol.layer.Tile({
            //    source: new ol.source.MapQuest({ layer: 'osm' })
            //});






            $scope.mapData.layers.rosreg = new ol.layer.Tile({
                source: new ol.source.TileArcGISRest({
                    url: $scope.data.settings.rosregUrl
                })
            });








            $scope.mapData.sources.vector = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.vector = new ol.layer.Vector({
                source: $scope.mapData.sources.vector,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 255, 0, 0.5)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#ff0000',
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                })
            });

            $scope.mapData.sources.buffer = new ol.source.Vector({ wrapX: false });
            $scope.mapData.layers.buffer = new ol.layer.Vector({
                source: $scope.mapData.sources.buffer,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 255, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#00ff00',
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                })
            });






            var mapProjection = new ol.proj.Projection({
                code: 'EPSG:3857',
                units: 'm'
            });

            $scope.mapData.view = new ol.View({
                center: defaultCenter,
                zoom: defaultZoom,
                maxZoom: 21,
                projection: mapProjection
            });

            $scope.mapData.interaction = new ol.interaction.Draw({
                source: $scope.mapData.sources.vector,
                type: 'Polygon',
            });




            //Список слоев
            var layers = [
                    $scope.mapData.layers.rosreg,
                    $scope.mapData.layers.vector,
                    $scope.mapData.layers.buffer,
            ];

            if ($scope.mapData.layers.linesHV) {
                layers.push($scope.mapData.layers.linesHV);
            }

            if ($scope.mapData.layers.linesMV) {
                layers.push($scope.mapData.layers.linesMV);
            }

            if ($scope.mapData.layers.linesLV) {
                layers.push($scope.mapData.layers.linesLV);
            }

            if ($scope.mapData.layers.polesT) {
                layers.push($scope.mapData.layers.polesT);
            }

            if ($scope.mapData.layers.polesD) {
                layers.push($scope.mapData.layers.polesD);
            }

            if ($scope.mapData.layers.subPointHV) {
                layers.push($scope.mapData.layers.subPointHV);
            }

            if ($scope.mapData.layers.subPointMV) {
                layers.push($scope.mapData.layers.subPointMV);
            }

            if ($scope.mapData.layers.subArea) {
                layers.push($scope.mapData.layers.subArea);
            }

            $scope.mapData.layersGroup = new ol.layer.Group({
                layers: layers,
            });






            $scope.mapData.map = new ol.Map({
                target: 'olmap',
                view: $scope.mapData.view
            });


            //gmap
            $scope.mapData.gmap = new google.maps.Map(document.getElementById('gmap'), {
                disableDefaultUI: true,

                keyboardShortcuts: false,
                draggable: false,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                streetViewControl: false,

                center: new google.maps.LatLng(defaultCenter[1], defaultCenter[0]),
                zoom: defaultZoom,

                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });

            $scope.mapData.synchronizeCenter = function () {
                var center = ol.proj.transform($scope.mapData.view.getCenter(), 'EPSG:3857', 'EPSG:4326');
                $scope.mapData.gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
            }

            $scope.mapData.view.on('change:center', function () {
                $scope.mapData.synchronizeCenter();
            });

            $scope.mapData.synchronizeZoom = function () {
                $scope.mapData.gmap.setZoom($scope.mapData.view.getZoom());
            }

            $scope.mapData.view.on('change:resolution', function () {
                $scope.mapData.synchronizeZoom();
            });
            ///

            $scope.mapData.showRosReg = true;
            $scope.mapData.showTopo = true;
            $scope.mapData.map.setLayerGroup($scope.mapData.layersGroup);

            $scope.mapIsInitialized = true;
        }

        //Легенда
        $scope.switchRosReg = function () {
            $scope.mapData.showRosReg = !$scope.mapData.showRosReg;
            $scope.mapData.layers.rosreg.setVisible($scope.mapData.showRosReg);

        }

        $scope.switchTopo = function () {
            $scope.mapData.showTopo = !$scope.mapData.showTopo;
            $scope.mapData.layers.topo.setVisible($scope.mapData.showTopo);
        }

        $scope.switchMapType = function () {
            if ($scope.mapData.mapType == undefined)
                $scope.mapData.mapType = 0;

            $scope.mapData.mapType++;

            if ($scope.mapData.mapType == 4)
                $scope.mapData.mapType = 0;

            switch ($scope.mapData.mapType) {
                case 0: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.ROADMAP); break;
                case 1: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.SATELLITE); break;
                case 2: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.HYBRID); break;
                case 3: $scope.mapData.gmap.setMapTypeId(google.maps.MapTypeId.TERRAIN); break;
            }
        }

        //Восстановление сохраненных параметров карты
        $scope.finalMapInitialing = function () {
            if ($scope.data.nativeMapContext && $scope.data.nativeMapContext.length > 0) {
                var mapContext = JSON.parse($scope.data.nativeMapContext);

                $scope.mapData.view.setCenter(mapContext.center);
                $scope.mapData.view.setZoom(mapContext.zoom);
            } else $scope.gotoMapReference();

            var geojson = new ol.format.GeoJSON();

            if ($scope.data.regionGeometry && $scope.data.regionGeometry.length > 0) {
                var regionGeometry = JSON.parse($scope.data.regionGeometry);
                $scope.mapData.sources.vector.addFeatures(geojson.readFeatures(regionGeometry));

                $scope.calculateBuffer();
            }

            //if ($scope.data.bufferGeometry && $scope.data.bufferGeometry.length > 0) {
            //    var bufferGeometry = JSON.parse($scope.data.bufferGeometry);
            //    $scope.mapData.sources.buffer.addFeatures(geojson.readFeatures(bufferGeometry));
            //}
        }


        //Оцифровка учатска и построение буфера
        $scope.digitizeMode = false;
        $scope.digitizeModeName = "Оцифровать контур";
        $scope.startStopDigitizing = function () {
            $scope.digitizeMode = !$scope.digitizeMode;

            if ($scope.digitizeMode) {
                $scope.mapData.sources.vector.clear();
                $scope.mapData.sources.buffer.clear();

                $scope.mapData.map.addInteraction($scope.mapData.interaction);
                $scope.digitizeModeName = 'Завершить оцифровку';

                $scope.data.isPreferential = false;
                return;
            } else {
                $scope.mapData.map.removeInteraction($scope.mapData.interaction);
                $scope.digitizeModeName = 'Оцифровать контур';
            }

            $scope.calculateBuffer();
        }

        $scope.projDistanceToRealDistance = function(distance, coordinate) {
            var p1 = coordinate;
            var p2 = [coordinate[0] + 100, coordinate[1]];

            var wgs84Sphere = new ol.Sphere(6378137);

            var groundDistance = wgs84Sphere.haversineDistance(
                ol.proj.transform(p1, 'EPSG:3857', 'EPSG:4326'),
                ol.proj.transform(p2, 'EPSG:3857', 'EPSG:4326')
            );

            if (groundDistance == 0)
                return distance;

            return distance * 100 / groundDistance;
        }

        $scope.calculateBuffer = function () {
            var wcrds = "";
            $scope.mapData.sources.buffer.clear();

            var parser = new jsts.io.olParser();
            var features = $scope.mapData.sources.vector.getFeatures();
            for (var i = 0; i < features.length; i++) {
                var feature = features[i];
                var geometry = feature.getGeometry();

                var jstsGeometry = parser.read(geometry);
                var jstsBuffer = jstsGeometry.buffer($scope.projDistanceToRealDistance($scope.getBufferWidth(), geometry.getFirstCoordinate()));

                var buffer = parser.write(jstsBuffer);
                var bufferFeature = new ol.Feature({
                    name: "Buffer",
                    geometry: buffer
                });

                var crds = bufferFeature.getGeometry().getCoordinates()[0];
                for (var i = 0; i < crds.length; i++)
                {
                    var crd = crds[i];
                    var lonlat = ol.proj.transform(crd, 'EPSG:3857', 'EPSG:4326');
                    var lon = lonlat[0];
                    var lat = lonlat[1];

                    if (wcrds.length > 0)
                        wcrds = wcrds + ", ";

                    wcrds = wcrds + lat.toString() + " " + lon.toString();
                }

                $scope.mapData.sources.buffer.addFeature(bufferFeature);
                $scope.getWFS("cql_filter=intersects(geometry, polygon((" + wcrds + ")))");
            }
        }


        $scope.getBufferWidth = function () {
            if ($scope.data.locationType == 1)
                return 500;

            return 300;
        }
    }
})();