﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .factory('BizCRUDService', ['CRUDServiceApi',
        function (CRUDServiceApi) {
            return new CRUDServiceApi('/api/biz/requests/item');
        }])
        .factory('BizQueryService', ['QueryServiceApi',
        function (QueryServiceApi) {
            return new QueryServiceApi('/api/biz/requests/query');
        }])
        .factory('BizControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }]);
})();