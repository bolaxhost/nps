﻿'use strict';

_transition = false;

(function () {
    angular
        .module('NpsControllerHelpers', ['NpsAppConfig'])
        //Значение перечислений
        .factory('BizMetaEnumService', ['EnumServiceApi',
        function (EnumServiceApi) {
            return new EnumServiceApi('/api/biz/meta/enums/values');
        }])
        //Экземпляры метаобъектов для заполнения ссылок
        .factory('BizMetaObjectInstanceService', ['EnumServiceApi',
        function (EnumServiceApi) {
            return new EnumServiceApi('/api/biz/meta/objects/instances/enumeration');
        }])
        .service('CommonControllerApi', ['$location', '$resource', '$window', 'BizMetaEnumService', 'BizMetaObjectInstanceService', CommonControllerApi])

        //Account Info Infrastructure
        .factory('AccountInfoControllerHelper', ['CommonControllerApi',
        function (CommonControllerApi) {
            return new CommonControllerApi('/search');
        }])
        .controller('AccountInfoCtrl', ['$scope', '$rootScope', '$resource', '$route', '$routeParams', '$location', 'AccountInfoControllerHelper', AccountInfoController]);

    //Контроллер управления учетной записью
    function AccountInfoController($scope, $rootScope, $resource, $route, $routeParams, $location, AccountInfoControllerHelper) {
        AccountInfoControllerHelper.initBase($scope);

        //Информация о пользователе
        var accountInfoResource = $resource('/api/admin/:method', {}, {
            'getInfo': { method: 'GET', params: {method: 'account-info'} },
            'logOut': { method: 'POST', params: { method: 'logout' } },
        });

        $scope.data = accountInfoResource.getInfo();
        $scope.accountInfo = $scope.data;
        $scope.accountInfo.$promise.then(function () {
            $rootScope.$isSupervisor = $scope.accountInfo.isSupervisor;
            $rootScope.$userOperations = $scope.accountInfo.operations;

            $rootScope.operationWithPrefixIsPermitted = function (prefix) {
                if ($rootScope.$isSupervisor)
                    return true;

                if (!$rootScope.$userOperations)
                    return false;

                for (var i = 0; i < $rootScope.$userOperations.length; i++)
                    if ($rootScope.$userOperations[i].startsWith(prefix))
                        return true;

                return false;
            }

            $rootScope.$admIsPermitted = $rootScope.operationWithPrefixIsPermitted('adm');
            $rootScope.$bizIsPermitted = $rootScope.operationWithPrefixIsPermitted('biz');
            $rootScope.$metaIsPermitted = $rootScope.operationWithPrefixIsPermitted('meta');
            $rootScope.$spatialEditorIsPermitted = $rootScope.operationWithPrefixIsPermitted('data');
        })

        //Выход из системы
        $scope.logOut = function () {
            if (!$scope.accountInfo)
                return;

            if (!$scope.data.isAuthorized)
                return;

            $scope.data.$logOut().then($scope.createRequestResultHandler(function () {
                $scope.gotoHref('http:/');
            }), $scope.resourceErrorHandler);
        }

        $scope.logIn = function () {
            $scope.gotoHref('http:/Account/Login');
        }
    }

    //API контроллера
    function CommonControllerApi($location, $resource, $window, BizMetaEnumService, BizMetaObjectInstanceService) {
        var CommonController = function (returnUrl) {
            //общая инициализация
            var baseInit = function (scope) {
                //Список филиалов
                scope.branches = GetBranches();
                scope.getBranchName = function (id) {
                    return scope.getItemName(scope.branches, id);
                }



                //Возврат из контроллера
                scope.returnUrl = returnUrl;

                //Флаг инициализации страницы
                scope._pageIsInitialized = false;
                //флаг отправки формы
                scope._submitting = false;

                scope.getFormInputValue = function (id) {
                    return document.getElementById(id).value;
                }


                //Сервисы работы с номером телефона
                scope.getNumberExpression = function (text) {
                    if (!text || text.length == 0)
                        return "";

                    var regex = /[0-9]+/gi;
                    return regex.exec(text);
                }

                scope.normalizePhoneExpression = function (phone) {
                    if (!phone || phone.length == 0)
                        return "";

                    return phone.replace(/-/g, "");
                }

                scope.getPhoneCountryCode = function (phone) {
                    return "7"

                    //Только код РФ
                    //var regex = /^(\+)*\s*[0-9]{1,4}(\s|\()/gi;
                    //return scope.getNumberExpression(regex.exec(scope.normalizePhoneExpression(phone)));
                }

                scope.getPhoneCityCode = function (phone) {
                    var regex = /\(\s*[0-9]{1,}\s*\)/gi;
                    return scope.getNumberExpression(regex.exec(scope.normalizePhoneExpression(phone)));
                }

                scope.getPhoneExtensionCode = function (phone) {
                    var regex = /доб.:\s*[0-9]{1,}/gi;
                    return scope.getNumberExpression(regex.exec(scope.normalizePhoneExpression(phone)));
                }

                scope.getPhoneMainCode = function (phone) {
                    var regex = /[0-9]{6,}/gi;
                    return scope.getNumberExpression(regex.exec(scope.normalizePhoneExpression(phone)));
                }

                scope.buildPhoneNumber = function (country, city, main, extension) {
                    var result = "";

                    if (country && country.length > 0)
                        result = result + "+" + country + " ";

                    if (city && city.length > 0)
                        result = result + "(" + city + ") ";

                    if (main && main.length > 0)
                        result = result + main;

                    if (extension && extension.length > 0)
                        result = result + ", доб.: " + extension;

                    return result;
                }







                //Базовый обработчик ошибок
                scope.resourceErrorHandler = function (response) {
                    alert('Ошибка ресурса ' + response.status + '!');
                };

                //Обработчик ошибок бизнес запросов (базовый)
                scopeResourceErrorHandler = function (data) {
                    alert('Ошибка бизнес-логики! ' + data.message);
                };

                //Обработчик ошибок бизнес запросов, формирующих объект scope.data
                scopeErrorHandler = function () {
                    scopeResourceErrorHandler(scope.data);
                };




                //Постобработка запросов по умолчанию
                scope.doDataProcessing = function (data) { }




                //Обработчик бизнес запросов (базовый)
                scopeResourceRequestHandler = function (data, skipDoDataProcessing) {
                    if (data.state == 'error') {
                        scopeResourceErrorHandler(data);
                        return false;
                    }

                    //Обработка полей DateTime
                    scope.dateTimeCorrection(data);

                    if (!skipDoDataProcessing)
                        scope.doDataProcessing(data);

                    return true;
                }

                //Создание обработчика бизнес запроса на основе базового
                scope.createBizResourceRequestHandler = function (postProcessing, dataOwner) {
                    var handler = function (data) {
                        if (dataOwner)
                            dataOwner.data = data;

                        if (!scopeResourceRequestHandler(data, postProcessing))
                            return false;

                        //Постобработка
                        if (postProcessing)
                            postProcessing(data);

                        return true;
                    }

                    return handler;
                }




                //Обработчик бизнес запросов, формирующих объект scope.data (базовый)
                scope.requestResultHandler = function () {
                    return scopeResourceRequestHandler(scope.data)
                }

                //Создание обработчика бизнес запроса, формирующего объект scope.data, на основе базового
                scope.createRequestResultHandler = function (postProcessing) {
                    var handler = function () {
                        if (!scope.requestResultHandler())
                            return false;

                        //Постобработка
                        if (postProcessing)
                            postProcessing(scope.data);

                        return true;
                    }

                    return handler;
                }



                //Базовый API
                scope.isObject = function (val) {
                    if (val === null) { return false; }
                    if (val instanceof Date) { return false; }
                    return ((typeof val === 'object'));
                }

                scope.isString = function (val) {
                    if (val === null) { return false; }
                    return ((typeof val === 'string'));
                }

                scope.isArray = function (val) {
                    if (val === null) { return false; }
                    return Array.isArray(val);
                }

                scope.booleanToString = function (value) {
                    if (value == null || value == undefined)
                        return "";

                    if (value)
                        return "Да";

                    return "Нет";
                }

                scope.correctDateTime = function (serverDateTime) {
                    return new Date(parseInt(serverDateTime.replace("/Date(", "").replace(")/", ""), 10))
                }

                //Коррекция объектов с датой для работы на стороне клиента
                scope.dateTimeCorrection = function (source) {
                    if (source === null) { return; }
                    for (var attr in source) {
                        if (source.hasOwnProperty(attr) && attr.indexOf("$") == -1) {
                            var val = source[attr];
                            if (scope.isString(val) && val.indexOf("/Date") != -1) {
                                source[attr] = scope.correctDateTime(val);
                            }
                            else {
                                if (scope.isObject(val)) {
                                    scope.dateTimeCorrection(val);
                                }
                            }
                        }
                    }
                }





                //VMDS Api
                var vmdsResource = $resource('/api/biz/control/vmds/:method/:id', {}, {
                    'getVMDS': { method: 'GET' },
                    'setVMDS': { method: 'POST' },
                    'gotoActual': { method: 'PUT' },
                    'goto': { method: 'PUT', params: { method: 'version' } },
                    'getVersion': { method: 'GET', params: { method: 'version' } },
                    'setVersion': { method: 'POST', params: {method: 'version'} },
                    'getVersions': { method: 'GET', isArray: true, params: {method: 'versions'} },
                });

                scope.getVMDS = function () {
                    scope.vmds = vmdsResource.getVMDS();
                    scope.vmds.$promise.then(function () {
                        if (scope.vmds.snapshot == null) {
                            scope.snapshotChanged();
                            return;
                        }

                        scope.vmds.snapshot = scope.correctDateTime(scope.vmds.snapshot);
                        scope.snapshotChanged();
                    })
                }

                scope.setVMDS = function (postback) {
                    scope.vmds.$setVMDS().then(function () {
                        scope.getVMDS();
                        scope.afterVMDSChanged(postback)
                    });
                }

                //Переход к версии объекта
                scope.gotoObjectVersion = function (id, version, postback) {
                    scope.vmds.$goto({id: id, version: version}).then(function () {
                        scope.getVMDS();
                        scope.afterVMDSChanged(postback)
                    });
                }

                //Переход к актуальным данным
                scope.gotoActualData = function (postback) {
                    scope.vmds.$gotoActual().then(function () {
                        scope.getVMDS();
                        scope.afterVMDSChanged(postback)
                    });
                }

                //Текущая версия объекта
                scope.getCurrentVersion = function () {
                    if (scope.id == null)
                        return null;

                    return vmdsResource.getVersion({ id: scope.id });
                }

                //Все версии объекта
                scope.getVersions = function () {
                    if (scope.id == null)
                        return [];

                    scope.hasActualVersion = false;

                    var versions = vmdsResource.getVersions({ id: scope.id });
                    versions.$promise.then(function (result)
                    {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].isActual) {
                                scope.hasActualVersion = true;
                                return;
                            }
                        }
                    })

                    return versions;
                }

                //Выбор текущей версии объекта (связан с директивой npa-version)
                scope.setVersion = function () {
                    scope.currentVersion.$setVersion({ id: scope.id }).then(function () { scope.reRead(); scope.getVMDS(); });
                }

                //Переход к актуальной версии объекта
                scope.gotoActualVersion = function () {
                    scope.gotoActualData(function () { scope.reRead(); });
                }

                scope.afterVMDSChanged = function (postback) {
                    if (postback == undefined || postback == null)
                        return;

                    postback();
                }

                scope.snapshotChanged = function () {
                    if (scope.vmds.snapshot == null) {
                        scope.vmds.SnapshotToString = "Актуальные данные";
                        return;
                    }

                    scope.vmds.SnapshotToString = scope.vmds.snapshot.toString();
                }





                //Общие данные и справочники 
                var now = new Date();
                var year = now.getFullYear();
                scope.years = [];
                for (var i = year - 10; i <= year + 10; i++) {
                    scope.years.push({ id: i, name: i });
                }

                scope._yearsRange = null;
                scope.yearsRange = function (startYear, endYear) {
                    if (scope._yearsRange != null)
                        return scope._yearsRange;

                    scope._yearsRange = [];
                    for (var i = startYear; i <= endYear; i++) {
                        scope._yearsRange.push({ id: i, name: i });
                    }

                    return scope._yearsRange;
                }

                scope.months = [
                    { id: 1, name: "Январь", quarter: 1 },
                    { id: 2, name: "Февраль", quarter: 1 },
                    { id: 3, name: "Март", quarter: 1 },
                    { id: 4, name: "Апрель", quarter: 2 },
                    { id: 5, name: "Май", quarter: 2 },
                    { id: 6, name: "Июнь", quarter: 2 },
                    { id: 7, name: "Июль", quarter: 3 },
                    { id: 8, name: "Август", quarter: 3 },
                    { id: 9, name: "Сентябрь", quarter: 3 },
                    { id: 10, name: "Октябрь", quarter: 4 },
                    { id: 11, name: "Ноябрь", quarter: 4 },
                    { id: 12, name: "Декабрь", quarter: 4 }
                ];

                scope.quarters = [
                    { id: 1, name: "1-й квартал" },
                    { id: 2, name: "2-й квартал" },
                    { id: 3, name: "3-й квартал" },
                    { id: 4, name: "4-й квартал" }
                ]

                scope.currencyValues = [
                    { id: "RUB", name: "Российский рубль" },
                ]



                //Работа с массивами
                scope.getItemIndex = function (array, id) {
                    for (var i = 0; i < array.length; i++) {
                        if (array[i].id == id) {
                            return i;
                        }
                    }

                    return -1;
                }

                scope.getItemName = function (array, id) {
                    if (!array)
                        return "";

                    var index = scope.getItemIndex(array, id);

                    if (index == -1)
                        return "";

                    return array[index].name;
                }

                scope.getItemReference = function (array, id) {
                    var index = scope.getItemIndex(array, id);

                    if (index == -1)
                        return "";

                    return array[index];
                }

                //Имя месяца по номеру
                scope.getMonthName = function (id) {
                    return scope.getItemName(scope.months, id);
                }

                //Имя квартала по номеру
                scope.getQuarterName = function (id) {
                    return scope.getItemName(scope.quarters, id);
                }




                //Работа с метаперечислениями
                scope.metaEnumValues = function (enumId) {
                    var methodName = "enumValuesFor_" + enumId;

                    if (scope[methodName] === undefined) {
                        scope[methodName] = BizMetaEnumService.query(enumId);
                    }

                    return scope[methodName];
                }

                //Работа с экземплярами метаобъектов
                scope.metaObjectInstances = function (definitionId) {
                    var methodName = "metaObjectInstancesOf_" + definitionId;

                    if (scope[methodName] === undefined) {
                        scope[methodName] = BizMetaObjectInstanceService.query(definitionId);
                    }

                    return scope[methodName];
                }







                //Закладки
                scope.tabs = [];
                scope.selectedTabIndex = -1;
                scope.onTabSelected = function (index) { };
                scope.selectTab = function (index) {                    
                    if (scope.tabs.length == 0)
                        return;

                    if (index < 0)
                        return;

                    if (index > scope.tabs.length - 1)
                        return;

                    scope.selectedTabIndex = index;
                    scope.onTabSelected(index);
                }





                //Getting vmds
                scope.getVMDS();



                //Переход по ссылке
                scope.gotoHref = function (href) {
                    if (href.indexOf("http:") == 0) {
                        _transition = true;
                        $window.location.href = href.replace("http:", "");
                    }

                    $location.path(href);

                }


                //Files uploader API
                $AddFile = function (file) {
                    scope.data.Files.AddedFiles.push(file);
                    scope.$apply();
                };

                scope.RemoveFile = function (index) {
                    scope.data.Files.AddedFiles.splice(index, 1);
                };

                scope.DeleteFile = function (index) {
                    scope.data.Files.DeletedFiles.push(scope.data.Files.ExistingFiles[index].id);
                    scope.data.Files.ExistingFiles.splice(index, 1);
                };
            }
            this.initBase = baseInit;

            //инициализация для CRUD
            this.initCRUD = function (scope, service) {
                baseInit(scope);

                //Обработчик сохранения данных
                scope.submitResultHandler = function () {
                    if (scope.requestResultHandler()) {
                        scope.return();
                    }
                }

                //Возврат на место вызова
                scope.return = function () {
                    scope.gotoHref(scope.returnUrl);
                }



                //Валидация на стороне сервера и сохранение данных, если валидация прошла успешно
                scope.validateAndSubmit = function (preValidate) {
                    if (preValidate) {
                        if (!preValidate())
                            return;
                    }

                    service.validate(scope.data).$promise.then(function (result) {
                        if (result.isValid) {
                            scope.submit();
                            return;
                        }

                        alert(result.validationResults);
                    }, scope.resourceErrorHandler)
                }

                //Валидация на стороне клиента и сохранение данных, если валидация прошла успешно
                scope.localValidateAndSubmit = function (validate) {
                    if (!validate())
                        return;
                    
                    scope.submit();
                }



                //Обработчик чтения данных
                scope.readResultHandler = function () {
                    scope._pageIsInitialized = true;
                    return scope.requestResultHandler()
                }



                //Создание нового объекта (шаблона данных)
                scope.create = function (params) {
                    scope.readonly = false;
                    scope.data = service.getDefault(params);
                    scope.data.$promise.then(scope.readResultHandler, scope.resourceErrorHandler);
                }




                //Чтение для редактирования
                scope.read = function (id) {
                    scope.id = id;
                    scope.readonly = false;

                    scope.versions = scope.getVersions();
                    scope.currentVersion = scope.getCurrentVersion();

                    scope.data = service.read(id, false, false);
                    scope.data.$promise.then(scope.readResultHandler, scope.resourceErrorHandler);
                }

                //Чтение для редактирования
                scope.readWithoutVersions = function (id) {
                    scope.id = id;
                    scope.readonly = false;

                    scope.data = service.read(id, false, false);
                    scope.data.$promise.then(scope.readResultHandler, scope.resourceErrorHandler);
                }

                //Чтение для просмотра
                scope.view = function (id, version) {
                    scope.id = id;
                    scope.readonly = true;

                    scope.versions = scope.getVersions();
                    scope.currentVersion = scope.getCurrentVersion();

                    scope.data = service.read(id, false, true, version);
                    scope.data.$promise.then(scope.readResultHandler, scope.resourceErrorHandler);
                }

                //Чтение для просмотра
                scope.viewWithoutVersions = function (id) {
                    scope.id = id;
                    scope.readonly = true;

                    scope.data = service.read(id, false, true);
                    scope.data.$promise.then(scope.readResultHandler, scope.resourceErrorHandler);
                }

                //Повторное чтение данных для редактирования или просмотра
                scope.reRead = function () {
                    if (!scope.id)
                        return;

                    if (scope.readonly) {
                        scope.view(scope.id);
                    } else {
                        scope.read(scope.id);
                    }
                }


                //Обработчик чтения для клонирования
                scope.readAndCloneResultHandler = function () {
                    if (scope.readResultHandler()) {
                        scope.simpleClear(scope.data);
                        scope.data.CanUpdate = true;
                        scope.data.CanDelete = true;
                    }
                }

                //Чтение для клонирования
                scope.clone = function (id) {
                    scope.readonly = false;

                    scope.data = service.read(id, true, false);
                    scope.data.$promise.then(scope.readAndCloneResultHandler, scope.resourceErrorHandler);
                }


                //Сохранение данных (создание нового или обновление данных)
                scope.submit = function () {
                    scope._submitting = true;
                    if (scope.data.id == undefined || scope.data.id == null) {
                        service.create(scope.data).then(scope.submitResultHandler, scope.resourceErrorHandler);
                    }
                    else {
                        service.update(scope.data).then(scope.submitResultHandler, scope.resourceErrorHandler);
                    }
                }

                //Удаление данных
                scope.delete = function () {
                    service.delete(scope.data).then(scope.submitResultHandler, scope.resourceErrorHandler);
                }




                //Простое копирование объектов (только свойства без рекурсии)
                scope.simpleCopy = function (source, target) {
                    for (var attr in source) {
                        if (source.hasOwnProperty(attr) && attr.indexOf("$") == -1) {
                            //Копирование массива
                            if (scope.isArray(source[attr])) {
                                target[attr] = new Array();
                                for (var i = 0; i < source[attr].length; i++) {
                                    var newItem = new Object();
                                    scope.simpleCopy(source[attr][i], newItem)
                                    target[attr].push(newItem);
                                }
                            //Копирование объекта    
                            } else if (scope.isObject(source[attr])) {
                                target[attr] = new Object();
                                scope.simpleCopy(source[attr], target[attr]);
                            //Копирование свойства
                            } else {
                                target[attr] = source[attr];
                            }
                        }
                    }
                }

                //"Простая очистка" объекта (для клонирования) - рекурсивное удаление значений id
                scope.simpleClear = function (source) {
                    //Если id не определен, то выходим без обработки вложенных структур
                    if (source.id == undefined || source.id == null)
                        return;

                    //Очистка id
                    source.id = null;

                    //Очистка вложенных объектов и массивов
                    for (var attr in source) {
                        if (source.hasOwnProperty(attr) && attr.indexOf("$") == -1) {
                            //Очистка массива
                            if (scope.isArray(source[attr])) {
                                var array = source[attr];
                                for (i = 0; i < array.length; i++) {
                                    scope.simpleClear(array[i]);
                                }
                            //Очистка объекта
                            } else if (scope.isObject(source[attr])) {
                                scope.simpleClear(source[attr]);
                            }
                        }
                    }
                }




                //Работа с коллекциями объектов
                scope.addValue = function (context, array, form) {
                    var newValue = {id: "", displayName: ""};
                    scope.simpleCopy(form, newValue);

                    newValue.id = "";
                    array.push(newValue);

                    context.currentValue = null;
                };

                scope.removeValue = function (context, array, index) {
                    if (!confirm('Удалить значение?'))
                        return;

                    array.splice(index, 1);
                    context.currentValue = null;
                };

                scope.beginEditValue = function (context, array, index, form) {
                    context.currentValue = index;
                    scope.simpleCopy(array[index], form);
                };

                scope.submitEditValue = function (context, array, form) {
                    scope.simpleCopy(form, array[context.currentValue]);
                    context.currentValue = null;
                };

                scope.swapValues = function (context, array, index1, index2) {
                    var temp = {};
                    scope.simpleCopy(array[index1], temp);
                    scope.simpleCopy(array[index2], array[index1]);
                    scope.simpleCopy(temp, array[index2]);
                    context.currentValue = null;
                };

                scope.moveUpValue = function (context, array, index) {
                    scope.swapValues(context, array, index, index - 1);
                };

                scope.moveDownValue = function (context, array, index) {
                    scope.swapValues(context, array, index, index + 1);
                };





                ///Работа с коллекциями по имени массива (для метаобъектов)
                scope.getObjectByName = function (name, makeArrayIfNull) {
                    var members = name.split(".");
                    var result = scope.data;
                    for (i = 0; i < members.length; i++) {
                        if (result == undefined || result == null) {
                            return null;
                        }

                        var item = result[members[i]];

                        if (item == null) {
                            if (!makeArrayIfNull) {
                                result[members[i]] = new Object();
                            } else {
                                result[members[i]] = new Array();
                            }

                            item = result[members[i]];
                        }

                        result = item;
                    }

                    return result;
                }

                scope.getListFormItemModel = function (name) {
                    return scope.getObjectByName(name + "_form", false);
                }

                scope.getListFormItemsCntx = function (name) {
                    return scope.getObjectByName(name + "_cntx", false);
                }

                scope.getListFormItems = function (name) {
                    return scope.getObjectByName(name, true);
                }

                scope.addListFormItem = function (name) {
                    scope.addValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), scope.getListFormItemModel(name));
                }

                scope.submitListFormItem = function (name) {
                    scope.submitEditValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), scope.getListFormItemModel(name));
                }

                scope.beginEditListFormItem = function (name, index) {
                    scope.beginEditValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), index, scope.getListFormItemModel(name));
                }

                scope.removeListFormItem = function (name, index) {
                    scope.removeValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), index);
                }

                scope.moveUpListFormItem = function (name, index) {
                    scope.moveUpValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), index);
                }

                scope.moveDownListFormItem = function (name, index) {
                    scope.moveDownValue(scope.getListFormItemsCntx(name), scope.getListFormItems(name), index);
                }
            }

            //Инициализация для списка
            this.initList = function (scope, service) {
                baseInit(scope);

                scope.data = {};

                scope.extraFilter = false;
                scope.extraFilterEnabled = false;
                scope.extraFilterToggle = function() {
                    scope.extraFilter = !scope.extraFilter;
                    return false;
                }

                //Очистка фильтра
                scope.clearFilter = function () {
                    if (scope.data == undefined || scope.data == null)
                        return;

                    scope.data.filter = {};
                    scope.query();
                }

                //Обработчик запроса данных
                scope.queryResultHandler = function () {
                    scope._pageIsInitialized = true;
                    scope._queryIsCompleted = true;

                    if (scope.requestResultHandler()) {
                        scope.data.pagesCount = scope.pagesCount();
                        scope.data.pageSizes = [10, 20, 50, 100];

                        //Повторный запрос, если страница выбрана некорректно
                        if (scope.data.pagesCount > 0 && scope.data.page > scope.data.pagesCount) {
                            scope.selectPage(1);
                            return;
                        }

                        scope.data.pages = [];
                        var min = 1;
                        var max = scope.data.pagesCount;

                        if (max > 10) {
                            if (scope.data.page > 5) {
                                if (scope.data.page < max - 5) {
                                    min = scope.data.page - 4;
                                } else {
                                    min = max - 9;
                                }
                            }

                            max = min + 9;
                        }

                        for (var i = min; i <= max; i++) {
                            scope.data.pages.push({number:i, text:i});
                        }

                        if (scope.data.page > 1)
                            scope.data.pages.splice(0, 0, { number: scope.data.page - 1, text: '«' })
                        if (min > 2)
                            scope.data.pages.splice(0, 0, { number: 1, text: '««' })

                        if (scope.data.page < scope.data.pagesCount)
                            scope.data.pages.push({ number: scope.data.page + 1, text: '»' })
                        if (max < scope.data.pagesCount - 1)
                            scope.data.pages.push({ number: scope.data.pagesCount, text: '»»' })
                    }
                }

                //Обработчик выполнения custom Action
                scope.customActionResultHandler = function () {
                    if (scopeResourceRequestHandler(scope.customActionResult)) {
                        scope.query();
                    }
                }               

                //Запрос данных
                scope.query = function () {
                    if (_transition)
                        return;

                    scope._queryIsCompleted = false;
                    scope.data = service.query(scope.data.filter, scope.data.page, scope.data.pageSize);
                    scope.data.$promise.then(scope.queryResultHandler, scope.resourceErrorHandler);
                }

                //Получение контекста исполнения запроса
                scope.getContext = function (params) {
                    return service.context(params);
                }

                //Выполнение custom action
                scope.customAction = function (params) {
                    scope.customActionResult = service.customAction(params);
                    scope.customActionResult.$promise.then(scope.customActionResultHandler, scope.resourceErrorHandler);
                }



                //Работа со страницами
                scope.pagesCount = function () {
                    if (scope.data.itemsCount == undefined)
                        return 0;

                    if (scope.data.pageSize == undefined)
                        return 0;

                    var res = parseInt(scope.data.itemsCount / scope.data.pageSize);

                    if (scope.data.itemsCount % scope.data.pageSize > 0)
                        res++;

                    return res;
                }

                scope.selectPage = function (page) {
                    scope.data.page = page;
                    scope.query();
                }

                scope.selectPageSize = function (pageSize) {
                    scope.data.page = 1;
                    scope.data.pageSize = pageSize;
                    scope.query();
                }


                //Сортировки
                scope.orderBy = function (column) {
                    scope.data.filter.orderByItem = column;
                    scope.query();
                }

                scope.orderByClass = function (column) {
                    if (scope.data.filter == undefined) {
                        return "";
                    }

                    if (scope.data.filter.orderByExpression.contains(column + ":1;")) {
                        return "nps-asc";
                    } else if (scope.data.filter.orderByExpression.contains(column + ":2;")) {
                        return "nps-desc";
                    }

                    return "";
                }




                //Обновление данных после изменений настроек VMDS
                scope.afterVMDSChanged = function () {
                    scope.query();
                }




                //Работа с SelectSet
                scope.isItemSelected = function (item) {
                    if (scope.data.filter == undefined) {
                        return;
                    }

                    var index = scope.getItemIndex(scope.data.filter.selectSet, item.id);
                    item.$selected = index != -1

                    return item.$selected;
                }

                scope.selectAllItems = function () {
                    var select = scope.data.items.length != scope.data.filter.selectSet.length;
                    if (select) {
                        scope.data.filter.selectSet = scope.data.items.map(function (item) {
                            return { id: item.id, type: item.type }
                        });
                        scope.selectSet = scope.data.items.map(function (item) {
                            return { id: item.id, type: item.type, item: item }
                        });
                    }
                    else {
                        scope.data.filter.selectSet = [];
                        scope.selectSet = [];
                    }
                }

                scope.determineCheckAll = function (checkbox) {
                    var checked;
                    if (!scope.data.filter || scope.data.items.length == 0) {
                        //нет фильтра по выбранным - не надо выделять общую галочку
                        checked = false;
                    }
                    else {
                        //ищем выбранные среди отфильтрованных
                        var selectedIds = scope.data.filter.selectSet.map(function (item) {
                            return item.id;
                        });
                        checked = scope.data.items.every(function (element) {
                            var index = selectedIds.indexOf(element.id);
                            return index != -1;
                        });
                    }
                    
                    //предвыбор общей галочки "Выбрать всё
                    $(checkbox).prop('checked', checked);
                }

                scope.onItemCheckBoxClick = function (item) {
                    if (scope.data.filter == undefined) {
                        return;
                    }

                    if (!scope.selectSet || scope.selectSet.length != scope.data.filter.selectSet.length) {
                        scope.selectSet = scope.data.filter.selectSet.splice();
                    }

                    var index = scope.getItemIndex(scope.data.filter.selectSet, item.id);
                    if (index == -1) {
                        scope.data.filter.selectSet.push({ id: item.id, type: item.type })
                        scope.selectSet.push({ id: item.id, type: item.type, item: item })

                    } else {
                        scope.data.filter.selectSet.splice(index, 1);
                        scope.selectSet.splice(index, 1);
                    }
                    //предвыбор общей галочки "Выбрать всё
                    scope.determineCheckAll($('.main-data input.check-all'));
                }

                scope.onSelectSetClick = function () {
                    if (scope.data.filter == undefined) {
                        return;
                    }

                    scope.data.filter.filterBySelectSet = !scope.data.filter.filterBySelectSet;
                    scope.query();
                }

                scope.clearSelectSet = function () {
                    scope.selectSet = [];

                    if (scope.data && scope.data.filter) {
                        scope.data.filter.selectSet = [];
                        scope.data.filter.filterBySelectSet = false;
                    }
                }
            }
        }

        return CommonController;
    }
})()