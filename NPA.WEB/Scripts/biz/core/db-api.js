﻿'use strict';

(function () {
    angular
        .module('NpsDbApi', ['NpsAppConfig'])
        .service('CRUDServiceApi', ['$resource', '$q', '$http', CRUDServiceApi])
        .service('QueryServiceApi', ['$resource', '$q', '$http', QueryServiceApi])
        .service('EnumServiceApi', ['$resource', '$q', '$http', EnumServiceApi]);
    
    //API CRUD (работа с объектом)
    function CRUDServiceApi($resource, $q, $http) {
        var CRUDService = function (api) {
            this.api = api;
            this.resource = null;

            //Инициализация ресурса по умолчанию
            this.initResourceByDefault = function () {
                this.initResource();
            }

            //Инициализация ресурса
            this.initResource = function (params) {
                params = params || {};
                params.id = '@id';

                this.resource = $resource('{0}/:id'.format(this.api), params, {
                    'update': { method: 'POST' },
                    'create': { method: 'PUT' },
                    'default': { method: 'GET', params: { id: 'default' } }
                });
            }

            //валидация (POST)
            this.validate = function (data) {
                var resource = $resource('{0}/validate'.format(this.api), {}, {
                    'validate': { method: 'POST' },
                });

                return resource.validate({data: JSON.stringify(data) })
            }

            //добавление (PUT)
            this.create = function (data) {
                return data.$create();
            }

            //обновление (POST)
            this.update = function (data) {
                return data.$update();
            }

            //удаление (DELETE)
            this.delete = function (data) {
                return data.$delete();
            }

            //чтение (GET)
            this.read = function (id, clone, readonly, version) {
                if (clone == undefined)
                    clone = false;

                if (readonly == undefined)
                    readonly = false;

                if (this.resource == null) {
                    this.initResourceByDefault();
                }

                return this.resource.get({ id: id, clone: clone, readonly: readonly, version: version });
            }

            //значение по умолчанию (GET)
            this.getDefault = function (params) {
                if (this.resource == null) {
                    this.initResourceByDefault();
                }

                return this.resource.default(params);
            }
        }

        return CRUDService;
    }

    //API Query (работа со списком объектов)
    function QueryServiceApi($resource, $q, $http) {
        var QueryService = function (api) {
            this.api = api;
            this.resource = null;
            this.resourceCntx = null;

            //Инициализация ресурса по умолчанию
            this.initResourceByDefault = function () {
                this.initResource();
            }

            //Инициализация ресурса
            this.initResource = function (params) {
                this.resource = $resource('{0}/:page/:pageSize'.format(this.api), params, {
                    'query': { method: 'GET' }
                });
                this.resourceCntx = $resource('{0}/context'.format(this.api), params, {
                    'context': { method: 'GET' }
                });
            }

            //запрос к базе данных
            this.query = function (filter, page, pageSize) {
                if (this.resource == null) {
                    this.initResourceByDefault();
                }

                return this.resource.query({ page: page, pageSize: pageSize, filter: JSON.stringify(filter) });
            }

            //контекст работы списка (любые дополнительные настройки и ограничения)
            this.context = function (params) {
                if (this.resourceCntx == null) {
                    this.initResourceByDefault();
                }

                return this.resourceCntx.context(params);
            }

            //Выполнение custom action
            this.customAction = function (params) {
                var resourceCustom = $resource('{0}/:action'.format(this.api), params, {
                    'custom': { method: 'POST' }
                });

                return resourceCustom.custom(params);
            }
        }

        return QueryService;
    }

    //API Enum (работа с перечислением)
    function EnumServiceApi($resource, $q, $http) {
        var EnumService = function (api) {
            this.api = api;
            this.resource = null;

            //Инициализация ресурса по умолчанию
            this.initResourceByDefault = function () {
                this.initResource();
            }

            //Инициализация ресурса
            this.initResource = function (params) {
                this.initCustomResource('{0}/:name'.format(this.api), params);
            }

            //Инициализация зависимого от id ресурса
            this.initResourceWithId = function (id, params) {
                params = params || {};
                params.id = id;
                this.initCustomResource('{0}/:name/:id'.format(this.api), params);
            }

            //Инициализация независимого ресурса
            this.initCustomResource = function (url, params) {
                this.resource = $resource(url, params, {
                    'query': { method: 'GET', isArray: true },
                    'getData': { method: 'GET', isArray: false }
            });
            }

            //Получение данных справочника name (array!)
            this.query = function (name, addNull) {
                if (this.resource == null) {
                    this.initResourceByDefault();
                }

                var res = this.resource.query({ name: name });

                if (addNull) {
                    res.$promise.then(function (data) {
                        data.push({ id: null, name: '' });
                    });
                }

                return res;
            }

            //Получение данных объекта name
            this.getData = function (name) {
                if (this.resource == null) {
                    this.initResourceByDefault();
                }

                return this.resource.getData({ name: name });
            }
        }

        return EnumService;
    }
})()