﻿'use strict';

(function () {
    angular
        .module('NpsDirectives', ['NpsAppConfig'])
        .directive('npsPagination', function () {
            return {
                templateUrl: '/Content/biz/templates/pagination.html',
                replace: true,
                restrict: 'E',
                scope: true
            }
        })
        .directive('npsVmdsPanel', function () {
            return {
                templateUrl: '/Content/biz/templates/vmds.html',
                replace: true,
                restrict: 'E',
                scope: true
            }
        })
        .directive('npsVersions', function () {
            return {
                templateUrl: '/Content/biz/templates/versions.html',
                replace: true,
                restrict: 'E',
            }
        })
        .directive('npsSearch', function () {
            return {
                templateUrl: '/Content/biz/templates/search.html',
                replace: true,
                restrict: 'E',
            }
        })
        .directive('npsTabs', function () {
            return {
                templateUrl: '/Content/biz/templates/tabs.html',
                replace: true,
                restrict: 'E',
                scope: true
            }
        });
})()