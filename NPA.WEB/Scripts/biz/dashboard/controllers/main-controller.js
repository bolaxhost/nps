﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('MainCtrl', ['$scope', '$rootScope', '$resource', '$route', '$routeParams', '$location', 'BizControllerHelper', MainController]);

    function MainController($scope, $rootScope, $resource, $route, $routeParams, $location, BizControllerHelper) {
        BizControllerHelper.initBase($scope);

        $rootScope.pageHeader = { selected: 'dashboard' };


        var resource = $resource('/api/biz/dashboard/profile', {}, {
            'getProfile': { method: 'GET' },
        });

        $scope.data = resource.getProfile();
    }
})();