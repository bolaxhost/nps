﻿'use strict';

function GetBranches() {
    var branches = [
        { id: 1, name: "Костромаэнерго" },
        { id: 2, name: "Ярэнерго" },
        { id: 3, name: "Тверьэнерго" },
        { id: 4, name: "Смоленскэнерго" },
        { id: 5, name: "Брянскэнерго" },
        { id: 6, name: "Орелэнерго" },
        { id: 7, name: "Курскэнерго" },
        { id: 8, name: "Липецкэнерго" },
        { id: 9, name: "Белгородэнерго" },
        { id: 10, name: "Тамбовэнерго" },
        { id: 11, name: "Воронежэнерго" },
        { id: 99, name: "Исполнительный аппарат" },
    ];

    return branches;    
};