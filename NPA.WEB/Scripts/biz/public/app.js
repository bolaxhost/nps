﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/main', {
                  templateUrl: '/api/biz/public/main-form',
                  controller: 'MainCtrl'
              }).
              otherwise({
                  redirectTo: '/main'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();