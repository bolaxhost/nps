﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('MainCtrl', ['$scope', '$resource', '$route', '$routeParams', '$location', 'BizControllerHelper', MainController]);

    function MainController($scope, $resource, $route, $routeParams, $location, BizControllerHelper) {
        BizControllerHelper.initBase($scope);
    }
})();