﻿'use strict';

(function () {
    angular
        .module('BizApp', [
            'NpsAppConfig',
            'NpsDbApi',
            'NpsControllerHelpers',
            'NpsDirectives'
    ])
        .config([
            '$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', AppConfig
        ])
        .run([
            '$rootScope', '$window', '$document', '$location', AppRun
        ]);

    //Конфигурация
    function AppConfig($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
        $routeProvider.
              when('/edit', {
                  templateUrl: '/api/admin/settings/edit-form',
                  controller: 'EditCtrl'
              }).
              otherwise({
                  redirectTo: '/edit'
              });
    }

    //Старт
    function AppRun($rootScope, $window, $document, $location) {
    }
})();