﻿'use strict';

(function () {

    angular
        .module('BizApp')
        .controller('EditCtrl', ['$scope', '$rootScope', '$resource', '$route', '$routeParams', '$location', 'BizControllerHelper', EditController]);

    function EditController($scope, $rootScope, $resource, $route, $routeParams, $location, BizControllerHelper) {
        $rootScope.pageHeader = { selected: 'admin' };

        BizControllerHelper.initBase($scope);

        var resource = $resource('/api/admin/settings/edit', {}, {
            'getSettings': { method: 'GET' },
            'setSettings': { method: 'POST' },
        });

        $scope.data = resource.getSettings();
        $scope.submit = function () {
            $scope.data.$setSettings().then($scope.createRequestResultHandler(function () {
                alert("Настройки сохранены успешно.")
                $scope.data = resource.getSettings();
            }), $scope.resourceErrorHandler);
        }
    }
})();