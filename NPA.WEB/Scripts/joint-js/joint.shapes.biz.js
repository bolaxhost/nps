/*! JointJS v0.9.3 - JavaScript diagramming library  2015-02-03 


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
if (typeof exports === 'object') {

    var joint = {
        util: require('../src/core').util,
        shapes: {
            basic: require('./joint.shapes.basic')
        },
        dia: {
            ElementView: require('../src/joint.dia.element').ElementView,
            Link: require('../src/joint.dia.link').Link
        }
    };
    var _ = require('lodash');
}

joint.shapes = {};

joint.shapes.Block = joint.shapes.basic.Generic.extend(_.extend({}, joint.shapes.basic.PortsModelInterface, {

    markup: '<g class="rotatable"><g class="scalable"><rect class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>',
    portMarkup: '<g class="port port<%= id %>"><circle class="port-body"/><text class="port-label"/></g>',

    defaults: joint.util.deepSupplement({

        type: 'biz.Block',
        size: { width: 220, height: 50 },
        
        inPorts: ['in'],
        outPorts: ['out'],

        attrs: {
            '.': { magnet: false },
            '.body': {
                width: 220, height: 50,
                stroke: 'black'
            },
            '.port-body': {
                r: 10,
                magnet: true,
                stroke: '#000000'
            },
            text: {
                'pointer-events': 'none'
            },
            rect: { fill: '#eeeeee', filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } } },
            '.label': { 'ref-x': 0.5, 'ref-y': 20, ref: '.body', 'text-anchor': 'middle', fill: 'black' },
            '.inPorts circle': { fill: '#00CCFF', magnet: 'passive', type: 'input', attr1: 'test-in' },
            '.outPorts circle': { fill: 'yellow', type: 'output', attr1: 'test-out' },
            '.inPorts .port-label': { visibility: 'hidden' },
            '.outPorts .port-label': { visibility: 'hidden' }
        }

    }, joint.shapes.basic.Generic.prototype.defaults),

    getPortAttrs: function(portName, index, total, selector, type) {

        var attrs = {};

        var portClass = 'port' + index;
        var portSelector = selector + '>.' + portClass;
        var portLabelSelector = portSelector + '>.port-label';
        var portBodySelector = portSelector + '>.port-body';

        attrs[portLabelSelector] = { text: portName };
        attrs[portBodySelector] = { port: { id: portName || _.uniqueId(type), type: type } };

        attrs[portSelector] = { ref: '.body', 'ref-x': 0.5, 'ref-y': 0 };
        if (selector === '.outPorts') { attrs[portSelector]['ref-y'] = 50; }

        return attrs;
    }
}));

joint.shapes.Condition = joint.shapes.basic.Generic.extend(_.extend({}, joint.shapes.basic.PortsModelInterface, {

    markup: '<g class="rotatable"><g class="scalable"><polygon class="body"/></g><text class="label"/><g class="inPorts"/><g class="outPorts"/></g>',
    portMarkup: '<g class="port port<%= id %>"><circle class="port-body"/><text class="port-label"/></g>',

    defaults: joint.util.deepSupplement({

        type: 'biz.Condition',
        size: { width: 250, height: 100 },

        inPorts: ['if'],
        outPorts: ['else', 'then'],

        attrs: {
            '.': { magnet: false },
            '.body': {
                fill: '#eeeeee',
                points: '125,0 250,50 125,100 0,50',
                stroke: 'black',
                filter: { name: 'dropShadow', args: { dx: 1, dy: 1, blur: 2 } }
            },
            '.port-body': {
                r: 10,
                magnet: true,
                stroke: '#000000'
            },
            text: {
                'pointer-events': 'none'
            },
            '.label': { 'ref-x': .5, 'ref-y': 45, ref: '.body', 'text-anchor': 'middle', fill: 'black' },
            '.inPorts circle': { fill: '#00CCFF', magnet: 'passive', type: 'input' },
            '.outPorts circle': { type: 'output' },

            '.inPorts .port-label': { visibility: 'hidden' },
            '.outPorts .port-label': { visibility: 'hidden' }
        }

    }, joint.shapes.basic.Generic.prototype.defaults),

    getPortAttrs: function (portName, index, total, selector, type) {

        var attrs = {};

        var portClass = 'port' + index;
        var portSelector = selector + '>.' + portClass;
        var portLabelSelector = portSelector + '>.port-label';
        var portBodySelector = portSelector + '>.port-body';

        attrs[portLabelSelector] = { text: portName };
        attrs[portBodySelector] = { port: { id: portName || _.uniqueId(type), type: type } };

        attrs[portSelector] = { ref: '.body', 'ref-x': index * (1 / total) };
        if (selector === '.inPorts')
        {
            attrs[portSelector] = { ref: '.body', 'ref-x': 125, 'ref-y': 0 };
        }
        else
        {
            var color = (index == 0) ? 'red' : 'green';
            attrs[portSelector] = { ref: '.body', 'ref-x': index * 250, 'ref-y' : 50, 'fill' : color };
        }

        return attrs;
    }
}));

joint.shapes.BlockView = joint.dia.ElementView.extend(joint.shapes.basic.PortsViewInterface);
joint.shapes.ConditionView = joint.dia.ElementView.extend(joint.shapes.basic.PortsViewInterface);

joint.createBizGraph = function () {
    return new joint.dia.Graph;
}

joint.createBizPaper = function (id, graph, width, height) {
    return new joint.dia.Paper({
        el: $('#' + id),
        width: width,
        height: height,
        gridSize: 1,
        model: graph,
        defaultLink: new joint.dia.Link({
            smooth: true,
            attrs: { '.marker-target': { d: 'M 10 0 L 0 5 L 10 10 z' } }
        }),
        validateConnection: function (cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
            // Prevent linking from input ports.
            //if (magnetS && magnetS.getAttribute('type') === 'input') {
            //    return false;
            //}

            // Prevent linking from output ports to input ports within one element.
            if (cellViewS === cellViewT) {
                return false;
            }

            // Prevent linking to input ports.
            return magnetT && magnetT.getAttribute('type') === 'input';
        },
        validateMagnet: function (cellView, magnet) {
            // Note that this is the default behaviour. Just showing it here for reference.
            // Disable linking interaction for magnets marked as passive (see below `.inPorts circle`).
            return magnet.getAttribute('magnet') !== 'passive';
        }
    });
}

joint.createBizBlock = function(x, y, text) {
    return new joint.shapes.Block({
        position: { x: x, y: y },
        attrs: {
            '.label': { text: text },
        }
    });
}

joint.createBizCondition = function (x, y, text) {
    return new joint.shapes.Condition({
        position: { x: x, y: y },
        attrs: {
            '.label': { text: text },
        }
    });
}



if (typeof exports === 'object') {
    module.exports = joint.shapes;
}
