﻿jQuery(function ($) {
    $.timepicker.regional['ru'] = {
        timeOnlyTitle: 'Выберите время',
        timeText: 'Время',
        hourText: 'Часы',
        minuteText: 'Минуты',
        secondText: 'Секунды',
        millisecText: 'Миллисекунды',
        timezoneText: 'Часовой пояс',
        currentText: 'Сейчас',
        closeText: 'Закрыть',
        timeFormat: 'HH:mm:ss',
        stepHour: 1,
        stepMinute: 1,
        stepSecond: 10

        //timeFormat: 'HH:mm',
        //amNames: ['AM', 'A'],
        //pmNames: ['PM', 'P'],
        //isRTL: false
    };
    $.timepicker.setDefaults($.timepicker.regional['ru']);
});