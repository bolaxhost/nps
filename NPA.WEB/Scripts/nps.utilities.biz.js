﻿//Обработчики на нажатие клавиш на докумете
$(document).keypress(function (e) {
    cancelKeyPress(e, false);
});

$(document).keydown(function (e) {
    cancelKeyPress(e, true);
});

function autojumpWithLength(input, length) {
    if (input.value.length >= length) {
        $(input).nextAll(".form-control:first").focus();
    }
}

//Замена точки на запятую
function replaceDotWithComma(input) {
    if (input.value.indexOf(".") >= 0) {
        var value = input.value.replace(".", ",");
        $(input).attr("value", value);
    }
    //Удаление нуля впереди если в числе небыло точки или запятой
    var re = new RegExp("^0[^\.].*");
    if (re.test(input.value)) {
        input.value = input.value.replace(/0/gi, "");
    }
}

function cancelKeyPress(e, activateNextInput) {
    //Отмена обработчика нажатия Enter по умолчанию (submit) на переход к следующему input
    if (e.which == 13 && e.target.tagName != "TEXTAREA") {
        e.preventDefault();

        if (activateNextInput) {
            $.each($('input'), function (index, input) {
                if (input == e.target) {
                    try {
                        $('input')[index + 1].focus();
                    } catch (ex) {
                    }
                }
            });
        }
    }

    //Отмена нажатия Backspase
    if (e.which == 8 && (e.target.tagName == "BODY" || e.target.tagName == "DIV" || $(e.target).hasClass("hasDatepicker"))) {
        e.preventDefault();
    }
}

//Настройки Ajax
$(window).load(function () {
    $.ajaxSetup({
        cache: false,
        beforeSend: asyncShowSpinner,
        complete: asyncHideSpinner,
        error: function () { asyncHideSpinner(); }
    });

});

function asyncShowSpinner() {
    var spinerDiv = $("#loading");
    if (spinerDiv)
        spinerDiv.show();
}

function asyncHideSpinner() {
    var spinerDiv = $("#loading");
    if (spinerDiv)
        spinerDiv.hide();
}
