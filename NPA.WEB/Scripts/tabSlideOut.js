(function ($) {
    $.fn.tabSlideOut = function (callerSettings) {
        var settings = $.extend({
            tabHandle: '.supportPanelHandle',
            speed: 300,
        }, callerSettings || {});

        settings.tabHandle = $(settings.tabHandle);
        var obj = this;

        var properties = {
            containerWidth: parseInt(obj.outerWidth(), 10) + 'px',
            containerHeight: parseInt(obj.outerHeight(), 10) + 'px',
            tabWidth: parseInt(settings.tabHandle.outerWidth(), 10) + 'px',
            tabHeight: parseInt(settings.tabHandle.outerHeight(), 10) + 'px'
        };

        settings.tabHandle.click(function (event) {
            event.preventDefault();
        });

        var slideIn = function () {
            obj.animate({ right: '-' + properties.containerWidth }, settings.speed).removeClass('open');
        };

        var slideOut = function () {
            obj.animate({ right: '-3px' }, settings.speed).addClass('open');
        };

        var clickScreenToClose = function () {
            obj.click(function (event) {
                event.stopPropagation();
            });

            $(document).click(function () {
                slideIn();
            });
        };

        settings.tabHandle.click(function (event) {
            if (obj.hasClass('open')) {
                slideIn();
            } else {
                slideOut();
            }
        });

        clickScreenToClose();
    };
})(jQuery);
