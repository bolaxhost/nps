﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;

using NPS.DAL; 

namespace NPS.WEB
{
    public static class SecurityHelper
    {

        public static string GetCurrentUser()
        {
            return GetCurrentUser(GetHttpRequestBase());
        }

        public static UserAccount GetCurrentUserAccount()
        {
            if (!isAuthorized)
                return null;

            return GetCurrentUserAccount(new BizDocument());
        }

        public static UserAccount GetCurrentUserAccount(BizDocument uow)
        {
            return GetUserAccount(uow, GetCurrentUser());
        }

        public static UserAccount GetUserAccount(string user)
        {
            if (string.IsNullOrWhiteSpace(user))
                return null;

            return GetUserAccount(new BizDocument(), user);
        }

        public static UserAccount GetUserAccount(BizDocument uow, string user)
        {
            if (string.IsNullOrWhiteSpace(user))
                return null;

            return uow.GetActualData<UserAccount>().FirstOrDefault(u => u.login == user);
        }

        public static bool isAuthorized
        {
            get
            {
                return !string.IsNullOrWhiteSpace(SecurityHelper.GetCurrentUser());
            }
        }


        public static HttpRequestBase GetHttpRequestBase()
        {
            return new HttpRequestWrapper(System.Web.HttpContext.Current.Request);
        }

        public static string GetCurrentUser(HttpRequestBase request)
        {
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie == null)
                return null;

            //Forms authentication
            return FormsAuthentication.Decrypt(cookie.Value).Name;
        }


        public static string GetWindowsUserName()
        {
            HttpApplication application = System.Web.HttpContext.Current.ApplicationInstance;
            if (application.User == null || application.User.Identity == null)
                return string.Empty; 

            string IdentityName = application.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(IdentityName))
                return string.Empty;

            IdentityName = IdentityName.Replace("\\", "\\\\");
            return IdentityName;
        }

        public static string GetUserByIntranetAccount()
        {
            var windowsUser = GetWindowsUserName();
            if (string.IsNullOrWhiteSpace(windowsUser))
                return string.Empty;

            var uow = new BizDocument();
            var account = uow.GetActualData<UserAccount>().FirstOrDefault(u => u.login == windowsUser);

            if (account == null)
                return string.Empty;

            return account.login;
        }




        public static bool isExpressionPermitted(string expression)
        {
            return isExpressionPermitted(GetCurrentUserAccount(), expression);
        }

        public static bool isExpressionPermitted(string user, string expression)
        {
            return isExpressionPermitted(GetUserAccount(user), expression);
        }
        public static bool isExpressionPermitted(UserAccount user, string expression)
        {
            if (user == null)
                return false;

            if (string.IsNullOrWhiteSpace(expression))
                return true;

            foreach (var i in expression.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries))
            {
                var andResult = true;
                foreach (var j in i.Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries))
                    if (!isOperationPermitted(user, j))
                    {
                        andResult = false;
                        break;
                    }

                if (andResult)
                    return true;
            }

            return false;
        }
        public static bool isOperationPermitted(string operation)
        {
            return isOperationPermitted(GetCurrentUserAccount(), operation);
        }

        public static bool isOperationPermitted(string user, string operation)
        {
            return isOperationPermitted(GetUserAccount(user), operation);
        }
        
        public static bool isOperationPermitted(UserAccount user, string operation)
        {
            if (user == null)
                return false;

            operation = operation.Trim(); 

            if (string.IsNullOrWhiteSpace(operation))
                return true;

            return user.Roles.SelectMany(r => r.Operations).Any(o => o.code == operation); 
        }
    }
}
