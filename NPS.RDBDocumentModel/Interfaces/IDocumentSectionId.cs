﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.RDBDocumentModel
{
    public interface IDocumentSectionId
    {
        Guid? id { get; }

        DateTime? createdDate { get; }
    }
}
