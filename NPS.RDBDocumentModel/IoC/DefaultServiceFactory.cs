﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using NPS.RDBDocumentService;   

namespace NPS.RDBDocumentModel.IoC
{
    public class DefaultServiceFactory : IServiceFactory 
    {
        private Ninject.StandardKernel _kernel = new Ninject.StandardKernel();

        public DefaultServiceFactory()
        {
            DoDefaultBindings();
        }

        public T Create<T>()
        {
            return _kernel.Get<T>();   
        }
        public object Create(Type type)
        {
            return _kernel.Get(type);
        }
        
        public void Bind<TType, TInterface>()
            where TType : class, TInterface
        {
            _kernel.Bind<TInterface>().To<TType>();
        }

        public void Unbind<TInterface>()
        {
            _kernel.Unbind<TInterface>();
        }




        protected virtual void DoDefaultBindings()
        {
            this.Bind<BaseDocument, IDocumentDataScope>();   
        }
    }
}
