﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.RDBDocumentModel.IoC
{
    public interface IServiceFactory
    {
        T Create<T>();
        object Create(Type type);

        void Bind<TType, TInterface>()
            where TType : class, TInterface;

        void Unbind<TInterface>();
    }
}
