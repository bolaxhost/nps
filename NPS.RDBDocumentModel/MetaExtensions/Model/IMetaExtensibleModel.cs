﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace NPS.RDBDocumentModel
{
    public interface IMetaExtensibleModel
    {
        DbSet<MetaEnum> MetaEnum { get; set; }
        DbSet<MetaEnumValue> MetaEnumValue { get; set; }

        DbSet<MetaObject> MetaObject { get; set; }
        DbSet<MetaObjectProperty> MetaObjectProperty { get; set; }

        DbSet<MetaObjectInstance> MetaObjectInstance { get; set; }
        DbSet<MetaObjectInstanceProperty_bit> MetaObjectInstanceProperty_bit { get; set; }
        DbSet<MetaObjectInstanceProperty_datetime> MetaObjectInstanceProperty_datetime { get; set; }
        DbSet<MetaObjectInstanceProperty_enum> MetaObjectInstanceProperty_enum { get; set; }
        DbSet<MetaObjectInstanceProperty_float> MetaObjectInstanceProperty_float { get; set; }
        DbSet<MetaObjectInstanceProperty_int> MetaObjectInstanceProperty_int { get; set; }
        DbSet<MetaObjectInstanceProperty_object> MetaObjectInstanceProperty_object { get; set; }
        DbSet<MetaObjectInstanceProperty_string> MetaObjectInstanceProperty_string { get; set; }
        DbSet<MetaObjectInstanceProperty_text> MetaObjectInstanceProperty_text { get; set; }
    }
}
