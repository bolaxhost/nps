//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NPS.RDBDocumentModel
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("moip_datetime", Schema = "biz")]

    public partial class MetaObjectInstanceProperty_datetime : MetaObjectInstanceProperty_base
    {
        public System.DateTime value { get; set; }
    }
}
