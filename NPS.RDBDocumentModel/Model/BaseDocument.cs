﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentModel;
using NPS.RDBDocumentModel.IoC;
using NPS.RDBDocumentService; 

namespace NPS.RDBDocumentModel
{
    public partial class BaseDocument : Document 
    {
        /// <summary>
        /// Метод создания фабрики сервисов
        /// </summary>
        protected static Func<IServiceFactory> _createServiceFactory = () => { return new DefaultServiceFactory(); };
        public static Func<IServiceFactory> CreateServiceFactory
        {
            get
            {
                return _createServiceFactory;
            }
        }



        /// <summary>
        /// Фабрика сервисов
        /// </summary>
        private IServiceFactory _serviceFactory = null;

        public IServiceFactory ServiceFactory
        {
            get
            {
                return _serviceFactory ?? (_serviceFactory = CreateServiceFactory()); 
            }

            set
            {
                _serviceFactory = value;
            }
        }







        /// <summary>
        /// Создание хранилища документов
        /// </summary>
        /// <returns></returns>
        protected override IDocumentStorage CreateDocumentStorage()
        {
            return ServiceFactory.Create<IDocumentStorage>();
        }





        private string _identity;
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public string Identity
        {
            get { return _identity; }
            set
            {
                _identity = value;
                HandleIdentityChanges(); 
            }
        }

        protected virtual void HandleIdentityChanges()
        {

        }

        protected bool HasIdentity()
        {
            return !string.IsNullOrWhiteSpace(this.Identity);
        }






        #region Сontext environment
        private T SynchronizeContext<T>(T section)
        {
            var res = section as BaseSection;
            if (res != null)
                res.SynchronizeContext(this);

            return section;
        }

        private ICollection<T> SynchronizeContext<T>(ICollection<T> sections)
        {
            foreach (var section in sections)
                SynchronizeContext(section);

            return sections;
        }

        protected override int StackFrameIndex
        {
            get
            {
                return base.StackFrameIndex + 1;
            }
        }

        protected override T GetSection<T>(bool createInstanceIfNecessary = false)
        {
            return SynchronizeContext(base.GetSection<T>(createInstanceIfNecessary));
        }
        protected override ICollection<T> GetSections<T>()
        {
            return SynchronizeContext(base.GetSections<T>());
        }

        protected override void SetSection<T>(T value)
        {
            base.SetSection<T>(SynchronizeContext(value));
        }

        #endregion Сontext environment
        





        #region Validation

        /// <summary>
        /// Валидация операции Save
        /// </summary>
        protected override void ValidateSave()
        {
            base.ValidateSave();
            this.ValidateUpdate();
        }

        /// <summary>
        /// Валидация операции Delete
        /// </summary>
        protected override void ValidateDelete()
        {
            base.ValidateDelete();
            this.ValidateUpdate();
        }

        /// <summary>
        /// Общая валидация для операций Save и Delete
        /// </summary>
        protected virtual void ValidateUpdate()
        {
            ValidateIdentity();
        }

        protected virtual void ValidateIdentity()
        {
            if (!HasIdentity())
                throw new DocumentException("Документ не может быть сохранен: Indentity не определен!");
        }

        #endregion






        public override Document Clone(bool excludeSystem = true)
        {
            BaseDocument clone = (BaseDocument)base.Clone(excludeSystem);
            clone.Identity = this.Identity;
            clone.MyContext = this.MyContext;

            return clone;
        }

    }
}
