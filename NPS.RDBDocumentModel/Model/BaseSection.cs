﻿using System;
using System.Collections.Generic;
using System.ComponentModel; 
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection; 
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NPS.RDBDocumentModel;
using NPS.RDBDocumentModel.IoC;
using NPS.RDBDocumentService;

namespace NPS.RDBDocumentModel
{

    /// <summary>
    /// Общая функциональность объектов RDBModel
    /// </summary>
    public abstract class BaseSection
    {
        /// <summary>
        /// Фабрика сервисов
        /// </summary>
        private IServiceFactory _serviceFactory = null;

        public IServiceFactory ServiceFactory
        {
            get
            {
                return _serviceFactory ?? (_serviceFactory = BaseDocument.CreateServiceFactory());
            }

            set
            {
                _serviceFactory = value;
            }
        }






        //Данные RDBModel
        private IDocumentDataScope _scope = null;

        [NotMapped]
        public IDocumentDataScope Scope
        {
            get
            {
                return _scope ?? (_scope = this.CreateDefaultDataScope());
            }

            set
            {
                _scope = value;
            }
        }

        protected IDocumentDataScope CreateDefaultDataScope()
        {
            return ServiceFactory.Create<IDocumentDataScope>(); 
        }







        public void SynchronizeContext(BaseDocument document)
        {
            if (this.Scope.HasContext && this.Scope.MyContext == document.MyContext)
                return;

            this.Scope.MyContext = document.MyContext;
        }











        //Wrappers для реализации связей 1-n
        protected T GetAssociatedObject<T>(ICollection<T> collection)
            where T : class, IVMDSAttributes, IDocumentAttributesNonVMDS
        {
            return this.Scope.GetActualData(collection.AsQueryable()).FirstOrDefault();
        }

        protected T GetAssociatedSection<T>(ICollection<T> collection)
            where T : class, IVMDSAttributes, IDocumentSectionAttributesNonVMDS
        {
            return GetAssociatedObject(collection);
        }

        protected Guid? GetAssociatedObjectId<T>(ICollection<T> collection)
            where T : class, IVMDSAttributes, IDocumentAttributesNonVMDS
        {
            var ao = this.GetAssociatedObject(collection);
            if (ao == null)
                return null;

            return ao.instance_id;
        }
        protected Guid? GetAssociatedSectionId<T>(ICollection<T> collection)
            where T : class, IVMDSAttributes, IDocumentSectionAttributesNonVMDS
        {
            var ao = this.GetAssociatedObject(collection);
            if (ao == null)
                return null;

            return ao.instance_section_id;
        }

        protected void SetAssociatedObject<T>(ICollection<T> collection, Guid? id)
            where T : class, IVMDSAttributes, IDocumentAttributesNonVMDS
        {
            if (id != null && id == GetAssociatedObjectId(collection))
                return;

            foreach (var i in this.Scope.GetActualData(collection.AsQueryable()).ToArray())
                collection.Remove(i);

            if (id == null)
                return;

            collection.Add(this.Scope.GetActualData<T>().First(i => i.instance_id == id));
        }

        protected void SetAssociatedSection<T>(ICollection<T> collection, Guid? id)
            where T : class, IVMDSAttributes, IDocumentSectionAttributesNonVMDS
        {
            if (id != null && id == GetAssociatedSectionId(collection))
                return;

            foreach (var i in this.Scope.GetActualData(collection.AsQueryable()).ToArray())
                collection.Remove(i);

            if (id == null)
                return;

            collection.Add(this.Scope.GetActualData<T>().First(i => i.instance_section_id == id));
        }

        protected void SetAssociatedItem<T>(ICollection<T> collection, T item)
            where T : class, IVMDSAttributes, IDocumentAttributesNonVMDS
        {
            foreach (var i in this.Scope.GetActualData(collection.AsQueryable()).ToArray())
                collection.Remove(i);

            if (item == null)
                return;

            collection.Add(item);
        }









        /// <summary>
        /// Простое копирование свойств
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void SimpleCopy(object source, object target, bool skipNotMapped = false)
        {
            Type _sourceType = source.GetType();
            Type _targetType = target.GetType();

            foreach (var sp in _sourceType.GetProperties().Where(i => i.CanRead))
            {
                var tp = _targetType.GetProperty(sp.Name);
                if (tp != null && tp.CanWrite && (!skipNotMapped || tp.GetCustomAttribute<System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute>() == null))
                    tp.SetValue(target, sp.GetValue(source));
            }
        }

        public static bool DoesSnapshotContainData(IDocumentAttributes data, DateTime? snapshot)
        {
            if (snapshot == null)
                return data.actual_till == null;

            return data.actual_from < snapshot && (data.actual_till == null || data.actual_till >= snapshot);
        }
    }
}
