﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Определение атрибута параметризации документа (Document Custom Attribute).
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class,
                   AllowMultiple = false,
                   Inherited = true)]
    public class DocumentDefinitionAttribute : System.Attribute
    {
        /// <summary>
        /// Определяет значение типа документа, сохраняемое в поле <c>instance_type</c>.
        /// </summary>
        public int InstanceType { get; set; }

        /// <summary>
        /// Описание документа.
        /// </summary>
        public string Description { get; set; }
    }

}
