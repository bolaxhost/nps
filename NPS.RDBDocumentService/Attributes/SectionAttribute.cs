﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace NPS.RDBDocumentService
{
 
    /// <summary>
    /// Определение атрибута параметризации раздела документа (Section Custom Attribute).
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property,
                   AllowMultiple = false,
                   Inherited = true)]
    public class DocumentSectionDefinitionAttribute : System.Attribute
    {
        /// <summary>
        /// Определяет значение типа раздела документа, сохраняемое в поле <c>instance_section</c>.
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// Определяет, является ли раздел обязательным (создается автоматически в момент создания документа).
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Определяет, является ли раздел системным. Системные разделы не копируются при клонировании по умолчанию.
        /// </summary>
        public bool IsSystem { get; set; }

        /// <summary>
        /// Описание раздела документа.
        /// </summary>
        public string Description { get; set; }
    }

}
