﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Определение документа - вспомогательный класс, используемый в контексте документа для получения его определения в runtime.
    /// </summary>
    public class DocumentDefinition
    {
        /// <summary>
        /// Конструктор определения.
        /// </summary>
        /// <param name="type">Тип документа (класс, в котором определена структура документа).</param>
        internal DocumentDefinition(Type type)
        {
            if ((_type = type) == null)
                throw new ArgumentNullException("Тип документа (type) не определен!");

            if ((_definition = _type.GetCustomAttribute<DocumentDefinitionAttribute>()) == null)
                throw new ArgumentNullException(string.Format("Атрибут DocumentDefinitionAttribute для класса {0} не определен!", _type.FullName));
        }




        private Type _type = null;
        /// <summary>
        /// Тип документа (класс).
        /// </summary>
        public Type DocumentType
        {
            get
            {
                return _type;
            }
        }






        private DocumentDefinitionAttribute _definition = null;
        /// <summary>
        /// Экземпляр класса - атрибута параметризации (Custom Attribute) документа, на основе которого сформировано данное определение.
        /// </summary>
        public DocumentDefinitionAttribute Definition
        {
            get
            {
                return _definition;
            }
        }





        private Dictionary<string, DocumentSectionDefinition> _sectionDefinitions = null;
        /// <summary>
        /// Определения разделов документа.
        /// </summary>
        public IDictionary<string, DocumentSectionDefinition> SectionDefinitions
        {
            get
            {
                return _sectionDefinitions ?? (_sectionDefinitions = GetSectionDefinitions());
            }
        }

        //Формирование списка определений разделов
        private Dictionary<string, DocumentSectionDefinition> GetSectionDefinitions()
        {
            Dictionary<string, DocumentSectionDefinition> result = new Dictionary<string, DocumentSectionDefinition>();
            DocumentSectionDefinitionAttribute pd = null;

            foreach (var p in this.DocumentType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                if ((pd = p.GetCustomAttribute<DocumentSectionDefinitionAttribute>()) != null)
                    result.Add(p.GetMethod.Name, new DocumentSectionDefinition(p.PropertyType, p.GetMethod.Name, pd));

            return result;
        }





        /// <summary>
        /// Проверка целостности определения документа.
        /// </summary>
        public void Validate()
        {
        }
    }


}
