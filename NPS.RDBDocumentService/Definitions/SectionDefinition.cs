﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace NPS.RDBDocumentService
{
 
    /// <summary>
    /// Определение раздела документа - вспомогательный класс, используемый в контексте документа для получения определения его раздела в runtime.
    /// </summary>
    public class DocumentSectionDefinition
    {
        /// <summary>
        /// Конструктор определения раздела.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="definition"></param>
        internal DocumentSectionDefinition(Type type, string name, DocumentSectionDefinitionAttribute definition)
        {
            if (type == null)
                throw new ArgumentNullException("Тип раздела документа (type) не определен!");

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("Имя раздела (name) не определено!");

            if (definition == null)
                throw new ArgumentNullException("Описание раздела (definition) не определено!");

            if (!type.IsGenericType)
            {
                _type = type;
                _isCollection = false;
            }
            else
            {
                //Определение коллекции
                _type = type.GenericTypeArguments[0];
                _isCollection = true;
            }

            _name = name;
            _definition = definition;
        }






        //Методы для внутреннего использования: создание определений разделов по умолчанию
        internal static DocumentSectionDefinition GetDefaltSectionDefinition(object sectionValue)
        {
            return GetDefaltSectionDefinition(sectionValue.GetType());
        }

        internal static DocumentSectionDefinition GetDefaltSectionDefinition(Type sectionType)
        {
            DocumentSectionDefinition section = new DocumentSectionDefinition(sectionType, "default", new DocumentSectionDefinitionAttribute());
            section.Validate();

            return section;
        }




        /// <summary>
        /// Определяет, является ли переданное свойство класса - раздела документа системным.
        /// </summary>
        /// <param name="property">Определение свойства класса.</param>
        /// <returns><c>true</c> - системное свойство, <c>false</c> - прикладное свойство.</returns>
        public bool IsSystemProperty(PropertyInfo property)
        {
            if (this.HasActualFrom && this.ActualFromProperty.Name == property.Name)
                return true;

            if (this.HasActualTill && this.ActualTillProperty.Name == property.Name)
                return true;

            if (this.HasTypeDefinition && this.InstanceTypeProperty.Name == property.Name)
                return true;

            if (this.HasIdDefinition && this.InstanceIdProperty.Name == property.Name)
                return true;

            if (this.HasSectionDefinition && this.InstanceSectionProperty.Name == property.Name)
                return true;

            if (this.HasSectionIdDefinition && this.InstanceSectionIdProperty.Name == property.Name)
                return true;

            if (this.HasSectionIndexDefinition && this.InstanceSectionIndexProperty.Name == property.Name)
                return true;

            return false;
        }








        /// <summary>
        /// Извлечение значения типа документа из экземпляра раздела документа.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Тип документа.</returns>
        public static int GetInstanceTypeOf(object sectionValue)
        {
            return (int)GetDefaltSectionDefinition(sectionValue)
                .InstanceTypeProperty
                .GetValue(sectionValue);
        }

        /// <summary>
        /// Извлечение идентификатора документа из экземпляра раздела документа.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Уникальный идентификатор документа.</returns>
        public static Guid GetInstanceIdOf(object sectionValue)
        {
            return Guid.Parse(GetDefaltSectionDefinition(sectionValue)
                .InstanceIdProperty
                .GetValue(sectionValue)
                .ToString());
        }

        /// <summary>
        /// Извлечение значения типа раздела документа из экземпляра.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Тип раздела документа.</returns>
        public static int GetInstanceSectionOf(object sectionValue)
        {
            DocumentSectionDefinition section = GetDefaltSectionDefinition(sectionValue);

            if (!section.HasSectionDefinition)
                return -1;

            return (int)section
                .InstanceSectionProperty
                .GetValue(sectionValue);
        }

        /// <summary>
        /// Извлечение идентификатора раздела документа из экземпляра.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Уникальный идентификатор раздела документа.</returns>
        public static Guid? GetInstanceSectionIdOf(object sectionValue)
        {
            DocumentSectionDefinition section = GetDefaltSectionDefinition(sectionValue);

            if (!section.HasSectionIndexDefinition)
                return null;

            return Guid.Parse(section
                .InstanceSectionIndexProperty
                .GetValue(sectionValue)
                .ToString());
        }

        /// <summary>
        /// Извлечение индекса элемента коллекции (повторяющегося раздела) документа из элемента.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Индекс от <c>0</c> до <c>Count() - 1</c>.</returns>
        public static int GetInstanceSectionIndexOf(object sectionValue)
        {
            DocumentSectionDefinition section = GetDefaltSectionDefinition(sectionValue);

            if (!section.HasSectionIndexDefinition)
                return -1;

            return (int)section
                .InstanceSectionIndexProperty
                .GetValue(sectionValue);
        }








        /// <summary>
        /// Является ли определение раздела определением коллекции.
        /// </summary>
        public bool IsCollection
        {
            get { return _isCollection; }
        }

        private bool _isCollection = false;




        /// <summary>
        /// Тип данных раздела документа (класс).
        /// </summary>
        public Type SectionDataType
        {
            get
            {
                return _type;
            }
        }

        private Type _type = null;



        /// <summary>
        /// Тип раздела документа
        /// </summary>
        public int SectionType
        {
            get
            {
                return _definition.SectionType;  
            }
        }



        /// <summary>
        /// Имя раздела.
        /// </summary>
        public string SectionName
        {
            get
            {
                return _name;
            }
        }

        private string _name;




        /// <summary>
        /// Экземпляр класса - атрибута параметризации (Custom Attribute) раздела документа.
        /// </summary>
        public DocumentSectionDefinitionAttribute Definition
        {
            get
            {
                return _definition;
            }
        }

        private DocumentSectionDefinitionAttribute _definition = null;




        /// <summary>
        /// Определение поля 'Актуален с'.
        /// </summary>
        public PropertyInfo ActualFromProperty
        {
            get { return _actualFromProperty ?? (_actualFromProperty = _type.GetProperty("actual_from")); }
        }

        PropertyInfo _actualFromProperty = null;

        
        
        
        
        
        /// <summary>
        /// Определение поля 'Актуален по'.
        /// </summary>
        public PropertyInfo ActualTillProperty
        {
            get { return _actualTillProperty ?? (_actualTillProperty = _type.GetProperty("actual_till")); }
        }

        PropertyInfo _actualTillProperty = null;






        /// <summary>
        /// Определение поля 'Уникальный идентификатор документа'.
        /// </summary>
        public PropertyInfo InstanceIdProperty
        {
            get { return _instanceIdProperty ?? (_instanceIdProperty = _type.GetProperty("instance_id")); }
        }

        PropertyInfo _instanceIdProperty = null;






        /// <summary>
        /// Определение поля 'Тип документа'.
        /// </summary>
        public PropertyInfo InstanceTypeProperty
        {
            get { return _instanceTypeProperty ?? (_instanceTypeProperty = _type.GetProperty("instance_type")); }
        }

        PropertyInfo _instanceTypeProperty = null;






        /// <summary>
        /// Определение поля 'Тип раздела документа'.
        /// </summary>
        public PropertyInfo InstanceSectionProperty
        {
            get { return _instanceSectionProperty ?? (_instanceSectionProperty = _type.GetProperty("instance_section")); }
        }

        PropertyInfo _instanceSectionProperty = null;






        /// <summary>
        /// Определение поля 'Уникальный идентификатор экземпляра раздела документа'.
        /// </summary>
        public PropertyInfo InstanceSectionIdProperty
        {
            get { return _instanceSectionIdProperty ?? (_instanceSectionIdProperty = _type.GetProperty("instance_section_id")); }
        }

        PropertyInfo _instanceSectionIdProperty = null;






        /// <summary>
        /// Определение поля 'Индекс элемента коллекции документа'.
        /// </summary>
        public PropertyInfo InstanceSectionIndexProperty
        {
            get { return _instanceSectionIndexProperty ?? (_instanceSectionIndexProperty = _type.GetProperty("instance_section_index")); }
        }

        PropertyInfo _instanceSectionIndexProperty = null;





        /// <summary>
        /// Имеется ли определение системного поля 'Актуален с'.
        /// </summary>
        public bool HasActualFrom
        {
            get
            {
                if (_hasActualFrom == null)
                    _hasActualFrom = ActualFromProperty != null;

                return _hasActualFrom.Value;
            }
        }

        bool? _hasActualFrom = null;





        /// <summary>
        /// Имеется ли определение системного поля 'Актуален по'.
        /// </summary>
        public bool HasActualTill
        {
            get
            {
                if (_hasActualTill == null)
                    _hasActualTill = ActualTillProperty != null;

                return _hasActualTill.Value;
            }
        }

        bool? _hasActualTill = null;






        /// <summary>
        /// Имеется ли определение системного поля 'Уникальный идентификатор документа'.
        /// </summary>
        public bool HasIdDefinition
        {
            get
            {
                if (_hasId == null)
                    _hasId = InstanceIdProperty != null;

                return _hasId.Value;
            }
        }

        bool? _hasId = null;








        /// <summary>
        /// Имеется ли определение системного поля 'Тип раздела документа'.
        /// </summary>
        public bool HasTypeDefinition
        {
            get
            {
                if (_hasType == null)
                    _hasType = InstanceTypeProperty != null;

                return _hasType.Value;
            }
        }

        bool? _hasType = null;



        







        /// <summary>
        /// Имеется ли определение системного поля 'Тип раздела документа'.
        /// </summary>
        public bool HasSectionDefinition
        {
            get
            {
                if (_hasSection == null)
                    _hasSection = InstanceSectionProperty != null;

                return _hasSection.Value;
            }
        }
        
        bool? _hasSection = null;






        /// <summary>
        /// Имеется ли определение системного поля 'Уникальный идентификатор экземпляра раздела документа'.
        /// </summary>
        public bool HasSectionIdDefinition
        {
            get
            {
                if (_hasSectionId == null)
                    _hasSectionId = InstanceSectionIdProperty != null;

                return _hasSectionId.Value;
            }
        }

        bool? _hasSectionId = null;










        /// <summary>
        /// Имеется ли определение системного поля 'Индекс элемента коллекции документа'.
        /// </summary>
        public bool HasSectionIndexDefinition
        {
            get
            {
                if (_hasSectionIndex == null)
                    _hasSectionIndex = InstanceSectionIndexProperty != null;

                return _hasSectionIndex.Value;
            }
        }

        bool? _hasSectionIndex = null;




        /*
         Internal API работы с базой данных на основе метаданных раздела
         */

        #region GetAllValues Specifications
        //Фильтры на основе полей базовых интерфейсов сущностей базы данных
        internal IQueryable<T> GetAllValues_IDocumentAttributesNonVMDS<T>(IQueryable<T> data, int? instanceType = null, Guid? instanceId = null)
            where T : class, IDocumentAttributesNonVMDS
        {
            if (instanceType != null && instanceType > 0)
                data = data.Where(i => i.instance_type == instanceType);

            if (instanceId != null)
                data = data.Where(i => i.instance_id == instanceId);

            return data;
        }

        internal IQueryable<T> GetAllValues_IDocumentSectionAttributesNonVMDS<T>(IQueryable<T> data, int? sectionType = null, Guid? sectionId = null)
            where T : class, IDocumentSectionAttributesNonVMDS
        {
            if (sectionType != null && sectionType > 0)
                data = data.Where(i => i.instance_section == sectionType);

            if (sectionId != null)
                data = data.Where(i => i.instance_section_id == sectionId);

            return data;
        }

        internal IQueryable<T> GetActualAllValues_IDocumentAttributes<T>(IDocumentStorage storage, int? instanceType = null, Guid? instanceId = null)
            where T : class, IDocumentAttributes
        {
            return Document.GetActualData(
                    GetAllValues_IDocumentAttributesNonVMDS<T>(
                        storage.Set<T>(), 
                        instanceType, 
                        instanceId), 
                storage.Snapshot);
        }

        internal IQueryable<T> GetActualAllValues_IDocumentSectionAttributes<T>(IDocumentStorage storage, int? instanceType = null, int? sectionType = null, Guid? instanceId = null, Guid? sectionId = null)
            where T : class, IDocumentSectionAttributes
        {
            return Document.GetActualData(
                    GetAllValues_IDocumentSectionAttributesNonVMDS<T>(
                            GetAllValues_IDocumentAttributesNonVMDS<T>(
                                storage.Set<T>(), 
                                instanceType, 
                                instanceId), 
                        sectionType, 
                        sectionId), 
                storage.Snapshot);
        }

        internal IQueryable<T> GetActualAllValues_IDocumentSectionCollectionAttributes<T>(IDocumentStorage storage, int? instanceType = null, int? sectionType = null, Guid? instanceId = null, Guid? sectionId = null, bool orderBySectionIndex = true)
            where T : class, IDocumentSectionCollectionAttributes
        {
            var data = GetAllValues_IDocumentSectionAttributesNonVMDS<T>(
                GetAllValues_IDocumentAttributesNonVMDS<T>(
                        storage.Set<T>(), 
                        instanceType, 
                        instanceId), 
                sectionType, 
                sectionId);

            if (orderBySectionIndex)
                data = data.OrderBy(i => i.instance_section_index);

            return Document.GetActualData(data, storage.Snapshot);
        }










        internal IQueryable<object> GetObjectValues(IDocumentStorage storage, Document owner, Guid? sectionId = null)
        {
            MethodInfo method = GetType().GetMethod("GetValues", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                                         .MakeGenericMethod(new Type[] { this.SectionDataType });

            return (IQueryable<object>)method.Invoke(this, new object[] { storage, owner, sectionId });
        }

        //Чтение экземпляров разделов в контексте документа.
        internal IQueryable<T> GetValues<T>(IDocumentStorage storage, Document owner, Guid? sectionId = null)
            where T : class
        {
            //Специализация реализации в зависимости от T
            var exemplar = Activator.CreateInstance<T>();
            if (exemplar as IDocumentSectionCollectionAttributes != null)
            {
                MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentSectionCollectionAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                return (IQueryable<T>)method.Invoke(this, new object[] { storage, owner.MyDefinition.Definition.InstanceType, this.Definition.SectionType, owner.Id, sectionId, true });
            }
            else
                if (exemplar as IDocumentSectionAttributes != null)
                {
                    MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentSectionAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                    return (IQueryable<T>)method.Invoke(this, new object[] { storage, owner.MyDefinition.Definition.InstanceType, this.Definition.SectionType, owner.Id, sectionId });
                }
                else
                    if (exemplar as IDocumentAttributes != null)
                    {
                        MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                        return (IQueryable<T>)method.Invoke(this, new object[] { storage, owner.MyDefinition.Definition.InstanceType, owner.Id });
                    }

            throw new DocumentException(string.Format( "Тип {0} несовместим с RDB моделью.", exemplar.GetType().FullName));
        }



        internal IQueryable<object> GetObjectAllValues(IDocumentStorage storage, int? instanceType = null, int? sectionType = null, Guid? instanceId = null)
        {
            MethodInfo method = GetType().GetMethod("GetAllValues", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { this.SectionDataType });
            return (IQueryable<object>)method.Invoke(this, new object[] { storage, instanceType, sectionType, instanceId });
        }

        //Чтение данных вне контекста документа
        internal IQueryable<T> GetAllValues<T>(IDocumentStorage storage, int? instanceType = null, int? sectionType = null, Guid? instanceId = null)
            where T : class
        {
            //Специализация реализации в зависимости от T
            var exemplar = Activator.CreateInstance<T>();
            if (exemplar as IDocumentSectionCollectionAttributes != null)
            {
                MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentSectionCollectionAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                return (IQueryable<T>)method.Invoke(this, new object[] { storage, instanceType, sectionType, instanceId, null, false });
            }
            else
                if (exemplar as IDocumentSectionAttributes != null)
                {
                    MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentSectionAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                    return (IQueryable<T>)method.Invoke(this, new object[] { storage, instanceType, sectionType, instanceId, null });
                }
                else
                    if (exemplar as IDocumentAttributes != null)
                    {
                        MethodInfo method = GetType().GetMethod("GetActualAllValues_IDocumentAttributes", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly).MakeGenericMethod(new Type[] { typeof(T) });
                        return (IQueryable<T>)method.Invoke(this, new object[] { storage, instanceType, instanceId });
                    }

            throw new DocumentException(string.Format("Тип {0} несовместим с RDB моделью.", exemplar.GetType().FullName));
        }

        #endregion



        //Определение значений системных полей перед записью/обновлением раздела в БД
        internal void EvaluateInstanceValues(DocumentSectionInstance section, Document owner, Guid? sectionId, int sectionIndex = 0)
        {
            //Составной ключ экземпляра раздела
            if (section.State == EDocumentItemState.dsNew)
            {
                InstanceIdProperty.SetValue(section.Value, owner.Id);
                InstanceTypeProperty.SetValue(section.Value, owner.MyDefinition.Definition.InstanceType);

                if (HasActualFrom)
                    ActualFromProperty.SetValue(section.Value, owner.ActualFrom);

                if (HasSectionDefinition)
                    InstanceSectionProperty.SetValue(section.Value, this.Definition.SectionType);

                //Идентификатор экземпляра раздела
                if (HasSectionIdDefinition)
                {
                    if (sectionId == null || sectionId == Guid.Empty)
                        sectionId = Guid.NewGuid();

                    InstanceSectionIdProperty.SetValue(section.Value, sectionId.Value);
                }
            }

            if (HasActualTill)
                ActualTillProperty.SetValue(section.Value, owner.ActualTill);

            //Порядковый номер (индекс) экземпляра раздела
            if (HasSectionIndexDefinition)
                InstanceSectionIndexProperty.SetValue(section.Value, sectionIndex);
        }

        /*
                 End of Internal API
        */








        /// <summary>
        /// Проверка целостности определения раздела документа.
        /// </summary>
        public void Validate()
        {
            if (!HasIdDefinition)
                throw new DocumentException("Не определено свойство \"instance_id\"!");

            if (!HasTypeDefinition)
                throw new DocumentException("Не определено свойство \"instance_type\"!");
        }
    }

}
