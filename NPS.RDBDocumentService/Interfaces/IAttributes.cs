﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Интерфейс таблицы, поддерживающей версионность данных (VMDS - version managed data storage).
    /// </summary>
    public interface IVMDSAttributes
    {
        /// <summary>
        /// Дата и время, с которого актуальны данные текущего экземпляра объекта (дата и время создания версии данных).
        /// </summary>
        DateTime actual_from { get; set; }
        /// <summary>
        /// Дата и время, с которого данные текущего экземпляра объекта не актуальны (были удалены). Условие <c>this.actual_till == null</c> означает, что данные являются актуальными на текущий момент времени.
        /// </summary>
        DateTime? actual_till { get; set; }
    }

    /// <summary>
    /// Базовый интерфейс таблицы - раздела документа.
    /// </summary>
    public interface IDocumentAttributesNonVMDS
    {
        /// <summary>
        /// Тип документа.
        /// </summary>
        int instance_type { get; set; }
        /// <summary>
        /// Уникальный идентификатор экземпляра документа.
        /// </summary>
        Guid instance_id { get; set; }
    }

    /// <summary>
    /// Базовый интерфейс таблицы - раздела документа с поддержкой версионности.
    /// </summary>
    public interface IDocumentAttributes : IDocumentAttributesNonVMDS, IVMDSAttributes
    {
    }

    /// <summary>
    /// Дополнительный интерфейс таблицы - раздела документа с указанием типа и идентификатора раздела.
    /// </summary>
    public interface IDocumentSectionAttributesNonVMDS : IDocumentAttributesNonVMDS
    {
        /// <summary>
        /// Тип раздела документа.
        /// </summary>
        int instance_section { get; set; }
        /// <summary>
        /// Уникальный идентификатор экземпляра раздела.
        /// </summary>
        Guid instance_section_id { get; set; }
    }

    /// <summary>
    /// Дополнительный интерфейс таблицы - раздела документа с указанием типа и идентификатора раздела с поддержкой версионности.
    /// </summary>
    public interface IDocumentSectionAttributes : IDocumentSectionAttributesNonVMDS, IVMDSAttributes
    {
    }

    /// <summary>
    /// Дополнительный интерфейс таблицы - коллекции (повторяющегося раздела) документа.
    /// </summary>
    public interface IDocumentSectionCollectionAttributesNonVMDS : IDocumentSectionAttributesNonVMDS
    {
        /// <summary>
        /// Индекс элемента коллекции (экземпляра повтоярющегося раздела) документа.
        /// </summary>
        int instance_section_index { get; set; }
    }

    /// <summary>
    /// Дополнительный интерфейс таблицы - коллекции (повторяющегося раздела) документа с поддержкой версионности.
    /// </summary>
    public interface IDocumentSectionCollectionAttributes : IDocumentSectionCollectionAttributesNonVMDS, IVMDSAttributes
    {
    }

}
