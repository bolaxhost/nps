﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.RDBDocumentService
{
    public interface IDocumentDataScope
    {
        /// <summary>
        /// Признак, имеет ли текущий Scope контекст хранилища
        /// </summary>
        bool HasContext {get;}

        /// <summary>
        /// Текущий контекст хранилища
        /// </summary>
        DocumentContext MyContext { get; set; }

        /// <summary>
        /// Текущий срез данных (Если null, то работа идет с актуальными данными)
        /// </summary>
        DateTime? Snapshot { get; }

        /// <summary>
        /// Получение данных указанного типа.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <returns>Результат запроса к базе данных.</returns>
        IQueryable<T> GetData<T>()
            where T : class;

        /// <summary>
        /// Получение актуальных данных указанного типа из указанного источника.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <param name="data">Запрос, на основе которого извлекаются актуальные данные.</param>
        /// <returns>Результат запроса к базе данных.</returns>
        IQueryable<T> GetActualData<T>(IQueryable<T> data)
            where T : class, IVMDSAttributes;

        /// <summary>
        /// Получение актуальных данных указанного типа.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <returns>Результат запроса к базе данных.</returns>
        IQueryable<T> GetActualData<T>()
            where T : class, IVMDSAttributes;

    }
}
