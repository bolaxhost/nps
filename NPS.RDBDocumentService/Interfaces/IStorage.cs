﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Configuration;
using System.Reflection;

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Интерфейс хранилища документов.
    /// </summary>
    public interface IDocumentStorage
    {
        /// <summary>
        /// Получение набора данных по типу раздела документа.
        /// </summary>
        /// <param name="entityType">Тип раздела.</param>
        /// <returns>Ссылка на набор данных.</returns>
        DbSet Set(Type entityType);

        /// <summary>
        /// Получение набора данных по типу раздела документа.
        /// </summary>
        /// <typeparam name="TEntity">Тип раздела.</typeparam>
        /// <returns>Ссылка на набор данных.</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Исключение раздела из контекста базы данных.
        /// </summary>
        /// <param name="entity">Ссылка на раздел.</param>
        void Detach(object entity);

        /// <summary>
        /// Включение/отключение режима версионности.
        /// </summary>
        bool UseVersioning { get; set; }

        /// <summary>
        /// Включение/отключение режима физического удаления данных из базы данных.
        /// </summary>
        bool PhysicalDeletion { get; set; }

        /// <summary>
        /// Текущий срез данных, определяемый выбранной датой и временем. Если <c>Snapshot</c> не определен, то документ работает в контексте актуальных данных.
        /// </summary>
        DateTime? Snapshot { get; set; }

        /// <summary>
        /// Сохранение изменений в базе данных.
        /// </summary>
        void SaveChanges();
    }
}
