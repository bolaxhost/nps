﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;   

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Разделяемый контекст документов.
    /// </summary>
    public class DocumentContext
    {
        public DocumentContext(IDocumentStorage storage)
        {
            _storage = storage;
        }


        /// <summary>
        /// Хранилище документов
        /// </summary>
        public IDocumentStorage DbStorage
        {
            get
            {
                return _storage;
            }
        }
        private IDocumentStorage _storage;

        
        




        
        //События контекста
        public delegate void BeforeCommitHandler(DocumentContext sender);
        public event BeforeCommitHandler BeforeCommit;

        protected virtual void RaiseBeforeCommit()
        {
            if (BeforeCommit != null)
                BeforeCommit(this);
        }

        public delegate void AfterCommitHandler(DocumentContext sender);
        public event AfterCommitHandler AfterCommit;

        protected virtual void RaiseAfterCommit()
        {
            if (AfterCommit != null)
                AfterCommit(this);
        }





        //Проверка целостности перед сохранением данным
        public virtual void ValidateCommit()
        {
            if (this.DbStorage.Snapshot != null)
                throw new DocumentException("Текущий режим работы с базой данных не позволяет вносить изменения!");
        }

        /// <summary>
        /// Сохранение всех текущих изменений
        /// </summary>
        public void Commit()
        {
            ValidateCommit();

            RaiseBeforeCommit();

            try
            {
                DbStorage.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder errorList = new StringBuilder();
                foreach (var errEntry in ex.EntityValidationErrors)
                    foreach (var err in errEntry.ValidationErrors)
                        errorList.AppendLine(err.ErrorMessage);

                throw new ContextException(errorList.ToString());
            }

            RaiseAfterCommit();
        }
    }
}
