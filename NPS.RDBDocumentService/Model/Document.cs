﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;

namespace NPS.RDBDocumentService
{
    /// <summary>
    /// Состояние элементов документа
    /// </summary>
    public enum EDocumentItemState
    {
        /// <summary>
        /// Не определено
        /// </summary>
        dsUndefined = 0,
        /// <summary>
        /// Новый документ или раздел
        /// </summary>
        dsNew = 1,
        /// <summary>
        /// Прочитанный из базы данных документ или раздел
        /// </summary>
        dsRead = 2
    }





    /// <summary>
    /// Сервисный класс - ключи (ссылка) на документ.
    /// </summary>
    public class DocumentKeys
    {
        /// <summary>
        /// Тип документа.
        /// </summary>
        public int documentType { get; set; }
        /// <summary>
        /// Уникальный идентификатор документа.
        /// </summary>
        public Guid documentId { get; set; }
    }








    /// <summary>
    /// Базовый класс документа. Класс прикладного документа в качестве базового класса должен использовать <c>Document</c>.
    /// </summary>
    public abstract class Document : IDocumentDataScope
    {
        //Коллекция зарегистрированных типов документа
        private static Dictionary<int, Type> _documentTypes = null;

        /// <summary>
        /// Регистрация типа прикладного документа для того чтобы можно было использовать методы <c>CreateDocument()</c> и <c>ReadDocument()</c>.
        /// </summary>
        /// <param name="documentType"></param>
        public static void AppendDocumentType(Type documentType)
        {
            if (_documentTypes == null)
                _documentTypes = new Dictionary<int, Type>();

            DocumentDefinition _documentDefinition = new DocumentDefinition(documentType);
            _documentDefinition.Validate();

            //Если документ данного типа уже зарегистрирован, то удаляем его для того, чтобы добавить новую версию определения
            if (_documentTypes.ContainsKey(_documentDefinition.Definition.InstanceType))
                _documentTypes.Remove(_documentDefinition.Definition.InstanceType);

            _documentTypes.Add(_documentDefinition.Definition.InstanceType, documentType);
        }







        /// <summary>
        /// Включение и отключение валидации экземпляра документа перед сохранением или удалением.
        /// </summary>
        public bool IgnoreValidation { get; set; }





        /// <summary>
        /// Создание нового экземпляра класса документа по коду его определения. Для сохранения нового экземпляра документа в базе данных после его создания необходимо вызвать методу <c>document.Save()</c>.
        /// </summary>
        /// <param name="instanceTypeId">Код типа документа.</param>
        /// <returns>Экземпляр документа.</returns>
        public static Document CreateDocument(int instanceTypeId)
        {
            if (_documentTypes == null)
                return null;

            if (!_documentTypes.ContainsKey(instanceTypeId))
                return null;

            return (Document)Activator.CreateInstance(_documentTypes[instanceTypeId]);
        }






        /// <summary>
        /// Чтение экземпляра документа по его ключам.
        /// </summary>
        /// <param name="keys">Ключи экземпляра документа.</param>
        /// <returns>Экземпляр документа.</returns>
        public static Document ReadDocument(DocumentKeys keys)
        {
            return ReadDocument(keys.documentType, keys.documentId);
        }









        /// <summary>
        /// Чтение экземпляра документа по его типу и уникальному идентификатору.
        /// </summary>
        /// <param name="instanceTypeId">Тип документа.</param>
        /// <param name="instanceId">Уникальный идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        public static Document ReadDocument(int instanceTypeId, Guid instanceId)
        {
            Document _document = CreateDocument(instanceTypeId);

            if (_document == null)
                throw new DocumentException(string.Format("Документ {0} не зарегистрирован!", instanceTypeId));

            _document.Read(instanceId);
            return _document;
        }











        /// <summary>
        /// Чтение экземпляра документа по экземпляру одного из его разделов.
        /// </summary>
        /// <param name="sectionValue">Ссылка на раздел документа.</param>
        /// <returns>Экземпляр документа.</returns>
        public static Document ReadDocument(object sectionValue)
        {
            return ReadDocument(
                DocumentSectionDefinition.GetInstanceTypeOf(sectionValue), 
                DocumentSectionDefinition.GetInstanceIdOf(sectionValue));
        }












        /// <summary>
        /// Текстовое описание экземпляра документа.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (!string.IsNullOrWhiteSpace(this.MyDefinition.Definition.Description))
                return string.Format("{0} {1}", this.MyDefinition.Definition.Description, this._id.GetValueOrDefault());

            return string.Format("{0} {1}", base.ToString(), this._id.GetValueOrDefault());
        }









        /// <summary>
        /// Определен ли для экземпляра документа контекст. Используется редко, поскольку контекст документа создается автоматически при необходимости.
        /// </summary>
        public bool HasContext
        {
            get
            {
                return _context != null;
            }
        }

        /// <summary>
        /// Контекст экземпляра документа (документов).
        /// </summary>
        public DocumentContext MyContext
        {
            get
            {
                return _context ?? (MyContext = this.CreateDocumentContext());
            }
            set
            {
                if (_context != null)
                {
                    _context.BeforeCommit -= OnBeforeCommit;
                    _context.AfterCommit -= OnAfterCommit;
                }

                _context = value;

                if (_context != null)
                {
                    _context.BeforeCommit += OnBeforeCommit;
                    _context.AfterCommit += OnAfterCommit;
                }
            }
        }
        
        //Контекст документа
        private DocumentContext _context = null;







        /// <summary>
        /// Создание хранилища документов. Необходимо реализовать в производных документах
        /// </summary>
        /// <returns></returns>
        protected abstract IDocumentStorage CreateDocumentStorage();

        /// <summary>
        /// Создание контекста документа по умолчанию.
        /// </summary>
        /// <returns></returns>
        protected virtual DocumentContext CreateDocumentContext()
        {
            return new DocumentContext(this.CreateDocumentStorage());
        }








        /// <summary>
        /// Состояние текущего экземпляра документа.
        /// </summary>
        public EDocumentItemState State
        {
            get { return _state; }
        }

        private EDocumentItemState _state = EDocumentItemState.dsUndefined;







        /// <summary>
        /// Уникальный идентификатор документа.
        /// </summary>
        public Guid? Id
        {
            get
            {
                if (_id == null)
                {
                    if (State == EDocumentItemState.dsUndefined)
                        return null;

                    if (State == EDocumentItemState.dsNew)
                        _id = Guid.NewGuid();
                }

                return _id;
            }

            set
            {
                if (State != EDocumentItemState.dsNew)
                    throw new DocumentException("Некорректное текущее состояние документа для изменения Id!");

                _id = value;
            }
        }

        private Guid? _id = null;








        /// <summary>
        /// Свойство определяет, используется ли версионность в контексте текущего экземпляра документа.
        /// </summary>
        public virtual bool UseVersioning
        {
            get
            {
                if (_useVersioning != null)
                    return _useVersioning.Value;

                return MyContext.DbStorage.UseVersioning;
            }

            set
            {
                _useVersioning = value;
            }
        }

        private bool? _useVersioning = null;








        /// <summary>
        /// Свойство определяет, используется ли физическое удаление данных в процессе удаления текущего экземпляра документа, или данные только помечаются, как удаленные с помощью свойства <c>actual_till</c>.
        /// </summary>
        public bool PhysicalDeletion
        {
            get
            {
                if (_physicalDeletion != null)
                    return _physicalDeletion.Value;

                return MyContext.DbStorage.PhysicalDeletion;
            }

            set
            {
                _physicalDeletion = value;
            }
        }

        private bool? _physicalDeletion = null;





        /// <summary>
        /// Текущий срез данных (Если null, то работа идет с актуальными данными)
        /// </summary>
        public DateTime? Snapshot
        {
            get
            {
                return MyContext.DbStorage.Snapshot;
            }
        }










        /// <summary>
        /// Идентификатор версии документа - дата, с которой актуальны данные текущей версии документа.
        /// </summary>
        public DateTime? ActualFrom
        {
            get
            {
                if (_actualFrom == null)
                    _actualFrom = DateTime.Now;

                return _actualFrom;
            }

            protected set
            {
                _actualFrom = value;
            }
        }

        private DateTime? _actualFrom = null;





        /// <summary>
        /// Определен ли идентификатор версии для текущего экземпляра документа. Может быть <c>false</c> только для новых и еще не сохраненных экземпляров документов.
        /// </summary>
        public bool HasActualFrom
        {
            get { return _actualFrom != null; }
        }







        /// <summary>
        /// Дата удаления версии документа. Данное свойство может быть определено только для неактуальной версии документа.
        /// </summary>
        public DateTime? ActualTill
        {
            get
            {
                return _actualTill;
            }

            private set
            {
                _actualTill = value;
            }
        }

        private DateTime? _actualTill = null;






        /// <summary>
        /// Определена ли дата удаления для текущего экземпляра документа. Может быть <c>true</c> только для удаленных версий документа.
        /// </summary>
        public bool HasActualTill
        {
            get { return _actualTill != null; }
        }





        /// <summary>
        /// Признак, определено ли для текущего экземпляра документа значение Id. Может быть <c>false</c> только для новых и еще не сохраненных экземпляров документов.
        /// </summary>
        public bool HasId
        {
            get { return _id != null; }
        }








        /// <summary>
        /// Инициализация нового экземпляра документа.
        /// </summary>
        /// <param name="idToCreate">Идентификатор нового экземпляра документа.</param>
        public virtual void Create(Guid? idToCreate = null)
        {
            Clear();

            _id = idToCreate;
            _CreateMe();
            _state = EDocumentItemState.dsNew;

            AfterCreate();
        }
        
        /// <summary>
        /// Постобработка создания документа по умолчанию. Поведение метода при необходимости переопределяется.
        /// </summary>
        protected virtual void AfterCreate()
        {

        }









        /// <summary>
        /// Чтение документа по идентификатору. В процессе чтения из базы данных извлекаются только обязательные разделы и коллекции. Данные необязательных разделов и коллекций извлекаются при необходимости.
        /// </summary>
        /// <param name="IdToRead"></param>
        public virtual void Read(Guid IdToRead)
        {
            Clear();

            _id = IdToRead;
            _ReadMe();
            _state = EDocumentItemState.dsRead;

            AfterRead();
        }

        public async void ReadAsync(Guid IdToRead)
        {
            await Task.Run(() =>
            {
                Read(IdToRead);
            });
        }

        /// <summary>
        /// Постобработка чтения документа по умолчанию. Поведение метода при необходимости переопределяется.
        /// </summary>
        protected virtual void AfterRead()
        {

        }






        /// <summary>
        /// Чтение всех необязательных разделов, в том числе необязательных разделов и коллекций. Может быть вызван только после вызова метода <c>Read()</c>
        /// </summary>
        public virtual void ReadAll()
        {
            if (State != EDocumentItemState.dsRead)
                throw new DocumentException("Некорректное текущее состояние документа для чтения всех данных!");               

            foreach (DocumentSectionDefinition section in MyDefinition.SectionDefinitions.Select(i => i.Value))
                if (!_IsSectionInitialized(section))
                    _ReadSection(section);
        }

        public async void ReadAllAsync()
        {
            await Task.Run(() =>
            {
                ReadAll();
            });
        }

        /// <summary>
        /// Очистка документа и перевод его в состояние 'не определено'.
        /// </summary>
        public virtual void Clear()
        {
            //Освобождаются от контекста все загруженные секции документа, включая удаляемые
            var sectionsToRelease = SectionsValues
                .SelectMany(s => s.Value.Instances
                    .Union(s.Value.InstancesToDelete))
                .Union(SectionValues.Select(s => s.Value));

            //Освобождение секций
            foreach (var section in sectionsToRelease)
                ReleaseSection(section);

            _sectionValues = null;
            _sectionsValues = null;

            _id = null;
            _state = EDocumentItemState.dsUndefined;
            _commitContext = 0;
        }




        /// <summary>
        /// Отмена всех изменений, выполненных в документе после его чтения или создания и перевод его в исходное состояние.
        /// </summary>
        public virtual void Undo()
        {
            var state = this.State;
            var id = this.Id;

            this.Clear();

            if (state != EDocumentItemState.dsRead)
                return;

            this.Read(id.Value);
        }






        //Разделяемая коллекция определений документов
        private static Dictionary<string, DocumentDefinition> _documentDefinitions = new Dictionary<string, DocumentDefinition>();
        //Описание документа
        private DocumentDefinition _documentDefinition = null;

        /// <summary>
        /// Определение документа
        /// </summary>
        public DocumentDefinition MyDefinition
        {
            get
            {
                return _documentDefinition ?? (_documentDefinition = GetDocumentDefinition());
            }
        }
        private DocumentDefinition GetDocumentDefinition()
        {
            string typeName = this.GetType().AssemblyQualifiedName;

            if (!_documentDefinitions.ContainsKey(typeName))
                _documentDefinitions.Add(typeName, new DocumentDefinition(this.GetType()));

            return _documentDefinitions[typeName];
        }





        /*
            Работа с данными
         */



        //Значения простых разделов
        private Dictionary<string, DocumentSectionInstance> _sectionValues = null;
        internal Dictionary<string, DocumentSectionInstance> SectionValues
        {
            get
            {
                return _sectionValues ?? (_sectionValues = new Dictionary<string, DocumentSectionInstance>());
            }
        }






        //Список удаленных простых необязательных разделов (для предотвращения повторного чтения удаленных значений)
        private List<string> _deletedSingleOptionalSections = null;
        private List<string> DeletedSingleOptionalSections
        {
            get
            {
                return _deletedSingleOptionalSections ?? (_deletedSingleOptionalSections = new List<string>());
            }
        }






        //Значения разделов-коллекций
        private Dictionary<string, DocumentSectionInstances> _sectionsValues = null;
        internal Dictionary<string, DocumentSectionInstances> SectionsValues
        {
            get
            {
                return _sectionsValues ?? (_sectionsValues = new Dictionary<string, DocumentSectionInstances>());
            }
        }









        private void _CreateMe()
        {
            var sectionsToCreate = MyDefinition.SectionDefinitions
                                               .Select(s => s.Value)
                                               .Where(s => s.Definition.IsRequired);

            foreach (DocumentSectionDefinition section in sectionsToCreate)
                _CreateSection(section);
        }

        private void _ReadMe()
        {
            var sectionsToRead = MyDefinition.SectionDefinitions
                                             .Select(s => s.Value)
                                             .Where(s => s.Definition.IsRequired);

            foreach (DocumentSectionDefinition section in sectionsToRead)
                if (!_ReadSection(section))
                    throw new DocumentException(string.Format("Невозможно прочитать экземпляры обязательного раздела {0}!", section.SectionName));
        }

        private void _CreateSection(DocumentSectionDefinition section, object value = null)
        {
            if (!section.IsCollection)
            {
                if (this.SectionValues.ContainsKey(section.SectionName))
                    return;

                DocumentSectionInstance _sectionInstance = new DocumentSectionInstance(this, section);
                _sectionInstance.State = EDocumentItemState.dsNew;

                if (value == null)
                    _sectionInstance.Value = Activator.CreateInstance(section.SectionDataType);
                else
                    _sectionInstance.Value = value;

                this.SectionValues.Add(section.SectionName, _sectionInstance);
            }
            else
            {
                if (this.SectionsValues.ContainsKey(section.SectionName))
                    return;

                DocumentSectionInstances _sectionInstances = new DocumentSectionInstances(this, section);

                if (section.Definition.IsRequired)
                {
                    if (value == null)
                        _sectionInstances.Add(Activator.CreateInstance(section.SectionDataType), EDocumentItemState.dsNew);
                    else
                        _sectionInstances.Add(value, EDocumentItemState.dsNew);
                }
                else
                {
                    if (value != null)
                        _sectionInstances.Add(value, EDocumentItemState.dsNew);
                }

                this.SectionsValues.Add(section.SectionName, _sectionInstances);
            }
        }

        private bool _ReadSection(DocumentSectionDefinition section)
        {
            if (!section.IsCollection)
            {
                //Значение необязательного раздела было удалено и не должно быть прочитано заново!
                if (!section.Definition.IsRequired && this.DeletedSingleOptionalSections.Contains(section.SectionName))
                    return false;

                object value = section.GetObjectValues(this.MyContext.DbStorage, this).FirstOrDefault();
                if (value == null)
                    return false;

                EvaluateActualFromTillValues(section, value);

                DocumentSectionInstance _sectionInstance = new DocumentSectionInstance(this, section);
                _sectionInstance.State = EDocumentItemState.dsRead;
                _sectionInstance.Value = value;

                this.SectionValues.Add(section.SectionName, _sectionInstance);
            }
            else
            {
                IQueryable<object> values = section.GetObjectValues(this.MyContext.DbStorage, this);
                if (!values.Any())
                    return false;

                DocumentSectionInstances _sectionInstances = new DocumentSectionInstances(this, section);

                foreach (var value in values)
                {
                    EvaluateActualFromTillValues(section, value);
                    _sectionInstances.Add(value, EDocumentItemState.dsRead);
                }

                this.SectionsValues.Add(section.SectionName, _sectionInstances);
            }

            return true;
        }

        private void EvaluateActualFromTillValues(DocumentSectionDefinition section, object value)
        {
            if (section.HasActualFrom && !this.HasActualFrom)
                this.ActualFrom = (DateTime)section.ActualFromProperty.GetValue(value);

            if (section.HasActualTill && !this.HasActualTill)
            {
                var actualTill = section.ActualTillProperty.GetValue(value);
                if (actualTill != null)
                    this.ActualTill = (DateTime)actualTill;
            }
        }

        private bool _IsSectionInitialized(DocumentSectionDefinition section)
        {
            if (!section.IsCollection)
            {
                return this.SectionValues.ContainsKey(section.SectionName);
            }
            else
            {
                return this.SectionsValues.ContainsKey(section.SectionName);
            }
        }

        /// <summary>
        /// Индекс фрейма вызываемого метода в стеке вызовов. Имеет смысл переопределять только в случае переопределения методов <c>GetSection()</c> и <c>SetSection()</c> в производных документах.
        /// </summary>
        protected virtual int StackFrameIndex
        {
            get
            {
                return 2;
            }
        }

        //Описание вызываемого метода: CallingMethod=>GetSection/GetSections=>GetCallingMethod
        private MethodBase GetCallingMethod()
        {
            StackTrace stack = new StackTrace();
            StackFrame frame = stack.GetFrame(this.StackFrameIndex);

            return frame.GetMethod();
        }

        /// <summary>
        /// Сервисная функция. Получение ссылки на раздел документа по имени свойства раздела, определенного в документе.
        /// </summary>
        /// <param name="propertyName">Имя свойства с префиксом get_.</param>
        /// <returns>Ссылка на раздел.</returns>
        protected object GetSectionValue(string propertyName)
        {
            return GetType()
                    .GetMethod(propertyName)
                    .Invoke(this, null);
        }

        /// <summary>
        /// Сервисная функция. Получение ссылки на раздел, соответствующий вызываемому свойству, определенному в документе. Вызываемое свойство определяется через стек вызовов с использованием смещения <c>StackFrameIndex</c>.
        /// </summary>
        /// <typeparam name="T">Тип раздела.</typeparam>
        /// <param name="createInstanceIfNecessary">Определяет, нужно ли создавать раздел, если его еще нет (необязательный раздел).</param>
        /// <returns>Ссылка на раздел.</returns>
        protected virtual T GetSection<T>(bool createInstanceIfNecessary = false)
            where T : class
        {
            if (this.State == EDocumentItemState.dsUndefined)
                Create();

            return GetSection<T>(GetCallingMethod().Name, createInstanceIfNecessary);
        }

        private T GetSection<T>(string propertyName, bool createInstanceIfNecessary = false)
            where T : class
        {
            if (!SectionValues.ContainsKey(propertyName))
            {
                bool exists = false;
                if (this.State == EDocumentItemState.dsRead)
                    exists = _ReadSection(MyDefinition.SectionDefinitions[propertyName]);

                if (!exists)
                    if (createInstanceIfNecessary)
                        _CreateSection(MyDefinition.SectionDefinitions[propertyName]);
                    else
                        return null;
            }

            return (T)SectionValues[propertyName].Value;
        }

        /// <summary>
        /// Сервисная функция. Установка значения раздела, соответствующий вызываемому свойству, определенному в документе. Вызываемое свойство определяется через стек вызовов с использованием смещения <c>StackFrameIndex</c>.
        /// </summary>
        /// <typeparam name="T">Тип раздела.</typeparam>
        /// <param name="value">Ссылка на новый раздел.</param>
        protected virtual void SetSection<T>(T value)
            where T : class
        {
            if (this.State == EDocumentItemState.dsUndefined)
                Create();

            SetSection<T>(GetCallingMethod().Name.Replace("set_", "get_"), value);
        }

        private void SetSection<T>(string propertyName, T value)
            where T : class
        {
            DocumentSectionDefinition section = MyDefinition.SectionDefinitions[propertyName];

            if (section.Definition.IsRequired && value == null)
                throw new DocumentException(string.Format("Значение раздела {0} не может быть пустым!", section.SectionName));

            if (GetSection<T>(propertyName, false) != null)
            {
                DeleteSection(SectionValues[propertyName]);
                this.SectionValues.Remove(propertyName);
            }

            if (value == null)
                return;

            _CreateSection(section, value);
        }

        /// <summary>
        /// Сервисная функция. Получение ссылки на коллекцию документа по имени свойства раздела, определенного в документе.
        /// </summary>
        /// <param name="propertyName">Имя свойства с префиксом set_.</param>
        /// <returns>Ссылка на коллекцию.</returns>
        protected IEnumerable<object> GetSectionValues(string propertyName)
        {
            return (IEnumerable<object>)GetType()
                .GetMethod(propertyName)
                .Invoke(this, null);
        }

        /// <summary>
        /// Сервисная функция. Получение ссылки на коллекцию, соответствующую вызываемому свойству, определенному в документе. Вызываемое свойство определяется через стек вызовов с использованием смещения <c>StackFrameIndex</c>.
        /// </summary>
        /// <typeparam name="T">Тип раздела.</typeparam>
        /// <returns>Ссылка на коллекцию.</returns>
        protected virtual ICollection<T> GetSections<T>()
            where T : class
        {
            if (this.State == EDocumentItemState.dsUndefined)
                Create();

            return GetSections<T>(GetCallingMethod().Name);
        }

        private ICollection<T> GetSections<T>(string propertyName)
            where T : class
        {
            if (!SectionsValues.ContainsKey(propertyName))
            {
                bool exists = false;
                if (this.State == EDocumentItemState.dsRead)
                    exists = _ReadSection(MyDefinition.SectionDefinitions[propertyName]);

                if (!exists)
                    _CreateSection(MyDefinition.SectionDefinitions[propertyName]);
            }

            return (ICollection<T>)SectionsValues[propertyName].GetValues<T>();
        }

        /// <summary>
        /// Сортировка элементов в коллекции документа в соответствии с порядком, определенным списком <c>orderBy</c>.
        /// </summary>
        /// <typeparam name="T">Тип элементов коллекции.</typeparam>
        /// <param name="collectionToSort">Сортируемая коллекция.</param>
        /// <param name="orderBy">Список элементов, определяющий новый порядок коллекции.</param>
        public void OrderBy<T>(ICollection<T> collectionToSort, List<T> orderBy)
        {
            DocumentSectionInstances _data = SectionsValues
                                            .Select(d => d.Value)
                                            .FirstOrDefault(l => l.Is(collectionToSort));

            if (_data == null)
                return;

            List<DocumentSectionInstance> _list = _data.Instances;
            T[] _array = orderBy.ToArray();

            for (int i = 0; i < _array.Length; i++)
            {
                T _item = _array[i];

                DocumentSectionInstance _instance = _list.FirstOrDefault(d => Object.ReferenceEquals(d.Value, _item));

                if (_instance != null)
                {
                    _list.Remove(_instance);
                    _list.Insert(i, _instance);
                }
            }

        }

        //Сохранение новой секции
        private void SaveSection(DocumentSectionInstance section, Guid? id = null, int index = 0)
        {
            section.Definition.EvaluateInstanceValues(section, this, id, index);

            if (section.State != EDocumentItemState.dsNew)
                return;

            this.MyContext.DbStorage
                .Set(section.Definition.SectionDataType)
                .Add(section.Value);
        }
        //Удаление прочитанной секции
        private void DeleteSection(DocumentSectionInstance section)
        {
            if (section.State != EDocumentItemState.dsRead)
                return;

            this.MyContext.DbStorage
                .Set(section.Definition.SectionDataType)
                .Remove(section.Value);

            //Сохранение имени необязательного раздела в списке удаленных разделов
            if (!section.Definition.IsCollection && !section.Definition.Definition.IsRequired)
                this.DeletedSingleOptionalSections.Add(section.Definition.SectionName);
        }
        //Освобождение секции от контекста в результате отмены изменений
        private void ReleaseSection(DocumentSectionInstance section)
        {
            if (section.State != EDocumentItemState.dsRead)
                return;

            this.MyContext.DbStorage.Detach(section.Value);
        }





        /// <summary>
        /// Общая неотключаемая проверка целостности данных, вызываемая перед внесением любых изменений по документу в базу данных. Метод может быть переопределен.
        /// </summary>
        protected virtual void ValidateCommit()
        {
            this.MyContext.ValidateCommit();

            if (this.IsReadonly)
                throw new DocumentException("Документ только для чтения и не может быть изменен!");

            if (this.HasActualTill)
                throw new DocumentException("Документ помечен, как удаленный и не может быть изменен!");
        }

        /// <summary>
        /// Сохранение документа (создание новой версии документа).
        /// </summary>
        public virtual void Save()
        {
            PrepareSaveDocument();
            this.MyContext.Commit();
        }

        public async void SaveAsync()
        {
            await Task.Run(() => {
                Save();
            });
        }

        /// <summary>
        /// Предобработка сохранения документа без выполнения изменений в базе данных. Данный метод необходимо использовать при объединении сохранения нескольких документов в одну транзакцию, в том числе в процедурах контроля целостности данных.
        /// </summary>
        public virtual void PrepareSaveDocument()
        {
            if (isCommitContextDefined)
                return;

            if (this.State == EDocumentItemState.dsUndefined)
                throw new DocumentException("Некорректное текущее состояние документа для сохранения!");

            ValidateCommit();

            if (!IgnoreValidation)
                ValidateSave();

            if (UseVersioning && State == EDocumentItemState.dsRead)
            {
                //Создание новой версии
                var clone = this.Clone(false);
                clone.Id = this.Id.Value;
                clone.IgnoreValidation = this.IgnoreValidation;
                clone.PrepareSave();

                //Отмена изменений в текущем документе
                Undo();
                //Чтение всех данных
                ReadAll();
                //"Удаление" текущего документа
                ActualTill = clone.ActualFrom;
            }

            PrepareSave(true);
        }

        /// <summary>
        /// Подготовка разделов документа к сохранению в базу данных.
        /// </summary>
        /// <param name="skipValidation">Отключает проверки целостности данных.</param>
        private void PrepareSave(bool skipValidation = false)
        {
            if (!skipValidation)
            {
                if (isCommitContextDefined)
                    return;

                if (!IgnoreValidation)
                    ValidateSave();
            }

            //Удаление экземпляров разделов из коллекций
            foreach (DocumentSectionInstance section in SectionsValues.SelectMany(s => s.Value.InstancesToDelete))
                DeleteSection(section);

            //Сохранение/обновление простых разделов
            foreach (DocumentSectionInstance section in SectionValues.Select(s => s.Value))
                SaveSection(section, section.Id, 0);

            //Сохранение/обновление коллекций разделов с проставлением индекса - порядкового номера секции в коллекции
            foreach (DocumentSectionInstances sections in SectionsValues.Select(s => s.Value))
                for (int i = 0; i < sections.Instances.ToArray().Count(); i++)
                    SaveSection(sections.Instances[i], sections.Instances[i].Id, i);

            _commitContext = 1;
        }







        /// <summary>
        /// Удаление документа (пометка актуальной версии документа, как удаленной).
        /// </summary>
        public virtual void Delete()
        {
            PrepareDeleteDocument();
            this.MyContext.Commit();
        }
        public async void DeleteAsync()
        {
            await Task.Run(() =>
            {
                Delete();
            });
        }

        /// <summary>
        /// Предобработка удаления документа без выполнения изменений в базе данных. Данный метод необходимо использовать при объединении удаления нескольких документов в одну транзакцию, в том числе в процедурах контроля целостности данных.
        /// </summary>
        public virtual void PrepareDeleteDocument()
        {
            if (isCommitContextDefined)
                return;

            if (this.State == EDocumentItemState.dsUndefined)
                throw new DocumentException("Некорректное текущее состояние документа для удаления!");

            ValidateCommit();

            if (this.State != EDocumentItemState.dsRead)
                return;

            if (!IgnoreValidation)
                ValidateDelete();

            if (!PhysicalDeletion)
            {
                //Чтение всех данных
                ReadAll();
                //"Удаление" текущего документа
                ActualTill = DateTime.Now;
                //Сохранение "удаленного" документа
                PrepareSave();
            }
            else
                PrepareDelete(true);
        }

        /// <summary>
        /// Подготовка разделов документа к удалению из базы данных.
        /// </summary>
        /// <param name="skipValidation">Отключает проверки целостности данных.</param>
        private void PrepareDelete(bool skipValidation = false)
        {
            if (!skipValidation)
            {
                if (this.State != EDocumentItemState.dsRead)
                    return;

                if (isCommitContextDefined)
                    return;

                if (!IgnoreValidation) 
                    ValidateDelete();
            }

            ReadAll();

            //Список всех секций документа
            var sectionsToDelete = SectionValues
                                  .Select(s => s.Value)
                                  .Union(SectionsValues.SelectMany(s => s.Value.Instances));

            //Удаление всех секций
            foreach (DocumentSectionInstance instance in sectionsToDelete)
                DeleteSection(instance);

            _commitContext = -1;
        }






        /// <summary>
        /// Восстанавливает предыдущую версию документа. Если текущая версия является единственной, то удаляет ее
        /// </summary>
        public virtual void RestoreLastVersion()
        {
            if (this.MyContext.DbStorage.Snapshot != null)
                throw new DocumentException("Некорректное значение snapshot!");

            //Удаление текущей версии
            this.PhysicalDeletion = true;
            PrepareDeleteDocument();

            //Переход контекста на срез данных предыдущей версии
            this.MyContext.DbStorage.Snapshot = this._actualFrom;

            //Документ предыдущей версии
            Document lastVersion = (Document)Activator.CreateInstance(GetType());
            lastVersion.MyContext = this.MyContext;

            //Попытка чтения предыдущей версии
            try
            {
                lastVersion.Read(this._id.Value);
            }
            catch (DocumentException) //Предыдущей версии не существует
            {
                this.MyContext.DbStorage.Snapshot = null;
                this.MyContext.Commit();
                return;
            }

            //Восстановление документа
            lastVersion.PrepareRestoreDocument();

            //Сохранение изменений
            this.MyContext.DbStorage.Snapshot = null;
            this.MyContext.Commit();
        }

        protected virtual void PrepareRestoreDocument()
        {
            if (isCommitContextDefined)
                return;

            if (this.State != EDocumentItemState.dsRead)
                throw new DocumentException("Некорректное текущее состояние документа для удаления!");

            if (this.MyContext.DbStorage.Snapshot == null)
                throw new DocumentException("Значение snapshot должно быть определено!");

            if (this.ActualTill == null)
                return;

            //Чтение всех необязательных разделов
            ReadAll();

            //Восстановление предыдущей версии
            this.ActualTill = null;
            this.IgnoreValidation = true;

            this._skipBeforeAfterHandlers = true;
            PrepareSave(true);
        }






        /// <summary>
        /// Формирование новой версии отдельного рздела документа (для сохранения отдельного раздела вне процедуры сохранения всего документа). Используется для создания выделенной версии отдельного раздела без создания новой версии всего документа.
        /// </summary>
        public virtual T GetOrCreateNewVersionOf<T>(T section)
            where T : class, IDocumentAttributes
        {
            ValidateCommit();

            T clone = (T)CloneSectionInstance(section, this.Snapshot, false);

            clone.actual_from = DateTime.Now;
            clone.actual_till = null;

            section.actual_till = clone.actual_from;

            this.MyContext.DbStorage
                .Set<T>()
                .Add(clone);

            return clone;
        }







        private int _commitContext = 0;
        /// <summary>
        /// Сервисное свойство, сообщающее, находится ли документ в стадии операции сохранения/удаления или нет.
        /// </summary>
        protected bool isCommitContextDefined
        {
            get
            {
                return _commitContext != 0;
            }
        }










        //Отмена вызова обработчиков Before и After Commit Event
        private bool _skipBeforeAfterHandlers = false;
        private void OnBeforeCommit(DocumentContext sender)
        {
            if (_skipBeforeAfterHandlers)
                return;

            if (!isCommitContextDefined)
                return;

            if (sender != this.MyContext)
                return;

            if (_commitContext == 1)
                BeforeSave();
            else if (_commitContext == -1)
                BeforeDelete();
        }

        private void OnAfterCommit(DocumentContext sender)
        {
            if (_skipBeforeAfterHandlers)
                return;

            if (!isCommitContextDefined)
                return;

            if (sender != this.MyContext)
                return;

            if (_commitContext == 1)
                AfterSave();
            else if (_commitContext == -1)
                AfterDelete();
        }










        /// <summary>
        /// Проверки целостности данных перед сохранением. Для добавления собственной проверки метод необходимо переопределить.
        /// </summary>
        protected virtual void ValidateSave()
        {
        }

        /// <summary>
        /// Предобработка перед сохранением документа.  Для добавления собственной предобработки метод необходимо переопределить.
        /// </summary>
        protected virtual void BeforeSave()
        {
        }

        /// <summary>
        /// Постобработка после сохранением документа.  Для добавления собственной постобработки метод необходимо переопределить.
        /// </summary>
        protected virtual void AfterSave()
        {
        }

        /// <summary>
        /// Проверки целостности данных перед удалением. Для добавления собственной проверки метод необходимо переопределить.
        /// </summary>
        protected virtual void ValidateDelete()
        {
        }

        /// <summary>
        /// Предобработка перед удалением документа.  Для добавления собственной предобработки метод необходимо переопределить.
        /// </summary>
        protected virtual void BeforeDelete()
        {
        }

        /// <summary>
        /// Постобработка после удаления документа.  Для добавления собственной постобработки метод необходимо переопределить.
        /// </summary>
        protected virtual void AfterDelete()
        {
        }




















        /// <summary>
        /// Признак, что данные документа изменять нельзя.
        /// </summary>
        public virtual bool IsReadonly
        {
            get
            {
                return false;
            }
        }








        /// <summary>
        /// Копирование данных по разделам. Копируются только данные загруженных разделов. 
        /// Для копирования всех данных предварительно используйте метод ReadAll().
        /// </summary>
        /// <param name="target">Документ, куда копируются данные.</param>
        public virtual void Copy(Document target)
        {
            if (this.State == EDocumentItemState.dsUndefined)
                throw new DocumentException("Некорректное текущее состояние документа для копирования!");

            if (target.State == EDocumentItemState.dsUndefined)
                target.Create();

            foreach (DocumentSectionDefinition section in this.MyDefinition.SectionDefinitions.Select(i => i.Value))
                if (!section.Definition.IsSystem && _IsSectionInitialized(section))
                    if (section.IsCollection)
                    {
                        DocumentSectionInstances sourceInstances = SectionsValues[section.SectionName];

                        if (!target.SectionsValues.ContainsKey(section.SectionName) && target.State == EDocumentItemState.dsRead)
                            target._ReadSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        if (!target.SectionsValues.ContainsKey(section.SectionName))
                            target._CreateSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        DocumentSectionInstances targetInstances = target.SectionsValues[section.SectionName];

                        while (sourceInstances.Count < targetInstances.Count)
                            targetInstances.Remove(targetInstances.Instances[0]);

                        while (targetInstances.Count < sourceInstances.Count)
                            targetInstances.Add();

                        DocumentSectionInstance[] sourceValues = sourceInstances.Instances.ToArray();
                        DocumentSectionInstance[] targetValues = targetInstances.Instances.ToArray();

                        for (int i = 0; i < sourceValues.Length; i++)
                        {
                            DocumentSectionInstance instance = targetValues[i];

                            if (instance.Value == null)
                                instance.Value = CloneSectionInstance(sourceValues[i].Value, this.Snapshot);
                            else
                                CopySectionInstance(sourceValues[i].Value, instance.Value, this.Snapshot);
                        }
                    }
                    else
                    {
                        if (!target.SectionValues.ContainsKey(section.SectionName) && target.State == EDocumentItemState.dsRead)
                            target._ReadSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        if (!target.SectionValues.ContainsKey(section.SectionName))
                            target._CreateSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        DocumentSectionInstance instance = target.SectionValues[section.SectionName];

                        if (instance.Value == null)
                            instance.Value = CloneSectionInstance(SectionValues[section.SectionName].Value, this.Snapshot);
                        else
                            CopySectionInstance(SectionValues[section.SectionName].Value, instance.Value, this.Snapshot);
                    }
        }

        /// <summary>
        /// Создание нового документа - копии исходного документа. При необходимости изменить или доработать логику клонирования документа, метод необходимо переопределить.
        /// </summary>
        /// <param name="excludeSystem">Исключить копирование системных разделов.</param>
        /// <returns>Новый документ, являющийся клоном текущего документа.</returns>
        public virtual Document Clone(bool excludeSystem = true)
        {
            if (this.State == EDocumentItemState.dsUndefined)
                throw new DocumentException("Некорректное текущее состояние документа для клонирования!");

            Document target = (Document)Activator.CreateInstance(GetType());
            target.MyContext = this.MyContext;
            target.Create();

            foreach (DocumentSectionDefinition section in this.MyDefinition.SectionDefinitions.Select(i => i.Value))
                if (!excludeSystem || !section.Definition.IsSystem)
                    if (section.IsCollection)
                    {
                        if (!this.SectionsValues.ContainsKey(section.SectionName) && this.State == EDocumentItemState.dsRead)
                            _ReadSection(section);

                        if (!this.SectionsValues.ContainsKey(section.SectionName))
                            continue;

                        DocumentSectionInstances sourceInstances = SectionsValues[section.SectionName];

                        if (!target.SectionsValues.ContainsKey(section.SectionName))
                            target._CreateSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        DocumentSectionInstances targetInstances = target.SectionsValues[section.SectionName];
                        targetInstances.Clear();

                        foreach (object sourceInstance in sourceInstances.Instances.Select(i => i.Value))
                            targetInstances.Add(CloneSectionInstance(sourceInstance, this.Snapshot, excludeSystem));
                    }
                    else
                    {
                        if (!this.SectionValues.ContainsKey(section.SectionName) && this.State == EDocumentItemState.dsRead)
                            _ReadSection(section);

                        if (!this.SectionValues.ContainsKey(section.SectionName))
                            continue;

                        if (!target.SectionValues.ContainsKey(section.SectionName))
                            target._CreateSection(target.MyDefinition.SectionDefinitions[section.SectionName]);

                        target.SectionValues[section.SectionName].Value = CloneSectionInstance(SectionValues[section.SectionName].Value, this.Snapshot, excludeSystem);
                    }

            return target;
        }

        /// <summary>
        /// Создание копии раздела
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        internal static object CloneSectionInstance(object instance, DateTime? snapshot, bool excludeSystem = true)
        {
            object result = Activator.CreateInstance(instance.GetType());
            CopySectionInstance(instance, result, snapshot, excludeSystem);

            return result;
        }

        internal static void CopySectionInstance(object source, object target, DateTime? snapshot, bool excludeSystem = true)
        {
            DocumentSectionDefinition section = DocumentSectionDefinition.GetDefaltSectionDefinition(source);

            foreach (var p in source.GetType().GetProperties())
                if (p.CanRead && p.CanWrite && p.GetCustomAttribute<System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute>() == null)
                    if (!p.PropertyType.Name.StartsWith("ICollection"))
                    {
                        //Копирование простых свойств
                        if (!excludeSystem || !section.IsSystemProperty(p))
                            p.SetValue(target, p.GetValue(source));
                    }
                    else
                    {
                        //Копирование связей
                        MethodInfo method = typeof(Document).GetMethod("ConcatCollections", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.DeclaredOnly)
                                            .MakeGenericMethod(new Type[] { p.PropertyType.GenericTypeArguments[0] });
                        method.Invoke(null, new object[] { p.GetValue(source), p.GetValue(target), snapshot });
                    }
        }

        internal static void ConcatCollections<T>(ICollection<T> source, ICollection<T> target, DateTime? snapshot)
        {
            DocumentSectionDefinition section = null;
            try
            {
                section = DocumentSectionDefinition.GetDefaltSectionDefinition(typeof(T));
            }
            catch
            {
                foreach (var item in source)
                    target.Add(item);

                return;
            }

            foreach (var item in source)
            {
                bool add = true;
                if (section.HasActualFrom && section.HasActualTill)
                {
                    DateTime actualFrom = (DateTime)section.ActualFromProperty.GetValue(item);
                    DateTime? actualTill = (DateTime?)section.ActualTillProperty.GetValue(item);

                    if (snapshot == null)
                        add = actualTill == null;
                    else
                        add = actualFrom < snapshot && (actualTill == null || actualTill >= snapshot);
                }

                if (add)
                    target.Add(item);
            }
        }





        /// <summary>
        /// Удаление всех экземпляров данного типа документа из базы данных.
        /// </summary>
        public void DeleteAllInstances()
        {
            foreach (DocumentSectionDefinition section in MyDefinition.SectionDefinitions.Select(i => i.Value))
            {
                IQueryable<object> instances = section.GetObjectAllValues(
                        this.MyContext.DbStorage, 
                        MyDefinition.Definition.InstanceType, 
                        section.Definition.SectionType);

                this.MyContext.DbStorage.Set(section.SectionDataType).RemoveRange(instances);
            }

            this.MyContext.Commit();
        }

        public async void DeleteAllInstancesAsync()
        {
            await Task.Run(() =>
            {
                DeleteAllInstances();
            });
        }














        /// <summary>
        /// Извлечение данных по имени раздела.
        /// </summary>
        /// <typeparam name="T">Тип раздела документа (таблицы).</typeparam>
        /// <param name="sectionName">Имя раздела в определении документа с префиксом <c>get_</c>.</param>
        /// <param name="useFilterByDocumentType">Выбирать только данные, соответствующие типу текущего документа.</param>
        /// <param name="useFilterBySectionType">Выбирать только данные, соответствующие типу указанного раздела документа.</param>
        /// <param name="instanceId">Выбирать только данные указанного <c>instanceId</c> экземпляра.</param>
        /// <returns></returns>
        protected IQueryable<T> GetData<T>(string sectionName, bool useFilterByDocumentType = true, bool useFilterBySectionType = true, Guid? instanceId = null)
            where T : class
        {
            if (!MyDefinition.SectionDefinitions.ContainsKey(sectionName))
                return null;

            DocumentSectionDefinition section = MyDefinition.SectionDefinitions[sectionName];

            int? documentType = null;
            if (useFilterByDocumentType && MyDefinition.Definition.InstanceType > 0)
                documentType = MyDefinition.Definition.InstanceType;

            int? sectionType = null;
            if (useFilterBySectionType && section.Definition.SectionType > 0)
                sectionType = section.Definition.SectionType;

            return section.GetAllValues<T>(this.MyContext.DbStorage, documentType, sectionType, instanceId);
        }

        /// <summary>
        /// Получение данных указанного типа.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <returns>Результат запроса к базе данных.</returns>
        public IQueryable<T> GetData<T>()
            where T : class
        {
            if (Activator.CreateInstance<T>() as IDocumentAttributesNonVMDS != null)
                return this.MyContext
                           .DbStorage
                           .Set<T>()
                           .AsQueryable();

            throw new DocumentException(string.Format("Тип {0} несовместим с RDBDocument моделью.", typeof(T).FullName));
        }

        /// <summary>
        /// Получение актуальных данных указанного типа из указанного источника.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <param name="data">Запрос, на основе которого извлекаются актуальные данные.</param>
        /// <returns>Результат запроса к базе данных.</returns>
        public IQueryable<T> GetActualData<T>(IQueryable<T> data)
            where T : class, IVMDSAttributes
        {
            return GetActualData(data, this.Snapshot);
        }

        /// <summary>
        /// Получение актуальных данных указанного типа.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <returns>Результат запроса к базе данных.</returns>
        public IQueryable<T> GetActualData<T>()
            where T : class, IVMDSAttributes
        {
            return GetActualData(GetData<T>());
        }

        /// <summary>
        /// Получение актуальных данных указанного типа. Если определено значение <c>this.Snapshot</c>, то выбираются данные, соответствующие указанному срезу данных.
        /// </summary>
        /// <typeparam name="T">Тип раздела (таблицы).</typeparam>
        /// <param name="data">Запрос, на основе которого извлекаются актуальные данные.</param>
        /// <param name="snapshot">Срез данных.</param>
        /// <returns>Результат запроса к базе данных.</returns>
        public static IQueryable<T> GetActualData<T>(IQueryable<T> data, DateTime? snapshot)
            where T : class, IVMDSAttributes
        {
            if (snapshot == null)
                return data.Where(i => i.actual_till == null);

            return data.Where(i => i.actual_from < snapshot && (i.actual_till == null || i.actual_till >= snapshot));
        }
    }

}
