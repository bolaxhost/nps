﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPS.RDBDocumentService
{
    /*
        Классы - обертки для разделов документа, управляемые экземпляром класса Document
     */



    /// <summary>
    /// Обертка (wrapper) над экземпляром раздела документа.
    /// </summary>
    internal class DocumentSectionInstance
    {
        private DocumentSectionDefinition _definition;
        internal DocumentSectionDefinition Definition
        {
            get
            {
                return _definition;
            }
        }

        private Document _document;
        internal DocumentSectionInstance(Document document, DocumentSectionDefinition definition)
        {
            _document = document;
            _definition = definition;
        }




        internal Guid? Id
        {
            get
            {
                if (Value == null)
                    return null;

                if (!Definition.HasSectionIdDefinition)
                    return null;

                Guid _guid = Guid.Parse(Definition.InstanceSectionIdProperty.GetValue(Value).ToString());
                if (_guid == Guid.Empty)
                    return null;

                return _guid;
            }
        }

        internal int index
        {
            get
            {
                if (Value == null)
                    return -1;

                if (!Definition.HasSectionIndexDefinition)
                    return -1;

                if (!Definition.IsCollection)
                    return 0;

                return (int)Definition
                    .InstanceSectionIndexProperty
                    .GetValue(Value);
            }
        }




        internal object Value { get; set; }
        internal EDocumentItemState State { get; set; }
    }











    /// <summary>
    /// Обертка (wrapper) над коллекцией экземпляров разделов.
    /// </summary>
    internal class DocumentSectionInstances
    {
        private Document _document;
        internal Document Document { get { return _document; } }




        private DocumentSectionDefinition _definition;
        internal DocumentSectionDefinition Definition { get { return _definition; } }





        private List<DocumentSectionInstance> _instances = new List<DocumentSectionInstance>();
        internal List<DocumentSectionInstance> Instances  { get { return _instances; } }






        private List<DocumentSectionInstance> _instancesToDelete = new List<DocumentSectionInstance>();
        internal IEnumerable<DocumentSectionInstance> InstancesToDelete  { get { return _instancesToDelete; } }





        internal DocumentSectionInstances(Document document, DocumentSectionDefinition definition)
        {
            _document = document;
            _definition = definition;
        }




        internal DocumentSectionInstanceCollection<T> GetCollection<T>()
            where T : class
        {
            return (DocumentSectionInstanceCollection<T>)_collection ?? (_collection = new DocumentSectionInstanceCollection<T>(this));
        }

        private dynamic _collection;



        internal ICollection<T> GetValues<T>()
            where T : class
        {
            return GetCollection<T>();
        }

        internal bool Is<T>(ICollection<T> collection)
        {
            return _collection == collection;
        }

        internal void Add(object item, EDocumentItemState state)
        {
            _instances.Add(new DocumentSectionInstance(_document, _definition) { Value = item, State = state });
        }

        internal void Add(object item)
        {
            Add(item, EDocumentItemState.dsNew);
        }

        internal void Add(EDocumentItemState state)
        {
            Add(Activator.CreateInstance(this.Definition.SectionDataType), state);
        }

        internal void Add()
        {
            Add(EDocumentItemState.dsNew);
        }

        internal bool Remove(DocumentSectionInstance instanceToRemove)
        {
            if (instanceToRemove.State == EDocumentItemState.dsRead)
                _instancesToDelete.Add(instanceToRemove);

            return _instances.Remove(instanceToRemove);
        }

        internal void Clear()
        {
            foreach (var value in Instances.ToList())
                Remove(value);
        }

        internal int Count
        {
            get { return Instances.Count; }
        }
    }





    /// <summary>
    /// Вспомогательный класс - пользовательский интерфейс для работы с коллекцией экземпляров разделов документа.
    /// </summary>
    /// <typeparam name="T">Тип раздела</typeparam>
    public class DocumentSectionInstanceCollection<T> : ICollection<T>
        where T : class
    {
        private DocumentSectionInstances _scope = null;
        internal DocumentSectionInstanceCollection(DocumentSectionInstances scope)
        {
            _scope = scope;
        }




        private IEnumerable<T> GetItems() { return _scope.Instances.Select(i => (T)i.Value); }
        private DocumentSectionInstance GetSectionInstance(T item) { return _scope.Instances.FirstOrDefault(i => i.Value == item); }



        //Реализация ICollection для работы с коллекцией разделов, как с обычной коллекцией
        #region ICollection
        public void Add(T item)
        {
            _scope.Add(item);
        }

        public void Clear()
        {
            _scope.Clear();
        }

        public bool Contains(T item)
        {
            return GetItems().Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _scope.Instances.Select(i => (T)i.Value).ToArray().CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _scope.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(T item)
        {
            if (_scope.Definition.Definition.IsRequired && _scope.Count == 1)
                throw new DocumentException("Нельзя удалить последний экземпляр обязательного раздела!");

            DocumentSectionInstance valueToRemove = GetSectionInstance(item);

            if (valueToRemove == null)
                return false;

            return _scope.Remove(valueToRemove);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return GetItems().GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetItems().GetEnumerator();
        }
        #endregion
    }

}
